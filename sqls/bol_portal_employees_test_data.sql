/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : employeeportal

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 26/09/2018 16:39:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bol_portal_employees
-- ----------------------------
DROP TABLE IF EXISTS `bol_portal_employees`;
CREATE TABLE `bol_portal_employees`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `company_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `department_id` int(11) NULL DEFAULT NULL,
  `designation_id` int(11) NULL DEFAULT NULL,
  `date_of_birth` date NULL DEFAULT NULL,
  `blood_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `otp` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phones` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `extension` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bol_portal_employees
-- ----------------------------
INSERT INTO `bol_portal_employees` VALUES (1, 'Demo Employee 1', '1', 11, 3, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'rashed.anowar@bol-online.com', '901703', '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 12:46:55');
INSERT INTO `bol_portal_employees` VALUES (2, 'Demo Employee 2', '1', 1, 1, '2018-09-04', 'AB-', 'Dhaka, Bangladesh', 'employee2@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (3, 'Demo Employee 3', '1', 2, 1, '2018-09-04', 'AB+', 'Dhaka, Bangladesh', 'employee3@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (4, 'Demo Employee 4', '1', 3, 1, '2018-09-04', 'B+', 'Dhaka, Bangladesh', 'employee4@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (5, 'Demo Employee 5', '1', 2, 2, '2018-09-04', 'A-', 'Dhaka, Bangladesh', 'employee5@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (6, 'Demo Employee 6', '1', 2, 5, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee6@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (7, 'Demo Employee 7', '1', 2, 7, '2018-09-04', 'O+', 'Dhaka, Bangladesh', 'employee7@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (8, 'Demo Employee 8', '1', 11, 8, '2018-09-04', 'B-', 'Dhaka, Bangladesh', 'employee8@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (9, 'Demo Employee 9', '1', 11, 9, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee9@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (10, 'Demo Employee 10', '1', 3, 10, '2018-09-04', 'A-', 'Dhaka, Bangladesh', 'employee10@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 12:30:31');
INSERT INTO `bol_portal_employees` VALUES (11, 'Demo Employee 11', '1', 3, 4, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee11@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (12, 'Demo Employee 12', '1', 3, 6, '2018-09-04', 'B+', 'Dhaka, Bangladesh', 'employee12@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (13, 'Demo Employee 13', '1', 3, 10, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee13@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (14, 'Demo Employee 14', '1', 4, 5, '2018-09-04', 'O+', 'Dhaka, Bangladesh', 'employee14@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (15, 'Demo Employee 15', '1', 4, 5, '2018-09-04', 'O-', 'Dhaka, Bangladesh', 'employee15@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (16, 'Demo Employee 16', '1', 4, 5, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee16@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (17, 'Demo Employee 17', '1', 4, 6, '2018-09-04', 'O+', 'Dhaka, Bangladesh', 'employee17@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (18, 'Demo Employee 18', '1', 5, 6, '2018-09-04', 'B-', 'Dhaka, Bangladesh', 'employee18@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (19, 'Demo Employee 19', '1', 5, 6, '2018-09-04', 'B+', 'Dhaka, Bangladesh', 'employee19@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (20, 'Demo Employee 20', '1', 5, 7, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee20@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (21, 'Demo Employee 21', '1', 5, 7, '2018-09-04', 'AB-', 'Dhaka, Bangladesh', 'employee21@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (22, 'Demo Employee 22', '1', 6, 7, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee22@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (23, 'Demo Employee 23', '1', 6, 5, '2018-09-04', 'O-', 'Dhaka, Bangladesh', 'employee23@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (24, 'Demo Employee 24', '1', 7, 6, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee24@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (25, 'Demo Employee 25', '1', 8, 7, '2018-09-04', 'B+', 'Dhaka, Bangladesh', 'employee25@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (26, 'Demo Employee 26', '1', 7, 7, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee26@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (27, 'Demo Employee 27', '1', 7, 9, '2018-09-04', 'A-', 'Dhaka, Bangladesh', 'employee27@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (28, 'Demo Employee 28', '1', 7, 12, '2018-09-04', 'AB-', 'Dhaka, Bangladesh', 'employee28@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (29, 'Demo Employee 29', '1', 8, 10, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee29@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (30, 'Demo Employee 30', '1', 8, 10, '2018-09-04', 'O+', 'Dhaka, Bangladesh', 'employee30@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (31, 'Demo Employee 31', '1', 8, 10, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee31@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (32, 'Demo Employee 32', '1', 9, 11, '2018-09-04', 'A-', 'Dhaka, Bangladesh', 'employee32@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (33, 'Demo Employee 33', '1', 9, 11, '2018-09-04', 'B+', 'Dhaka, Bangladesh', 'employee33@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (34, 'Demo Employee 34', '1', 9, 11, '2018-09-04', 'B-', 'Dhaka, Bangladesh', 'employee34@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (35, 'Demo Employee 35', '1', 10, 12, '2018-09-04', 'B-', 'Dhaka, Bangladesh', 'employee35@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (36, 'Demo Employee 36', '1', 10, 12, '2018-09-04', 'A-', 'Dhaka, Bangladesh', 'employee36@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (37, 'Demo Employee 37', '1', 10, 12, '2018-09-04', 'AB-', 'Dhaka, Bangladesh', 'employee37@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (38, 'Demo Employee 38', '1', 11, 13, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee38@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (39, 'Demo Employee 39', '1', 13, 13, '2018-09-04', 'B-', 'Dhaka, Bangladesh', 'employee39@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (40, 'Demo Employee 40', '1', 14, 13, '2018-09-04', 'B+', 'Dhaka, Bangladesh', 'employee40@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (41, 'Demo Employee 41', '1', 1, 14, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee41@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (42, 'Demo Employee 42', '1', 2, 14, '2018-09-04', 'AB-', 'Dhaka, Bangladesh', 'employee42@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (43, 'Demo Employee 43', '1', 3, 14, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee43@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (44, 'Demo Employee 44', '1', 4, 15, '2018-09-04', 'AB-', 'Dhaka, Bangladesh', 'employee44@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (45, 'Demo Employee 45', '1', 5, 15, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee45@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (46, 'Demo Employee 46', '1', 6, 15, '2018-09-04', 'B+', 'Dhaka, Bangladesh', 'employee46@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (47, 'Demo Employee 47', '1', 7, 16, '2018-09-04', 'O-', 'Dhaka, Bangladesh', 'employee47@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (48, 'Demo Employee 48', '1', 8, 16, '2018-09-04', 'AB-', 'Dhaka, Bangladesh', 'employee48@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (49, 'Demo Employee 49', '1', 9, 16, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee49@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (50, 'Demo Employee 50', '1', 10, 17, '2018-09-04', 'A-', 'Dhaka, Bangladesh', 'employee50@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (51, 'Demo Employee 51', '1', 11, 17, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee51@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (52, 'Demo Employee 52', '1', 12, 17, '2018-09-04', 'O-', 'Dhaka, Bangladesh', 'employee52@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (53, 'Demo Employee 53', '1', 13, 11, '2018-09-04', 'B+', 'Dhaka, Bangladesh', 'employee53@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (54, 'Demo Employee 54', '1', 14, 13, '2018-09-04', 'B-', 'Dhaka, Bangladesh', 'employee54@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (55, 'Demo Employee 55', '1', 15, 14, '2018-09-04', 'B-', 'Dhaka, Bangladesh', 'employee55@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (56, 'Demo Employee 56', '1', 16, 16, '2018-09-04', 'O+', 'Dhaka, Bangladesh', 'employee56@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (57, 'Demo Employee 57', '1', 17, 17, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee57@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (58, 'Demo Employee 58', '1', 1, 12, '2018-09-04', 'B+', 'Dhaka, Bangladesh', 'employee58@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (59, 'Demo Employee 59', '1', 3, 5, '2018-09-04', 'A+', 'Dhaka, Bangladesh', 'employee59@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (60, 'Demo Employee 60', '1', 2, 5, '2018-09-04', 'A-', 'Dhaka, Bangladesh', 'employee60@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (61, 'Demo Employee 61', '1', 4, 5, '2018-09-04', 'B+', 'Dhaka, Bangladesh', 'employee61@portal.com', NULL, '[{\"type\":\"office\",\"phone\":\"09609000121\"},{\"type\":\"home\",\"phone\":\"09609000121\"}]', '10201', '2018-09-25 11:56:22', '2018-09-25 11:56:22');
INSERT INTO `bol_portal_employees` VALUES (62, 'Reza', '2', 19, 18, '1970-09-08', 'A+', 'Motijheel', 'liton_taj@yahoo.com', '570539', '[{\"type\":\"office\",\"phone\":\"01713339689\"}]', '14214', '2018-09-26 05:49:20', '2018-09-26 05:52:53');
INSERT INTO `bol_portal_employees` VALUES (63, 'Zahangir', '2', 21, 19, '1980-10-08', 'A-', 'Motijheel', 'test@test.com', NULL, '[{\"type\":\"office\",\"phone\":\"47895421\"},{\"type\":\"office\",\"phone\":\"123456\"}]', '1333', '2018-09-26 05:56:40', '2018-09-26 05:57:08');

SET FOREIGN_KEY_CHECKS = 1;
