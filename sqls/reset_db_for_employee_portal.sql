TRUNCATE backend_users;
TRUNCATE bol_portal_companies;
TRUNCATE bol_portal_company_users;
TRUNCATE bol_portal_departments;
TRUNCATE bol_portal_designations;
TRUNCATE bol_portal_employees;


INSERT INTO `employeeportal`.`backend_users`(`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `is_superuser`) VALUES (1, 'Rashed', 'Anowar', 'admin', 'rashed.anowar@bol-online.com', '$2y$10$aDJ.aOYoaWmHbUdd212nOuPkyRJg8LbQ49.16BwVzntLT4v.H/rWu', NULL, '$2y$10$Oh22uKH2i4PcgiFQwoTnzOKcxd6GeouEbUX.NmJPw9sojOcuQRMgK', NULL, '', 1, 2, NULL, '2018-08-14 11:45:44', '2018-08-12 07:50:10', '2018-08-14 11:45:44', 1);
