/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : employeeportal

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 24/09/2018 17:11:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for backend_access_log
-- ----------------------------
DROP TABLE IF EXISTS `backend_access_log`;
CREATE TABLE `backend_access_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of backend_access_log
-- ----------------------------
INSERT INTO `backend_access_log` VALUES (1, 1, '::1', '2018-08-12 07:50:18', '2018-08-12 07:50:18');
INSERT INTO `backend_access_log` VALUES (2, 1, '::1', '2018-08-12 07:50:38', '2018-08-12 07:50:38');
INSERT INTO `backend_access_log` VALUES (3, 1, '::1', '2018-08-12 07:56:21', '2018-08-12 07:56:21');
INSERT INTO `backend_access_log` VALUES (4, 1, '::1', '2018-08-12 09:08:07', '2018-08-12 09:08:07');
INSERT INTO `backend_access_log` VALUES (5, 1, '::1', '2018-08-14 03:48:48', '2018-08-14 03:48:48');
INSERT INTO `backend_access_log` VALUES (6, 1, '::1', '2018-08-14 03:51:11', '2018-08-14 03:51:11');
INSERT INTO `backend_access_log` VALUES (7, 14, '::1', '2018-08-14 08:48:52', '2018-08-14 08:48:52');
INSERT INTO `backend_access_log` VALUES (8, 1, '::1', '2018-08-14 08:50:53', '2018-08-14 08:50:53');
INSERT INTO `backend_access_log` VALUES (9, 14, '::1', '2018-08-14 08:56:48', '2018-08-14 08:56:48');
INSERT INTO `backend_access_log` VALUES (10, 1, '::1', '2018-08-14 11:45:44', '2018-08-14 11:45:44');
INSERT INTO `backend_access_log` VALUES (11, 14, '::1', '2018-08-14 12:52:26', '2018-08-14 12:52:26');
INSERT INTO `backend_access_log` VALUES (12, 14, '::1', '2018-08-16 04:13:22', '2018-08-16 04:13:22');
INSERT INTO `backend_access_log` VALUES (13, 14, '::1', '2018-08-16 04:14:44', '2018-08-16 04:14:44');
INSERT INTO `backend_access_log` VALUES (14, 2, '192.168.20.77', '2018-08-16 06:12:21', '2018-08-16 06:12:21');
INSERT INTO `backend_access_log` VALUES (15, 1, '192.168.20.77', '2018-08-16 06:21:15', '2018-08-16 06:21:15');
INSERT INTO `backend_access_log` VALUES (16, 2, '::1', '2018-08-16 06:35:58', '2018-08-16 06:35:58');
INSERT INTO `backend_access_log` VALUES (17, 2, '::1', '2018-08-16 09:21:51', '2018-08-16 09:21:51');
INSERT INTO `backend_access_log` VALUES (18, 2, '::1', '2018-08-16 11:54:07', '2018-08-16 11:54:07');
INSERT INTO `backend_access_log` VALUES (19, 3, '::1', '2018-08-16 12:04:36', '2018-08-16 12:04:36');
INSERT INTO `backend_access_log` VALUES (20, 2, '::1', '2018-08-16 12:05:10', '2018-08-16 12:05:10');
INSERT INTO `backend_access_log` VALUES (21, 2, '::1', '2018-08-16 12:09:56', '2018-08-16 12:09:56');
INSERT INTO `backend_access_log` VALUES (22, 2, '::1', '2018-08-16 12:40:05', '2018-08-16 12:40:05');
INSERT INTO `backend_access_log` VALUES (23, 3, '::1', '2018-08-16 12:41:35', '2018-08-16 12:41:35');
INSERT INTO `backend_access_log` VALUES (24, 1, '192.168.20.239', '2018-08-28 10:06:56', '2018-08-28 10:06:56');
INSERT INTO `backend_access_log` VALUES (25, 2, '192.168.20.239', '2018-09-03 10:57:51', '2018-09-03 10:57:51');
INSERT INTO `backend_access_log` VALUES (26, 5, '192.168.20.77', '2018-09-03 11:18:52', '2018-09-03 11:18:52');
INSERT INTO `backend_access_log` VALUES (27, 1, '192.168.20.239', '2018-09-03 12:04:14', '2018-09-03 12:04:14');
INSERT INTO `backend_access_log` VALUES (28, 2, '192.168.20.239', '2018-09-06 05:10:38', '2018-09-06 05:10:38');
INSERT INTO `backend_access_log` VALUES (29, 2, '192.168.4.118', '2018-09-16 07:07:37', '2018-09-16 07:07:37');
INSERT INTO `backend_access_log` VALUES (30, 3, '::1', '2018-09-17 04:07:44', '2018-09-17 04:07:44');
INSERT INTO `backend_access_log` VALUES (31, 1, '::1', '2018-09-17 09:13:22', '2018-09-17 09:13:22');
INSERT INTO `backend_access_log` VALUES (32, 2, '::1', '2018-09-18 08:51:10', '2018-09-18 08:51:10');
INSERT INTO `backend_access_log` VALUES (33, 2, '::1', '2018-09-19 07:20:14', '2018-09-19 07:20:14');
INSERT INTO `backend_access_log` VALUES (34, 2, '::1', '2018-09-20 07:30:06', '2018-09-20 07:30:06');
INSERT INTO `backend_access_log` VALUES (35, 2, '::1', '2018-09-20 07:30:08', '2018-09-20 07:30:08');
INSERT INTO `backend_access_log` VALUES (36, 2, '::1', '2018-09-20 10:47:34', '2018-09-20 10:47:34');
INSERT INTO `backend_access_log` VALUES (37, 2, '::1', '2018-09-23 09:04:27', '2018-09-23 09:04:27');
INSERT INTO `backend_access_log` VALUES (38, 8, '::1', '2018-09-23 11:06:00', '2018-09-23 11:06:00');
INSERT INTO `backend_access_log` VALUES (39, 2, '::1', '2018-09-23 13:03:24', '2018-09-23 13:03:24');
INSERT INTO `backend_access_log` VALUES (40, 2, '::1', '2018-09-23 13:40:20', '2018-09-23 13:40:20');
INSERT INTO `backend_access_log` VALUES (41, 2, '::1', '2018-09-24 04:06:20', '2018-09-24 04:06:20');
INSERT INTO `backend_access_log` VALUES (42, 2, '::1', '2018-09-24 04:13:18', '2018-09-24 04:13:18');
INSERT INTO `backend_access_log` VALUES (43, 2, '::1', '2018-09-24 04:43:48', '2018-09-24 04:43:48');
INSERT INTO `backend_access_log` VALUES (44, 2, '::1', '2018-09-24 04:44:54', '2018-09-24 04:44:54');
INSERT INTO `backend_access_log` VALUES (45, 2, '::1', '2018-09-24 04:46:26', '2018-09-24 04:46:26');
INSERT INTO `backend_access_log` VALUES (46, 2, '::1', '2018-09-24 04:48:06', '2018-09-24 04:48:06');
INSERT INTO `backend_access_log` VALUES (47, 2, '::1', '2018-09-24 04:51:48', '2018-09-24 04:51:48');
INSERT INTO `backend_access_log` VALUES (48, 2, '::1', '2018-09-24 04:53:21', '2018-09-24 04:53:21');
INSERT INTO `backend_access_log` VALUES (49, 2, '::1', '2018-09-24 05:51:33', '2018-09-24 05:51:33');
INSERT INTO `backend_access_log` VALUES (50, 2, '::1', '2018-09-24 07:00:05', '2018-09-24 07:00:05');
INSERT INTO `backend_access_log` VALUES (51, 2, '::1', '2018-09-24 07:22:07', '2018-09-24 07:22:07');
INSERT INTO `backend_access_log` VALUES (52, 9, '::1', '2018-09-24 09:05:15', '2018-09-24 09:05:15');
INSERT INTO `backend_access_log` VALUES (53, 2, '::1', '2018-09-24 09:06:48', '2018-09-24 09:06:48');
INSERT INTO `backend_access_log` VALUES (54, 9, '::1', '2018-09-24 09:10:40', '2018-09-24 09:10:40');
INSERT INTO `backend_access_log` VALUES (55, 9, '::1', '2018-09-24 09:31:25', '2018-09-24 09:31:25');
INSERT INTO `backend_access_log` VALUES (56, 2, '::1', '2018-09-24 10:01:46', '2018-09-24 10:01:46');

-- ----------------------------
-- Table structure for backend_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `backend_user_groups`;
CREATE TABLE `backend_user_groups`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name_unique`(`name`) USING BTREE,
  INDEX `code_index`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of backend_user_groups
-- ----------------------------
INSERT INTO `backend_user_groups` VALUES (1, 'Owners', '2018-08-12 07:50:10', '2018-09-20 08:41:36', 'owners', 'Default group for website owners.', 1);
INSERT INTO `backend_user_groups` VALUES (2, 'Test Group', '2018-09-20 08:42:14', '2018-09-20 08:42:26', 'test_group', 'Test Group', 1);

-- ----------------------------
-- Table structure for backend_user_preferences
-- ----------------------------
DROP TABLE IF EXISTS `backend_user_preferences`;
CREATE TABLE `backend_user_preferences`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_item_index`(`user_id`, `namespace`, `group`, `item`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of backend_user_preferences
-- ----------------------------
INSERT INTO `backend_user_preferences` VALUES (1, 1, 'backend', 'reportwidgets', 'dashboard', '{\"welcome\":{\"class\":\"Backend\\\\ReportWidgets\\\\Welcome\",\"sortOrder\":50,\"configuration\":{\"ocWidgetWidth\":6}},\"systemStatus\":{\"class\":\"System\\\\ReportWidgets\\\\Status\",\"sortOrder\":60,\"configuration\":{\"ocWidgetWidth\":6}},\"activeTheme\":{\"class\":\"Cms\\\\ReportWidgets\\\\ActiveTheme\",\"sortOrder\":70,\"configuration\":{\"ocWidgetWidth\":4}}}');
INSERT INTO `backend_user_preferences` VALUES (2, 2, 'backend', 'reportwidgets', 'dashboard', '{\"welcome\":{\"class\":\"Backend\\\\ReportWidgets\\\\Welcome\",\"sortOrder\":\"50\",\"configuration\":{\"ocWidgetWidth\":6}},\"systemStatus\":{\"class\":\"System\\\\ReportWidgets\\\\Status\",\"sortOrder\":\"70\",\"configuration\":{\"ocWidgetWidth\":6}},\"activeTheme\":{\"class\":\"Cms\\\\ReportWidgets\\\\ActiveTheme\",\"sortOrder\":\"71\",\"configuration\":{\"ocWidgetWidth\":4}}}');

-- ----------------------------
-- Table structure for backend_user_roles
-- ----------------------------
DROP TABLE IF EXISTS `backend_user_roles`;
CREATE TABLE `backend_user_roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `permissions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `role_unique`(`name`) USING BTREE,
  INDEX `role_code_index`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of backend_user_roles
-- ----------------------------
INSERT INTO `backend_user_roles` VALUES (1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2018-08-12 07:50:10', '2018-08-12 07:50:10');
INSERT INTO `backend_user_roles` VALUES (2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2018-08-12 07:50:10', '2018-08-12 07:50:10');
INSERT INTO `backend_user_roles` VALUES (3, 'Company Admin', 'company_admin', 'Who manage the employee protal', '{\"bol.portal.settings.manage_backend_users\":\"1\",\"bol.portal.manage_department\":\"1\",\"bol.portal.manage_notification\":\"1\",\"bol.portal.manage_group\":\"1\",\"bol.portal.create_company\":\"1\",\"bol.portal.update_company\":\"1\",\"bol.portal.manage_designation\":\"1\",\"bol.portal.manage_employee\":\"1\"}', 0, '2018-08-12 09:18:48', '2018-09-24 06:40:21');

-- ----------------------------
-- Table structure for backend_user_throttle
-- ----------------------------
DROP TABLE IF EXISTS `backend_user_throttle`;
CREATE TABLE `backend_user_throttle`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `ip_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT 0,
  `last_attempt_at` timestamp(0) NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT 0,
  `suspended_at` timestamp(0) NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT 0,
  `banned_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `backend_user_throttle_user_id_index`(`user_id`) USING BTREE,
  INDEX `backend_user_throttle_ip_address_index`(`ip_address`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of backend_user_throttle
-- ----------------------------
INSERT INTO `backend_user_throttle` VALUES (1, 1, '::1', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (2, 14, '::1', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (3, 2, '192.168.20.77', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (4, 1, '192.168.20.77', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (5, 2, '::1', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (6, 3, '::1', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (7, 1, '192.168.20.239', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (8, 2, '192.168.20.239', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (9, 5, '192.168.20.77', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (10, 2, '192.168.4.118', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (11, 8, '::1', 0, NULL, 0, NULL, 0, NULL);
INSERT INTO `backend_user_throttle` VALUES (12, 9, '::1', 0, NULL, 0, NULL, 0, NULL);

-- ----------------------------
-- Table structure for backend_users
-- ----------------------------
DROP TABLE IF EXISTS `backend_users`;
CREATE TABLE `backend_users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `login` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `persist_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `reset_password_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `permissions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `is_activated` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `activated_at` timestamp(0) NULL DEFAULT NULL,
  `last_login` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `login_unique`(`login`) USING BTREE,
  UNIQUE INDEX `email_unique`(`email`) USING BTREE,
  INDEX `act_code_index`(`activation_code`) USING BTREE,
  INDEX `reset_code_index`(`reset_password_code`) USING BTREE,
  INDEX `admin_role_index`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of backend_users
-- ----------------------------
INSERT INTO `backend_users` VALUES (1, 'Rashed', 'Anowar', 'admin', 'rashed.anowar@bol-online.com', '', '$2y$10$aDJ.aOYoaWmHbUdd212nOuPkyRJg8LbQ49.16BwVzntLT4v.H/rWu', NULL, '$2y$10$LTnIqbFexIC3dKHebHqHuukIw5PBMbWrwLUJqBZ.ikcRiJemDF.pC', NULL, '', 1, 2, NULL, '2018-09-17 09:13:21', '2018-08-12 07:50:10', '2018-09-17 09:13:21', 1);

-- ----------------------------
-- Table structure for backend_users_groups
-- ----------------------------
DROP TABLE IF EXISTS `backend_users_groups`;
CREATE TABLE `backend_users_groups`  (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `user_group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of backend_users_groups
-- ----------------------------
INSERT INTO `backend_users_groups` VALUES (1, 1);

-- ----------------------------
-- Table structure for bol_portal_companies
-- ----------------------------
DROP TABLE IF EXISTS `bol_portal_companies`;
CREATE TABLE `bol_portal_companies`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_portal_company_users
-- ----------------------------
DROP TABLE IF EXISTS `bol_portal_company_users`;
CREATE TABLE `bol_portal_company_users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `company_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_portal_departments
-- ----------------------------
DROP TABLE IF EXISTS `bol_portal_departments`;
CREATE TABLE `bol_portal_departments`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NULL DEFAULT NULL,
  `department_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_portal_designations
-- ----------------------------
DROP TABLE IF EXISTS `bol_portal_designations`;
CREATE TABLE `bol_portal_designations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NULL DEFAULT NULL,
  `sort_order` int(11) NULL DEFAULT 0,
  `designation_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_portal_employees
-- ----------------------------
DROP TABLE IF EXISTS `bol_portal_employees`;
CREATE TABLE `bol_portal_employees`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `company_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `department_id` int(11) NULL DEFAULT NULL,
  `designation_id` int(11) NULL DEFAULT NULL,
  `date_of_birth` date NULL DEFAULT NULL,
  `blood_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `otp` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phones` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `extension` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_portal_groups
-- ----------------------------
DROP TABLE IF EXISTS `bol_portal_groups`;
CREATE TABLE `bol_portal_groups`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NULL DEFAULT NULL,
  `department_ids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `employee_ids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `purpose` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_portal_notification_receivers
-- ----------------------------
DROP TABLE IF EXISTS `bol_portal_notification_receivers`;
CREATE TABLE `bol_portal_notification_receivers`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `seen` int(1) NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `notification_id`(`notification_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_portal_notifications
-- ----------------------------
DROP TABLE IF EXISTS `bol_portal_notifications`;
CREATE TABLE `bol_portal_notifications`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `company_id` int(255) NULL DEFAULT NULL,
  `created_by` int(255) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_areas
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_areas`;
CREATE TABLE `bol_tailor_areas`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district_id` int(11) NOT NULL,
  `name` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 72 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bol_tailor_areas
-- ----------------------------
INSERT INTO `bol_tailor_areas` VALUES (1, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (2, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (3, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (4, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (5, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (6, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (7, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (8, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (9, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (10, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (11, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (12, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (13, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (14, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (15, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (16, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (17, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (18, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (19, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (20, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (21, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (22, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (23, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (24, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (25, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (26, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (27, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (28, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (29, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (30, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (31, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (32, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (33, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (34, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (35, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (36, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (37, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (38, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (39, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (40, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (41, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (42, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (43, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (44, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (45, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (46, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (47, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (48, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (49, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (50, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (51, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (52, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (53, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (54, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (55, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (56, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (57, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (58, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (59, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (60, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (61, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (62, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (63, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (64, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (65, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (66, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (67, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (68, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (69, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (70, 0, '');
INSERT INTO `bol_tailor_areas` VALUES (71, 0, '');

-- ----------------------------
-- Table structure for bol_tailor_customer_addresses
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_customer_addresses`;
CREATE TABLE `bol_tailor_customer_addresses`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `area_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `district_id` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `customer_id`(`customer_id`) USING BTREE,
  CONSTRAINT `bol_tailor_customer_addresses_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `bol_tailor_customers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_customer_contacts
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_customer_contacts`;
CREATE TABLE `bol_tailor_customer_contacts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NULL DEFAULT NULL,
  `phone` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_contacts_ibfk_1`(`customer_id`) USING BTREE,
  CONSTRAINT `bol_tailor_customer_contacts_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `bol_tailor_customers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_customer_measurement_share
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_customer_measurement_share`;
CREATE TABLE `bol_tailor_customer_measurement_share`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `measurement_id` int(11) NULL DEFAULT NULL,
  `shop_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `measurement_id`(`measurement_id`) USING BTREE,
  INDEX `shop_id`(`shop_id`) USING BTREE,
  CONSTRAINT `bol_tailor_customer_measurement_share_ibfk_1` FOREIGN KEY (`measurement_id`) REFERENCES `bol_tailor_customer_measurements` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `bol_tailor_customer_measurement_share_ibfk_2` FOREIGN KEY (`shop_id`) REFERENCES `bol_tailor_shops` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_customer_measurements
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_customer_measurements`;
CREATE TABLE `bol_tailor_customer_measurements`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_item_id` int(11) NULL DEFAULT NULL,
  `customer_id` int(11) NULL DEFAULT NULL,
  `measurement` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_item_id`(`order_item_id`) USING BTREE,
  INDEX `customer_id`(`customer_id`) USING BTREE,
  CONSTRAINT `bol_tailor_customer_measurements_ibfk_1` FOREIGN KEY (`order_item_id`) REFERENCES `bol_tailor_order_items` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `bol_tailor_customer_measurements_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `bol_tailor_customers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_customer_wardrobe
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_customer_wardrobe`;
CREATE TABLE `bol_tailor_customer_wardrobe`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NULL DEFAULT NULL,
  `item_id` int(11) NULL DEFAULT NULL,
  `dress_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `dress_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `purchase_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `drawer_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `customer_id`(`customer_id`) USING BTREE,
  INDEX `category_id`(`item_id`) USING BTREE,
  CONSTRAINT `bol_tailor_customer_wardrobe_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `bol_tailor_customers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `bol_tailor_customer_wardrobe_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `bol_tailor_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_customers
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_customers`;
CREATE TABLE `bol_tailor_customers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `gender` enum('male','female') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'male',
  `email` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `latitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `longitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `otp` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `date_of_birth` date NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_districts
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_districts`;
CREATE TABLE `bol_tailor_districts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 75 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bol_tailor_districts
-- ----------------------------
INSERT INTO `bol_tailor_districts` VALUES (1, 'Barguna', 6);
INSERT INTO `bol_tailor_districts` VALUES (2, 'Barisal', 6);
INSERT INTO `bol_tailor_districts` VALUES (3, 'Bhola', 6);
INSERT INTO `bol_tailor_districts` VALUES (4, 'Jhalokati', 6);
INSERT INTO `bol_tailor_districts` VALUES (5, 'Patuakhali', 6);
INSERT INTO `bol_tailor_districts` VALUES (6, 'Pirojpur', 6);
INSERT INTO `bol_tailor_districts` VALUES (7, 'Bandarban', 2);
INSERT INTO `bol_tailor_districts` VALUES (8, 'Brahmanbaria', 2);
INSERT INTO `bol_tailor_districts` VALUES (9, 'Chandpur', 2);
INSERT INTO `bol_tailor_districts` VALUES (10, 'Chittagong', 2);
INSERT INTO `bol_tailor_districts` VALUES (11, 'Comilla', 2);
INSERT INTO `bol_tailor_districts` VALUES (12, 'Coxs Bazar', 2);
INSERT INTO `bol_tailor_districts` VALUES (13, 'Feni', 2);
INSERT INTO `bol_tailor_districts` VALUES (14, 'Khagrachari', 2);
INSERT INTO `bol_tailor_districts` VALUES (15, 'Lakshmipur', 2);
INSERT INTO `bol_tailor_districts` VALUES (16, 'Noakhali', 2);
INSERT INTO `bol_tailor_districts` VALUES (17, 'Rangamati', 2);
INSERT INTO `bol_tailor_districts` VALUES (18, 'Dhaka', 1);
INSERT INTO `bol_tailor_districts` VALUES (19, 'Faridpur', 1);
INSERT INTO `bol_tailor_districts` VALUES (20, 'Gazipur', 1);
INSERT INTO `bol_tailor_districts` VALUES (21, 'Gopalganj', 1);
INSERT INTO `bol_tailor_districts` VALUES (22, 'Jamalpur', 1);
INSERT INTO `bol_tailor_districts` VALUES (23, 'Kishoreganj', 1);
INSERT INTO `bol_tailor_districts` VALUES (24, 'Madaripur', 1);
INSERT INTO `bol_tailor_districts` VALUES (25, 'Manikganj', 1);
INSERT INTO `bol_tailor_districts` VALUES (26, 'Munshiganj', 1);
INSERT INTO `bol_tailor_districts` VALUES (27, 'Mymensingh', 1);
INSERT INTO `bol_tailor_districts` VALUES (28, 'Narayanganj', 1);
INSERT INTO `bol_tailor_districts` VALUES (29, 'Narsingdi', 1);
INSERT INTO `bol_tailor_districts` VALUES (30, 'Netrakona', 1);
INSERT INTO `bol_tailor_districts` VALUES (31, 'Rajbari', 1);
INSERT INTO `bol_tailor_districts` VALUES (32, 'Shariatpur', 1);
INSERT INTO `bol_tailor_districts` VALUES (33, 'Sherpur', 1);
INSERT INTO `bol_tailor_districts` VALUES (34, 'Tangail', 1);
INSERT INTO `bol_tailor_districts` VALUES (35, 'Bagerhat', 4);
INSERT INTO `bol_tailor_districts` VALUES (36, 'Chuadanga', 4);
INSERT INTO `bol_tailor_districts` VALUES (37, 'Jessore', 4);
INSERT INTO `bol_tailor_districts` VALUES (38, 'Jhenaidah', 4);
INSERT INTO `bol_tailor_districts` VALUES (39, 'Khulna', 4);
INSERT INTO `bol_tailor_districts` VALUES (40, 'Kushtia', 4);
INSERT INTO `bol_tailor_districts` VALUES (41, 'Satkhira', 4);
INSERT INTO `bol_tailor_districts` VALUES (42, 'Narail', 4);
INSERT INTO `bol_tailor_districts` VALUES (43, 'Magura', 4);
INSERT INTO `bol_tailor_districts` VALUES (44, 'Meherpur', 4);
INSERT INTO `bol_tailor_districts` VALUES (45, 'Bogra', 3);
INSERT INTO `bol_tailor_districts` VALUES (46, 'Dinajpur', 7);
INSERT INTO `bol_tailor_districts` VALUES (47, 'Gaibandha', 7);
INSERT INTO `bol_tailor_districts` VALUES (48, 'Joypurhat', 3);
INSERT INTO `bol_tailor_districts` VALUES (49, 'Kurigram', 7);
INSERT INTO `bol_tailor_districts` VALUES (50, 'Lalmonirhat ', 7);
INSERT INTO `bol_tailor_districts` VALUES (51, 'Naogaon', 3);
INSERT INTO `bol_tailor_districts` VALUES (52, 'Natore', 3);
INSERT INTO `bol_tailor_districts` VALUES (53, 'Nawabganj', 3);
INSERT INTO `bol_tailor_districts` VALUES (54, 'Nilphamari', 7);
INSERT INTO `bol_tailor_districts` VALUES (55, 'Pabna', 3);
INSERT INTO `bol_tailor_districts` VALUES (56, 'Panchagarh ', 7);
INSERT INTO `bol_tailor_districts` VALUES (57, 'Rajshahi', 3);
INSERT INTO `bol_tailor_districts` VALUES (58, 'Rangpur', 7);
INSERT INTO `bol_tailor_districts` VALUES (59, 'Sirajganj', 3);
INSERT INTO `bol_tailor_districts` VALUES (60, 'Thakurgaon', 7);
INSERT INTO `bol_tailor_districts` VALUES (61, 'Habiganj', 5);
INSERT INTO `bol_tailor_districts` VALUES (62, 'Maulvibazar', 5);
INSERT INTO `bol_tailor_districts` VALUES (63, 'Sunamganj', 5);
INSERT INTO `bol_tailor_districts` VALUES (64, 'Sylhet', 5);

-- ----------------------------
-- Table structure for bol_tailor_items
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_items`;
CREATE TABLE `bol_tailor_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_order_items
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_order_items`;
CREATE TABLE `bol_tailor_order_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NULL DEFAULT NULL,
  `shop_item_id` int(11) NULL DEFAULT NULL,
  `measurement_id` int(11) NULL DEFAULT NULL,
  `trial_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `sample_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_id`(`order_id`) USING BTREE,
  INDEX `shop_item_id`(`shop_item_id`) USING BTREE,
  INDEX `order_items_ibfk_2`(`measurement_id`) USING BTREE,
  CONSTRAINT `bol_tailor_order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `bol_tailor_orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `bol_tailor_order_items_ibfk_2` FOREIGN KEY (`measurement_id`) REFERENCES `bol_tailor_customer_measurements` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bol_tailor_order_items_ibfk_3` FOREIGN KEY (`shop_item_id`) REFERENCES `bol_tailor_shop_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_order_payments
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_order_payments`;
CREATE TABLE `bol_tailor_order_payments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NULL DEFAULT NULL,
  `pay_amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pay_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_id`(`order_id`) USING BTREE,
  CONSTRAINT `bol_tailor_order_payments_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `bol_tailor_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_orders
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_orders`;
CREATE TABLE `bol_tailor_orders`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NULL DEFAULT NULL,
  `shop_id` int(11) NULL DEFAULT NULL,
  `order_number` int(11) NULL DEFAULT NULL,
  `order_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `delivery_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `order_amount` int(11) NULL DEFAULT NULL,
  `discount` int(11) NULL DEFAULT NULL,
  `comments` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_sellers
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_sellers`;
CREATE TABLE `bol_tailor_sellers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `otp` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `gender` enum('male','female') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'male',
  `email` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_shop_contacts
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_shop_contacts`;
CREATE TABLE `bol_tailor_shop_contacts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NULL DEFAULT NULL,
  `phone` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `shop_id`(`shop_id`) USING BTREE,
  CONSTRAINT `bol_tailor_shop_contacts_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `bol_tailor_shops` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_shop_items
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_shop_items`;
CREATE TABLE `bol_tailor_shop_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NULL DEFAULT NULL,
  `item_id` int(11) NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `sale_price` decimal(10, 2) NULL DEFAULT NULL,
  `sale_start_date` datetime(0) NULL DEFAULT NULL,
  `sale_end_date` datetime(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `shop_id`(`shop_id`) USING BTREE,
  INDEX `item_id`(`item_id`) USING BTREE,
  CONSTRAINT `bol_tailor_shop_items_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `bol_tailor_shops` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bol_tailor_shop_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `bol_tailor_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bol_tailor_shops
-- ----------------------------
DROP TABLE IF EXISTS `bol_tailor_shops`;
CREATE TABLE `bol_tailor_shops`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NULL DEFAULT NULL,
  `shop_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `shop_logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `shop_banner` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `area_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `district_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `latitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `longitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `working_days` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `seller_id`(`seller_id`) USING BTREE,
  CONSTRAINT `bol_tailor_shops_ibfk_1` FOREIGN KEY (`seller_id`) REFERENCES `bol_tailor_sellers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cache
-- ----------------------------
DROP TABLE IF EXISTS `cache`;
CREATE TABLE `cache`  (
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE INDEX `cache_key_unique`(`key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_theme_data
-- ----------------------------
DROP TABLE IF EXISTS `cms_theme_data`;
CREATE TABLE `cms_theme_data`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `theme` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cms_theme_data_theme_index`(`theme`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_theme_logs
-- ----------------------------
DROP TABLE IF EXISTS `cms_theme_logs`;
CREATE TABLE `cms_theme_logs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `template` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `old_template` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `old_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cms_theme_logs_type_index`(`type`) USING BTREE,
  INDEX `cms_theme_logs_theme_index`(`theme`) USING BTREE,
  INDEX `cms_theme_logs_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for deferred_bindings
-- ----------------------------
DROP TABLE IF EXISTS `deferred_bindings`;
CREATE TABLE `deferred_bindings`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `master_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `deferred_bindings_master_type_index`(`master_type`) USING BTREE,
  INDEX `deferred_bindings_master_field_index`(`master_field`) USING BTREE,
  INDEX `deferred_bindings_slave_type_index`(`slave_type`) USING BTREE,
  INDEX `deferred_bindings_slave_id_index`(`slave_id`) USING BTREE,
  INDEX `deferred_bindings_session_key_index`(`session_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of deferred_bindings
-- ----------------------------
INSERT INTO `deferred_bindings` VALUES (1, 'BOL\\Portal\\Models\\Company', 'logo', 'System\\Models\\File', '1', 'TBOZA8VVGFfOvETE2EpvHaqgkj8N3qk7Gf2ZpS69', 1, '2018-08-12 09:51:56', '2018-08-12 09:51:56');
INSERT INTO `deferred_bindings` VALUES (3, 'BOL\\Portal\\Models\\Company', 'logo', 'System\\Models\\File', '3', 'g7f5LQgGHSsrQiCjUDylgFkBtfiIMLQSVkYOjmU6', 1, '2018-08-12 10:15:01', '2018-08-12 10:15:01');
INSERT INTO `deferred_bindings` VALUES (9, 'BOL\\Portal\\Models\\Company', 'company_logo', 'System\\Models\\File', '9', '5QyoauA7e84HFG6FxOhixEtEgXpPHmaGpk4y0QES', 1, '2018-08-16 06:25:41', '2018-08-16 06:25:41');
INSERT INTO `deferred_bindings` VALUES (25, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '30', '9s3ra783KpS4uxps6Yll4mKSmw4M1yVeVGWt1DL5', 1, '2018-09-05 05:13:46', '2018-09-05 05:13:46');
INSERT INTO `deferred_bindings` VALUES (26, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '31', 'BXBQZ6oI8QavPwB8hqERNSqv1sWIU5oIzCLkbGHA', 1, '2018-09-05 05:16:53', '2018-09-05 05:16:53');
INSERT INTO `deferred_bindings` VALUES (27, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '32', 'wrhZlbCPJJiH6ZwHECO7Zv6kCOzrxDHf46yX6nZ8', 1, '2018-09-05 05:18:47', '2018-09-05 05:18:47');
INSERT INTO `deferred_bindings` VALUES (28, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '33', 'NAs4rRhI3eIRdittfe3MHbLURVzKyirWIDYNhL3s', 1, '2018-09-05 05:22:05', '2018-09-05 05:22:05');
INSERT INTO `deferred_bindings` VALUES (29, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '34', 'M0y6zSRuAmdZnBVuuYfuIMRp2cTzhWNyyLzBpI8h', 1, '2018-09-05 05:22:40', '2018-09-05 05:22:40');
INSERT INTO `deferred_bindings` VALUES (30, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '35', 'nn50v9DZXAswMtJ7UvuTloDIuchQJpTa8mLDrBgc', 1, '2018-09-05 05:35:12', '2018-09-05 05:35:12');
INSERT INTO `deferred_bindings` VALUES (31, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '36', 'vS23pDJTvw7lVqVn0Xgats19rgQEJgIxoVIHPm7c', 1, '2018-09-05 05:36:28', '2018-09-05 05:36:28');
INSERT INTO `deferred_bindings` VALUES (32, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '37', 'IU6KwqcmymSJeod78qASc2CeQz9ymbHTZOfmSS4p', 1, '2018-09-05 05:36:41', '2018-09-05 05:36:41');
INSERT INTO `deferred_bindings` VALUES (33, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '38', 'sUazE8e2U53kxC6Z8cUxp83bjdVt8rLUwRqgg9sW', 1, '2018-09-05 05:38:47', '2018-09-05 05:38:47');
INSERT INTO `deferred_bindings` VALUES (34, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '39', 'M1aixfMrYVD2OufpbHG53bcl8YEiLkNI3uT2haOv', 1, '2018-09-05 05:39:23', '2018-09-05 05:39:23');
INSERT INTO `deferred_bindings` VALUES (35, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '40', 'TWXI02OVbhXEo3nmiYX6Xo8z2f0s1gsKgMmhLPUN', 1, '2018-09-05 05:39:59', '2018-09-05 05:39:59');
INSERT INTO `deferred_bindings` VALUES (36, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '41', 'TWXI02OVbhXEo3nmiYX6Xo8z2f0s1gsKgMmhLPUN', 1, '2018-09-05 05:40:06', '2018-09-05 05:40:06');
INSERT INTO `deferred_bindings` VALUES (37, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '42', 'fRPGk3di88kOcCmXfaKUsY6RRpSOaoy763ybzlXn', 1, '2018-09-05 05:41:56', '2018-09-05 05:41:56');
INSERT INTO `deferred_bindings` VALUES (38, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '43', 'e9BPHwuhHKWI0oNaiqdJCWSOHJn6ak9BDUzUV80v', 1, '2018-09-05 05:53:10', '2018-09-05 05:53:10');
INSERT INTO `deferred_bindings` VALUES (39, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '44', 'e9BPHwuhHKWI0oNaiqdJCWSOHJn6ak9BDUzUV80v', 1, '2018-09-05 05:53:45', '2018-09-05 05:53:45');
INSERT INTO `deferred_bindings` VALUES (40, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '45', 'B1UHpVyiFxMOFGkbsJUA01WuVDfUBR9wIf6DUl8g', 1, '2018-09-05 06:03:08', '2018-09-05 06:03:08');
INSERT INTO `deferred_bindings` VALUES (41, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '46', 'B1UHpVyiFxMOFGkbsJUA01WuVDfUBR9wIf6DUl8g', 1, '2018-09-05 06:04:02', '2018-09-05 06:04:02');
INSERT INTO `deferred_bindings` VALUES (42, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '47', 'OSysaqo6DKXFIP4Mgy1kvULb5qApVLjfbKGwEDmi', 1, '2018-09-05 06:04:41', '2018-09-05 06:04:41');
INSERT INTO `deferred_bindings` VALUES (43, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '48', 'FLBHKP2IKNcQ5hGkMhnjyhajtoKRWSaYDFpesC39', 1, '2018-09-05 06:08:10', '2018-09-05 06:08:10');
INSERT INTO `deferred_bindings` VALUES (44, 'BOL\\Portal\\Models\\EmployeeImport', 'import_file', 'System\\Models\\File', '49', 'rtEWSO6MwpdmqXl0ECPBM0HUyQL8ZffqWoDxq5AT', 1, '2018-09-05 10:01:18', '2018-09-05 10:01:18');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `failed_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED NULL DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jobs_queue_reserved_at_index`(`queue`, `reserved_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2013_10_01_000001_Db_Deferred_Bindings', 1);
INSERT INTO `migrations` VALUES (2, '2013_10_01_000002_Db_System_Files', 1);
INSERT INTO `migrations` VALUES (3, '2013_10_01_000003_Db_System_Plugin_Versions', 1);
INSERT INTO `migrations` VALUES (4, '2013_10_01_000004_Db_System_Plugin_History', 1);
INSERT INTO `migrations` VALUES (5, '2013_10_01_000005_Db_System_Settings', 1);
INSERT INTO `migrations` VALUES (6, '2013_10_01_000006_Db_System_Parameters', 1);
INSERT INTO `migrations` VALUES (7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1);
INSERT INTO `migrations` VALUES (8, '2013_10_01_000008_Db_System_Mail_Templates', 1);
INSERT INTO `migrations` VALUES (9, '2013_10_01_000009_Db_System_Mail_Layouts', 1);
INSERT INTO `migrations` VALUES (10, '2014_10_01_000010_Db_Jobs', 1);
INSERT INTO `migrations` VALUES (11, '2014_10_01_000011_Db_System_Event_Logs', 1);
INSERT INTO `migrations` VALUES (12, '2014_10_01_000012_Db_System_Request_Logs', 1);
INSERT INTO `migrations` VALUES (13, '2014_10_01_000013_Db_System_Sessions', 1);
INSERT INTO `migrations` VALUES (14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1);
INSERT INTO `migrations` VALUES (15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1);
INSERT INTO `migrations` VALUES (16, '2015_10_01_000016_Db_Cache', 1);
INSERT INTO `migrations` VALUES (17, '2015_10_01_000017_Db_System_Revisions', 1);
INSERT INTO `migrations` VALUES (18, '2015_10_01_000018_Db_FailedJobs', 1);
INSERT INTO `migrations` VALUES (19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1);
INSERT INTO `migrations` VALUES (20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1);
INSERT INTO `migrations` VALUES (21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1);
INSERT INTO `migrations` VALUES (22, '2017_10_01_000021_Db_System_Sessions_Update', 1);
INSERT INTO `migrations` VALUES (23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1);
INSERT INTO `migrations` VALUES (24, '2017_10_01_000023_Db_System_Mail_Partials', 1);
INSERT INTO `migrations` VALUES (25, '2013_10_01_000001_Db_Backend_Users', 2);
INSERT INTO `migrations` VALUES (26, '2013_10_01_000002_Db_Backend_User_Groups', 2);
INSERT INTO `migrations` VALUES (27, '2013_10_01_000003_Db_Backend_Users_Groups', 2);
INSERT INTO `migrations` VALUES (28, '2013_10_01_000004_Db_Backend_User_Throttle', 2);
INSERT INTO `migrations` VALUES (29, '2014_01_04_000005_Db_Backend_User_Preferences', 2);
INSERT INTO `migrations` VALUES (30, '2014_10_01_000006_Db_Backend_Access_Log', 2);
INSERT INTO `migrations` VALUES (31, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2);
INSERT INTO `migrations` VALUES (32, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2);
INSERT INTO `migrations` VALUES (33, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2);
INSERT INTO `migrations` VALUES (34, '2017_10_01_000010_Db_Backend_User_Roles', 2);
INSERT INTO `migrations` VALUES (35, '2014_10_01_000001_Db_Cms_Theme_Data', 3);
INSERT INTO `migrations` VALUES (36, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3);
INSERT INTO `migrations` VALUES (37, '2017_10_01_000003_Db_Cms_Theme_Logs', 3);

-- ----------------------------
-- Table structure for offline_snipcartshop_categories
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_categories`;
CREATE TABLE `offline_snipcartshop_categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `nest_left` int(11) NULL DEFAULT NULL,
  `nest_right` int(11) NULL DEFAULT NULL,
  `nest_depth` int(11) NULL DEFAULT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `offline_snipcartshop_categories_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of offline_snipcartshop_categories
-- ----------------------------
INSERT INTO `offline_snipcartshop_categories` VALUES (1, 'Men’s Clothing', 'Mens-Clothing', 'Men’s Clothing', 'Men’s Clothing Like Shirt, T Shirt, Underware', 0, '2018-05-23 16:36:59', '2018-05-23 16:36:59', NULL, 1, 2, 0, '321');

-- ----------------------------
-- Table structure for offline_snipcartshop_category_product
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_category_product`;
CREATE TABLE `offline_snipcartshop_category_product`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for offline_snipcartshop_discounts
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_discounts`;
CREATE TABLE `offline_snipcartshop_discounts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `guid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `total_to_reach` decimal(10, 0) NULL DEFAULT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Rate',
  `trigger` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Code',
  `rate` int(10) UNSIGNED NULL DEFAULT NULL,
  `amount` decimal(10, 0) NULL DEFAULT NULL,
  `alternate_price` decimal(10, 0) NULL DEFAULT NULL,
  `max_number_of_usages` int(10) UNSIGNED NULL DEFAULT NULL,
  `expires` datetime(0) NULL DEFAULT NULL,
  `number_of_usages` int(10) UNSIGNED NULL DEFAULT NULL,
  `number_of_usages_uncompleted` int(10) UNSIGNED NULL DEFAULT NULL,
  `shipping_description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_cost` decimal(10, 0) NULL DEFAULT NULL,
  `shipping_guaranteed_days_to_delivery` int(10) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for offline_snipcartshop_order_items
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_order_items`;
CREATE TABLE `offline_snipcartshop_order_items`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `unique_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `price` decimal(8, 2) NULL DEFAULT NULL,
  `total_price` decimal(8, 2) NULL DEFAULT NULL,
  `quantity` int(11) NULL DEFAULT NULL,
  `max_quantity` int(11) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `weight` int(11) NULL DEFAULT NULL,
  `width` int(11) NULL DEFAULT NULL,
  `length` int(11) NULL DEFAULT NULL,
  `height` int(11) NULL DEFAULT NULL,
  `total_weight` decimal(8, 2) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `stackable` tinyint(1) NULL DEFAULT NULL,
  `duplicatable` tinyint(1) NULL DEFAULT NULL,
  `shippable` tinyint(1) NULL DEFAULT NULL,
  `taxable` tinyint(1) NULL DEFAULT NULL,
  `custom_fields` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `taxes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `added_on` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for offline_snipcartshop_orders
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_orders`;
CREATE TABLE `offline_snipcartshop_orders`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `token` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `invoice_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `currency` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `creation_date` timestamp(0) NULL DEFAULT NULL,
  `modification_date` timestamp(0) NULL DEFAULT NULL,
  `completion_date` timestamp(0) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `payment_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `will_be_paid_later` tinyint(1) NULL DEFAULT NULL,
  `shipping_address_same_as_billing` tinyint(1) NULL DEFAULT NULL,
  `billing_address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `shipping_address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `credit_card_last4_digits` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tracking_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tracking_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_fees` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_provider` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_method` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `card_holder_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `card_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `payment_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `payment_gateway_used` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tax_provider` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `lang` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `refunds_amount` double(8, 2) NULL DEFAULT NULL,
  `adjusted_amount` double(8, 2) NULL DEFAULT NULL,
  `rebate_amount` double(8, 2) NULL DEFAULT NULL,
  `taxes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `items_total` decimal(8, 2) NULL DEFAULT NULL,
  `subtotal` decimal(8, 2) NULL DEFAULT NULL,
  `taxable_total` decimal(8, 2) NULL DEFAULT NULL,
  `grand_total` decimal(8, 2) NULL DEFAULT NULL,
  `total_weight` int(11) NULL DEFAULT NULL,
  `total_rebate_rate` int(11) NULL DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `custom_fields` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `shipping_enabled` tinyint(1) NULL DEFAULT NULL,
  `payment_transaction_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `metadata` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `ip_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `discounts` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `offline_snipcartshop_orders_token_unique`(`token`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for offline_snipcartshop_product_accessory
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_product_accessory`;
CREATE TABLE `offline_snipcartshop_product_accessory`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `accessory_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for offline_snipcartshop_product_custom_field_options
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_product_custom_field_options`;
CREATE TABLE `offline_snipcartshop_product_custom_field_options`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `custom_field_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NULL DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for offline_snipcartshop_product_custom_fields
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_product_custom_fields`;
CREATE TABLE `offline_snipcartshop_product_custom_fields`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for offline_snipcartshop_product_variant_custom_field_option
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_product_variant_custom_field_option`;
CREATE TABLE `offline_snipcartshop_product_variant_custom_field_option`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `variant_id` int(10) UNSIGNED NOT NULL,
  `custom_field_option_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for offline_snipcartshop_product_variants
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_product_variants`;
CREATE TABLE `offline_snipcartshop_product_variants`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `stock` int(11) NULL DEFAULT NULL,
  `allow_out_of_stock_purchases` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for offline_snipcartshop_products
-- ----------------------------
DROP TABLE IF EXISTS `offline_snipcartshop_products`;
CREATE TABLE `offline_snipcartshop_products`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_defined_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_short` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `meta_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `weight` int(10) UNSIGNED NULL DEFAULT NULL,
  `width` int(10) UNSIGNED NULL DEFAULT NULL,
  `length` int(10) UNSIGNED NULL DEFAULT NULL,
  `height` int(10) UNSIGNED NULL DEFAULT NULL,
  `quantity_default` int(10) UNSIGNED NULL DEFAULT NULL,
  `quantity_max` int(10) UNSIGNED NULL DEFAULT NULL,
  `quantity_min` int(10) UNSIGNED NULL DEFAULT NULL,
  `stock` int(11) NULL DEFAULT 0,
  `properties` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `links` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `inventory_management_method` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'single',
  `allow_out_of_stock_purchases` tinyint(1) NOT NULL DEFAULT 0,
  `stackable` tinyint(1) NOT NULL DEFAULT 1,
  `shippable` tinyint(1) NOT NULL DEFAULT 1,
  `taxable` tinyint(1) NOT NULL DEFAULT 1,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `offline_snipcartshop_products_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for rainlab_user_mail_blockers
-- ----------------------------
DROP TABLE IF EXISTS `rainlab_user_mail_blockers`;
CREATE TABLE `rainlab_user_mail_blockers`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `template` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `rainlab_user_mail_blockers_email_index`(`email`) USING BTREE,
  INDEX `rainlab_user_mail_blockers_template_index`(`template`) USING BTREE,
  INDEX `rainlab_user_mail_blockers_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions`  (
  `id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_activity` int(11) NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  UNIQUE INDEX `sessions_id_unique`(`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for system_event_logs
-- ----------------------------
DROP TABLE IF EXISTS `system_event_logs`;
CREATE TABLE `system_event_logs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `level` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `details` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `system_event_logs_level_index`(`level`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for system_files
-- ----------------------------
DROP TABLE IF EXISTS `system_files`;
CREATE TABLE `system_files`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `disk_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `field` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `attachment_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `attachment_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `system_files_field_index`(`field`) USING BTREE,
  INDEX `system_files_attachment_id_index`(`attachment_id`) USING BTREE,
  INDEX `system_files_attachment_type_index`(`attachment_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of system_files
-- ----------------------------
INSERT INTO `system_files` VALUES (5, '5b7251679dc5c912790706.png', 'logo.png', 7855, 'image/png', NULL, NULL, 'logo', '1', 'Backend\\Models\\BrandSetting', 1, 5, '2018-08-14 03:49:59', '2018-08-14 03:50:06');

-- ----------------------------
-- Table structure for system_mail_layouts
-- ----------------------------
DROP TABLE IF EXISTS `system_mail_layouts`;
CREATE TABLE `system_mail_layouts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content_html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content_css` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of system_mail_layouts
-- ----------------------------
INSERT INTO `system_mail_layouts` VALUES (1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, '2018-08-12 07:50:09', '2018-08-12 07:50:09');
INSERT INTO `system_mail_layouts` VALUES (2, 'System layout', 'system', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, '2018-08-12 07:50:10', '2018-08-12 07:50:10');
INSERT INTO `system_mail_layouts` VALUES (3, 'Email Pricing', 'email_pricing', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\r\n</head>\r\n<body>\r\n    <style type=\"text/css\" media=\"screen\">\r\n        {{ brandCss|raw }}\r\n        {{ css|raw }}\r\n    </style>\r\n\r\n    <h2 style=\"text-align:center\">Employee Portal Pricing Tables</h2>\r\n    <p style=\"text-align:center\">Dear Customer, here is our price details. If need any custom requirement and help feel free to contact with us.</p>\r\n    \r\n    <div class=\"columns\">\r\n      <ul class=\"price\">\r\n        <li class=\"header\" style=\"background-color:#512884\">Basic</li>\r\n        <li class=\"grey\">Free</li>\r\n        <li>Entry Panel</li>\r\n        <li>100 Employee Contacts</li>\r\n        <li>-</li>\r\n        <li>-</li>\r\n        <li>-</li>\r\n        <li>-</li>\r\n        <li>-</li>\r\n        <li>-</li>\r\n        <li>-</li>\r\n        <li class=\"grey\"><a href=\"{{ url(\'/\') }}\" class=\"button\">ORDER</a></li>\r\n      </ul>\r\n    </div>\r\n    \r\n    <div class=\"columns\">\r\n      <ul class=\"price\">\r\n        <li class=\"header\" style=\"background-color:#2b2b33\">Pro</li>\r\n        <li class=\"grey\">$ 500 / year</li>\r\n        <li>Entry Panel</li>\r\n        <li>1000 Employee Contacts</li>\r\n        <li>Attendence</li>\r\n        <li>Leave tracking</li>\r\n        <li>Employee Profiling</li>\r\n        <li>Employee Self-Service</li>\r\n        <li>-</li>\r\n        <li>-</li>\r\n        <li>-</li>\r\n        <li class=\"grey\"><a href=\"{{ url(\'/\') }}\" class=\"button\">ORDER</a></li>\r\n      </ul>\r\n    </div>\r\n    \r\n    <div class=\"columns\">\r\n      <ul class=\"price\">\r\n        <li class=\"header\" style=\"background-color:#04dbdd\">Premium</li>\r\n        <li class=\"grey\">$ 5100 / year</li>\r\n        <li>Entry Panel</li>\r\n        <li>Unlimited Employee Contacts</li>\r\n        <li>Attendence</li>\r\n        <li>Leave tracking</li>\r\n        <li>Employee Profiling</li>\r\n        <li>Employee Self-Service</li>\r\n        <li>HR Reporting & Chart</li>\r\n        <li>Duty Rostering</li>\r\n        <li>Tech Support</li>\r\n        <li class=\"grey\"><a href=\"{{ url(\'/\') }}\" class=\"button\">ORDER</a></li>\r\n      </ul>\r\n    </div>\r\n\r\n</body>\r\n</html>', '', '* {\r\n    box-sizing: border-box;\r\n    font-family: \'Lucida Sans\', \'Lucida Sans Regular\', \'Lucida Grande\', \'Lucida Sans Unicode\', Geneva, Verdana, sans-serif;\r\n}\r\n\r\n.columns {\r\n    float: left;\r\n        width: 28.3%;\r\n    margin-right: 10px\r\n}\r\n\r\n.price {\r\n    border-radius: 25px;\r\n    list-style-type: none;\r\n    border: 3px solid #eee;\r\n    margin: 0;\r\n    padding: 0;\r\n    -webkit-transition: 0.3s;\r\n    transition: 0.3s;\r\n}\r\n\r\n.price:hover {\r\n    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)\r\n}\r\n\r\n.price .header {\r\n    background-color: #111;\r\n    color: white;\r\n    font-size: 25px;\r\n    border-top-left-radius: 25px;\r\n    border-top-right-radius: 25px;\r\n    border-bottom: 2px solid#ffd500;\r\n    padding: 20px;\r\n}\r\n\r\n.price li {\r\n    border-bottom: 1px solid #eee;\r\n    padding: 10px;\r\n    text-align: center;\r\n    margin:0px !important;\r\n}\r\n\r\n.price .grey {\r\n    background-color: #eee;\r\n    font-size: 20px;\r\n    border-bottom-left-radius: 25px;\r\n    border-bottom-right-radius: 25px;\r\n}\r\n\r\n.button {\r\n    background-color: #4CAF50;\r\n    border: none;\r\n    color: white;\r\n    padding: 10px 25px;\r\n    text-align: center;\r\n    text-decoration: none;\r\n    font-size: 18px;\r\n    border-radius: 10px;\r\n}\r\n\r\n@media only screen and (max-width: 600px) {\r\n    .columns {\r\n        width: 100%;\r\n    }\r\n}', 0, '2018-09-03 09:08:14', '2018-09-04 05:37:15');

-- ----------------------------
-- Table structure for system_mail_partials
-- ----------------------------
DROP TABLE IF EXISTS `system_mail_partials`;
CREATE TABLE `system_mail_partials`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content_html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of system_mail_partials
-- ----------------------------
INSERT INTO `system_mail_partials` VALUES (1, 'Header', 'header', '<tr>\n    <td class=\"header\">\n        {% if url %}\n            <a href=\"{{ url }}\">\n                {{ body }}\n            </a>\n        {% else %}\n            <span>\n                {{ body }}\n            </span>\n        {% endif %}\n    </td>\n</tr>', '*** {{ body|trim }} <{{ url }}>', 0, '2018-08-28 10:07:06', '2018-08-28 10:07:06');
INSERT INTO `system_mail_partials` VALUES (2, 'Footer', 'footer', '<tr>\n    <td>\n        <table class=\"footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n            <tr>\n                <td class=\"content-cell\" align=\"center\">\n                    {{ body|md_safe }}\n                </td>\n            </tr>\n        </table>\n    </td>\n</tr>', '-------------------\n{{ body|trim }}', 0, '2018-08-28 10:07:06', '2018-08-28 10:07:06');
INSERT INTO `system_mail_partials` VALUES (3, 'Button', 'button', '<table class=\"action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td align=\"center\">\n                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                            <tr>\n                                <td>\n                                    <a href=\"{{ url }}\" class=\"button button-{{ type ?: \'primary\' }}\" target=\"_blank\">\n                                        {{ body }}\n                                    </a>\n                                </td>\n                            </tr>\n                        </table>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }} <{{ url }}>', 0, '2018-08-28 10:07:06', '2018-08-28 10:07:06');
INSERT INTO `system_mail_partials` VALUES (4, 'Panel', 'panel', '<table class=\"panel\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td class=\"panel-content\">\n            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td class=\"panel-item\">\n                        {{ body|md_safe }}\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2018-08-28 10:07:06', '2018-08-28 10:07:06');
INSERT INTO `system_mail_partials` VALUES (5, 'Table', 'table', '<div class=\"table\">\n    {{ body|md_safe }}\n</div>', '{{ body|trim }}', 0, '2018-08-28 10:07:06', '2018-08-28 10:07:06');
INSERT INTO `system_mail_partials` VALUES (6, 'Subcopy', 'subcopy', '<table class=\"subcopy\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td>\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '-----\n{{ body|trim }}', 0, '2018-08-28 10:07:06', '2018-08-28 10:07:06');
INSERT INTO `system_mail_partials` VALUES (7, 'Promotion', 'promotion', '<table class=\"promotion\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2018-08-28 10:07:06', '2018-08-28 10:07:06');

-- ----------------------------
-- Table structure for system_mail_templates
-- ----------------------------
DROP TABLE IF EXISTS `system_mail_templates`;
CREATE TABLE `system_mail_templates`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content_html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `layout_id` int(11) NULL DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `system_mail_templates_layout_id_index`(`layout_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of system_mail_templates
-- ----------------------------
INSERT INTO `system_mail_templates` VALUES (1, 'backend::mail.invite', 'Welcome to {{ appName }}', 'Invite to site and send product price', 'Hi {{ name }}\r\n\r\nA user account has been created for you on **{{ appName }}**.\r\n\r\n{% partial \'panel\' body %}\r\n- Login: `{{ login ?: \'sample\' }}`\r\n- Password: `{{ password ?: \'********\' }}`\r\n{% endpartial %}\r\n\r\nYou can use the following link to sign in:\r\n\r\n{% partial \'button\' url=link body %}\r\n    Sign in to admin area\r\n{% endpartial %}\r\n\r\nAfter signing in you should change your password by clicking your name on the top right corner of the administration area.', '', 2, 1, '2018-08-28 10:07:06', '2018-09-23 11:02:32');
INSERT INTO `system_mail_templates` VALUES (2, 'backend::mail.restore', NULL, 'Reset an admin password', NULL, NULL, 2, 0, '2018-08-28 10:07:06', '2018-08-28 10:07:06');
INSERT INTO `system_mail_templates` VALUES (3, 'backend::mail.otp', 'OTP for login {{ appName }}', 'A template for send otp to registered user.', 'Hi {{ employee_name ?: \'Employee\' }},\r\n\r\nPlease use the following OTP to login into **{{ appName }}** mobile application.\r\n\r\n\r\n{% partial \'promotion\' body %}\r\n**OTP: `{{ otp ?: \'000000\' }}`**\r\n{% endpartial %}\r\n\r\n\r\nBest Regards,\r\n\r\n**{{ appName }} Team**', '', 2, 1, '2018-08-28 11:31:06', '2018-08-29 03:54:32');
INSERT INTO `system_mail_templates` VALUES (4, 'backend::mail.pricing', 'Welcome to {{ appName }}', 'Invite to site and send product price', '&nbsp;', '', 3, 1, '2018-09-03 09:18:05', '2018-09-04 05:37:38');

-- ----------------------------
-- Table structure for system_parameters
-- ----------------------------
DROP TABLE IF EXISTS `system_parameters`;
CREATE TABLE `system_parameters`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `namespace` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `item_index`(`namespace`, `group`, `item`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of system_parameters
-- ----------------------------
INSERT INTO `system_parameters` VALUES (1, 'system', 'update', 'count', '0');
INSERT INTO `system_parameters` VALUES (2, 'system', 'core', 'hash', '\"d4a4e1f641e333ff5c26037f86cfe619\"');
INSERT INTO `system_parameters` VALUES (3, 'system', 'core', 'build', '\"437\"');
INSERT INTO `system_parameters` VALUES (4, 'system', 'update', 'retry', '1537866253');
INSERT INTO `system_parameters` VALUES (5, 'backend', 'reportwidgets', 'default.dashboard', '{\"welcome\":{\"class\":\"Backend\\\\ReportWidgets\\\\Welcome\",\"sortOrder\":50,\"configuration\":{\"ocWidgetWidth\":6}},\"systemStatus\":{\"class\":\"System\\\\ReportWidgets\\\\Status\",\"sortOrder\":60,\"configuration\":{\"ocWidgetWidth\":6}},\"activeTheme\":{\"class\":\"Cms\\\\ReportWidgets\\\\ActiveTheme\",\"sortOrder\":70,\"configuration\":{\"ocWidgetWidth\":4}}}');

-- ----------------------------
-- Table structure for system_plugin_history
-- ----------------------------
DROP TABLE IF EXISTS `system_plugin_history`;
CREATE TABLE `system_plugin_history`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `system_plugin_history_code_index`(`code`) USING BTREE,
  INDEX `system_plugin_history_type_index`(`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for system_plugin_versions
-- ----------------------------
DROP TABLE IF EXISTS `system_plugin_versions`;
CREATE TABLE `system_plugin_versions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_frozen` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `system_plugin_versions_code_index`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for system_request_logs
-- ----------------------------
DROP TABLE IF EXISTS `system_request_logs`;
CREATE TABLE `system_request_logs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status_code` int(11) NULL DEFAULT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `referer` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for system_revisions
-- ----------------------------
DROP TABLE IF EXISTS `system_revisions`;
CREATE TABLE `system_revisions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `field` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `cast` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `old_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `new_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revisionable_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `system_revisions_revisionable_id_revisionable_type_index`(`revisionable_id`, `revisionable_type`) USING BTREE,
  INDEX `system_revisions_user_id_index`(`user_id`) USING BTREE,
  INDEX `system_revisions_field_index`(`field`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for system_settings
-- ----------------------------
DROP TABLE IF EXISTS `system_settings`;
CREATE TABLE `system_settings`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `value` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `system_settings_item_index`(`item`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of system_settings
-- ----------------------------
INSERT INTO `system_settings` VALUES (1, 'backend_brand_settings', '{\"app_name\":\"EmployeePortal\",\"app_tagline\":\"One stop solution for managing Organization\'s People.\",\"primary_color\":\"#34495e\",\"secondary_color\":\"#e67e22\",\"accent_color\":\"#3498db\",\"menu_mode\":\"inline\",\"custom_css\":\"\"}');
INSERT INTO `system_settings` VALUES (2, 'system_mail_settings', '{\"send_mode\":\"smtp\",\"sender_name\":\"EmployeePortal\",\"sender_email\":\"noreply@employeeportal.com\",\"sendmail_path\":\"\\/usr\\/sbin\\/sendmail -bs\",\"smtp_address\":\"smtp.gmail.com\",\"smtp_port\":\"587\",\"smtp_user\":\"developer@bol-online.com\",\"smtp_password\":\"mcd@@d3v3loper\",\"smtp_authorization\":\"1\",\"smtp_encryption\":\"tls\",\"mailgun_domain\":\"\",\"mailgun_secret\":\"\",\"mandrill_secret\":\"\",\"ses_key\":\"\",\"ses_secret\":\"\",\"ses_region\":\"\"}');
INSERT INTO `system_settings` VALUES (3, 'bol_portal_settings', NULL);

SET FOREIGN_KEY_CHECKS = 1;
