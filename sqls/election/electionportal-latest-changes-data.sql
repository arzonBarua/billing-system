-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 07, 2018 at 01:19 AM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `electionportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_agent`
--

CREATE TABLE `bol_election_agent` (
  `id` int(11) NOT NULL,
  `electioncenter_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_agent`
--

INSERT INTO `bol_election_agent` (`id`, `electioncenter_id`, `people_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2018-10-03 04:47:55', '2018-10-03 04:47:55');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_designations`
--

CREATE TABLE `bol_election_designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `bol_election_designations`
--

INSERT INTO `bol_election_designations` (`id`, `name`, `organization_id`, `sort_order`, `created_at`, `updated_at`) VALUES
(18, 'General Secretory', 3, 18, '2018-10-02 05:41:48', '2018-10-02 05:41:48'),
(19, 'Assistant Commissioner', 4, 19, '2018-10-04 04:53:34', '2018-10-04 04:53:34'),
(20, 'Upazilla health officer', 4, 20, '2018-10-04 04:53:45', '2018-10-04 04:53:45'),
(21, 'Upazilla Agriculture officer', 4, 21, '2018-10-04 04:53:55', '2018-10-04 04:53:55'),
(22, 'Officer Incharge', 4, 22, '2018-10-04 04:54:07', '2018-10-04 04:54:07'),
(23, 'Upazilla Engineer', 4, 23, '2018-10-04 04:54:28', '2018-10-04 04:54:28'),
(24, 'Upazilla Family Planning Officer', 4, 24, '2018-10-04 04:54:38', '2018-10-04 04:54:38'),
(25, 'Upazilla Project Officer', 4, 25, '2018-10-04 04:54:46', '2018-10-04 04:54:46'),
(26, 'Upazilla Engineer, Health', 4, 26, '2018-10-04 04:54:54', '2018-10-04 04:54:54'),
(27, 'Upazilla Chairman', 4, 27, '2018-10-04 04:55:01', '2018-10-04 04:55:01'),
(28, 'Upazilla Vice Chairman', 4, 28, '2018-10-04 04:55:11', '2018-10-04 04:55:11'),
(29, 'Upazilla Vice Chairman, Female', 4, 29, '2018-10-04 04:55:18', '2018-10-04 04:55:18'),
(30, 'Union Chariman', 4, 30, '2018-10-04 04:55:31', '2018-10-04 04:55:31'),
(31, 'Union Member', 4, 31, '2018-10-04 04:55:39', '2018-10-04 04:55:39'),
(32, 'Executive Member', 3, 32, '2018-10-04 06:58:09', '2018-10-04 06:58:09'),
(33, 'UNO- Nawabganj', 4, 33, '2018-10-06 05:50:00', '2018-10-06 06:02:45'),
(34, 'AC Land', 4, 34, '2018-10-06 06:04:53', '2018-10-06 06:04:53'),
(35, 'ASP', 4, 35, '2018-10-06 06:24:14', '2018-10-06 06:24:29'),
(36, 'Upozila Health and Family Planning Officer', 4, 36, '2018-10-06 06:28:07', '2018-10-06 06:28:07'),
(37, 'Agricultural Officer', 4, 37, '2018-10-06 06:32:53', '2018-10-06 06:32:53'),
(38, 'Agricultural Extension Officer', 4, 38, '2018-10-06 06:34:35', '2018-10-06 06:34:35'),
(39, 'Fisheries Officer', 4, 39, '2018-10-06 06:36:56', '2018-10-06 06:36:56'),
(40, 'Livestock Officer', 4, 40, '2018-10-06 06:38:22', '2018-10-06 06:38:22'),
(41, 'Veterinary Surgeon', 4, 41, '2018-10-06 06:40:03', '2018-10-06 06:40:03'),
(42, 'Engineer, LGED', 4, 42, '2018-10-06 06:41:51', '2018-10-06 06:41:51'),
(43, 'Asst. Programmer', 4, 43, '2018-10-06 06:44:30', '2018-10-06 06:44:30'),
(44, 'Asst. Engineer, LGED', 4, 44, '2018-10-06 06:45:40', '2018-10-06 06:45:40'),
(45, 'Education Officer', 4, 45, '2018-10-06 06:46:57', '2018-10-06 06:46:57'),
(46, 'Social Service Officer', 4, 46, '2018-10-06 06:48:31', '2018-10-06 06:48:31'),
(47, 'UNO- Dohar', 4, 47, '2018-10-06 06:50:49', '2018-10-06 06:50:49'),
(48, 'Accounts Officer', 4, 48, '2018-10-06 06:51:41', '2018-10-06 06:51:41'),
(49, 'AC Land, Dohar', 4, 49, '2018-10-06 06:54:37', '2018-10-06 06:54:37'),
(50, 'OC, Dohar', 5, 50, '2018-10-06 06:56:49', '2018-10-06 06:56:49'),
(51, 'Election Officer', 4, 51, '2018-10-06 06:57:08', '2018-10-06 06:57:08'),
(52, 'Food Controller', 4, 52, NULL, NULL),
(53, 'Statistics Officer', 4, 53, NULL, NULL),
(54, 'Asst. Engineer, Public Health', 4, 54, NULL, NULL),
(55, 'Deputy Asst. Engineer, Public Health', 4, 55, NULL, NULL),
(56, 'Family Planning Officer', 4, 56, NULL, NULL),
(57, 'Youth Development Officer', 4, 57, NULL, NULL),
(58, 'Women affairs officer', 4, 58, NULL, NULL),
(59, 'Cooperative Officer', 4, 59, NULL, NULL),
(60, 'Rural Development Officer', 4, 60, NULL, NULL),
(61, 'Ansar & VDP Officer', 4, 61, NULL, NULL),
(62, 'Instructor, Resource Center,', 4, 62, NULL, NULL),
(63, 'Forest Officer', 4, 63, NULL, NULL),
(64, 'Sub- Register', 4, 64, NULL, NULL),
(65, 'Poverty Alleviation Officer,', 4, 65, NULL, NULL),
(66, 'Settlement Officer', 4, 66, NULL, NULL),
(67, 'Sr. GM, Dhaka Palli Bidyut Samiti-2', 4, 67, NULL, NULL),
(68, 'AGM (MS), Dhaka Palli Bidyut Samiti-2', 4, 68, NULL, NULL),
(69, 'AGM (O&M)', 4, 69, NULL, NULL),
(70, 'AGM (E&C)', 4, 70, NULL, NULL),
(71, 'AGM (Finance)', 4, 71, NULL, NULL),
(72, 'Deputy Asst. Engineer (BADC)(Acting)', 4, 72, NULL, NULL),
(73, 'Divisional Engineer, TNT (Keraniganj)', 4, 73, NULL, NULL),
(74, 'Project Implementation Officer (BRDB)', 4, 74, NULL, NULL),
(75, 'Coordinator, One house one farm', 4, 76, NULL, NULL),
(76, 'Asst. Engineer (BADC)', 4, 79, NULL, NULL),
(77, 'OC, Nawabganj', 4, 80, NULL, NULL),
(78, 'OC- Inquiry, Nawabganj', 4, 81, NULL, NULL),
(79, 'Project Implementation Officer', 4, 82, NULL, NULL),
(80, 'Secondary Education Officer', 4, 83, NULL, NULL),
(81, 'Academic Supervisor Secondary Office', 4, 84, NULL, NULL),
(82, 'Election Officer', 4, 85, NULL, NULL),
(83, 'Upozila Engineer', 4, 83, '2018-10-06 07:20:20', '2018-10-06 07:20:20'),
(84, 'Upozila Family Planning Officer', 4, 84, '2018-10-06 07:21:39', '2018-10-06 07:21:39'),
(85, 'Deputy Asst. Enginner, Public health', 4, 85, '2018-10-06 07:23:41', '2018-10-06 07:23:41'),
(86, 'Coordinator, One house one farm, Dohar', 4, 86, '2018-10-06 07:28:56', '2018-10-06 07:28:56'),
(87, 'Instructor, Resource Center', 4, 87, '2018-10-06 07:33:39', '2018-10-06 07:33:39'),
(88, 'Poverty Alleviation Officer', 4, 88, '2018-10-06 07:35:34', '2018-10-06 07:35:34'),
(89, 'Poverty Alleviation Officer', 4, 89, '2018-10-06 07:37:15', '2018-10-06 07:37:15'),
(90, 'Project Officer', 4, 90, '2018-10-06 08:15:03', '2018-10-06 08:15:03'),
(91, 'Project Officer, BRDB', 4, 91, '2018-10-06 08:16:16', '2018-10-06 08:16:16'),
(92, 'Liaison Officer', 4, 92, '2018-10-06 08:27:05', '2018-10-06 08:27:05');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_districts`
--

CREATE TABLE `bol_election_districts` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_districts`
--

INSERT INTO `bol_election_districts` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Barguna', NULL, NULL),
(2, 'Barisal', NULL, NULL),
(3, 'Bhola', NULL, NULL),
(4, 'Jhalokati', NULL, NULL),
(5, 'Patuakhali', NULL, NULL),
(6, 'Pirojpur', NULL, NULL),
(7, 'Bandarban', NULL, NULL),
(8, 'Brahmanbaria', NULL, NULL),
(9, 'Chandpur', NULL, NULL),
(10, 'Chittagong', NULL, NULL),
(11, 'Comilla', NULL, NULL),
(12, 'Coxs Bazar', NULL, NULL),
(13, 'Feni', NULL, NULL),
(14, 'Khagrachari', NULL, NULL),
(15, 'Lakshmipur', NULL, NULL),
(16, 'Noakhali', NULL, NULL),
(17, 'Rangamati', NULL, NULL),
(18, 'Dhaka', NULL, NULL),
(19, 'Faridpur', NULL, NULL),
(20, 'Gazipur', NULL, NULL),
(21, 'Gopalganj', NULL, NULL),
(22, 'Jamalpur', NULL, NULL),
(23, 'Kishoreganj', NULL, NULL),
(24, 'Madaripur', NULL, NULL),
(25, 'Manikganj', NULL, NULL),
(26, 'Munshiganj', NULL, NULL),
(27, 'Mymensingh', NULL, NULL),
(28, 'Narayanganj', NULL, NULL),
(29, 'Narsingdi', NULL, NULL),
(30, 'Netrakona', NULL, NULL),
(31, 'Rajbari', NULL, NULL),
(32, 'Shariatpur', NULL, NULL),
(33, 'Sherpur', NULL, NULL),
(34, 'Tangail', NULL, NULL),
(35, 'Bagerhat', NULL, NULL),
(36, 'Chuadanga', NULL, NULL),
(37, 'Jessore', NULL, NULL),
(38, 'Jhenaidah', NULL, NULL),
(39, 'Khulna', NULL, NULL),
(40, 'Kushtia', NULL, NULL),
(41, 'Satkhira', NULL, NULL),
(42, 'Narail', NULL, NULL),
(43, 'Magura', NULL, NULL),
(44, 'Meherpur', NULL, NULL),
(45, 'Bogra', NULL, NULL),
(46, 'Dinajpur', NULL, NULL),
(47, 'Gaibandha', NULL, NULL),
(48, 'Joypurhat', NULL, NULL),
(49, 'Kurigram', NULL, NULL),
(50, 'Lalmonirhat ', NULL, NULL),
(51, 'Naogaon', NULL, NULL),
(52, 'Natore', NULL, NULL),
(53, 'Nawabganj', NULL, NULL),
(54, 'Nilphamari', NULL, NULL),
(55, 'Pabna', NULL, NULL),
(56, 'Panchagarh ', NULL, NULL),
(57, 'Rajshahi', NULL, NULL),
(58, 'Rangpur', NULL, NULL),
(59, 'Sirajganj', NULL, NULL),
(60, 'Thakurgaon', NULL, NULL),
(61, 'Habiganj', NULL, NULL),
(62, 'Maulvibazar', NULL, NULL),
(63, 'Sunamganj', NULL, NULL),
(64, 'Sylhet', NULL, NULL),
(77, 'test 1', '2018-10-02 06:24:13', '2018-10-02 06:24:13');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_election_area`
--

CREATE TABLE `bol_election_election_area` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazila_ids` text COLLATE utf8_unicode_ci,
  `union_ids` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_election_area`
--

INSERT INTO `bol_election_election_area` (`id`, `name`, `district_id`, `upazila_ids`, `union_ids`, `created_at`, `updated_at`) VALUES
(3, 'Dhaka 1', 18, '["6","5"]', '["6","5","7","8","9","10","11","12","27","13","14","15","16","17","18","19","20","21","22","23","24","25","26"]', '2018-10-03 14:05:55', '2018-10-06 13:04:53');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_election_center`
--

CREATE TABLE `bol_election_election_center` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `local_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_room` int(11) DEFAULT NULL,
  `male_voter` int(11) DEFAULT NULL,
  `female_voter` int(11) DEFAULT NULL,
  `electionarea_id` int(11) NOT NULL,
  `union_id` text COLLATE utf8_unicode_ci,
  `village_ids` text COLLATE utf8_unicode_ci,
  `center_number` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_election_center`
--

INSERT INTO `bol_election_election_center` (`id`, `name`, `local_name`, `total_room`, `male_voter`, `female_voter`, `electionarea_id`, `union_id`, `village_ids`, `center_number`, `created_at`, `updated_at`) VALUES
(1, 'Orongabad govt. Primary school', '1. North Arongabad: M-399: F- 3642. South Arongabad: M-329: F-321 3.Nosratpur:M-169: F-1694.Moddho Orongabad: M-477: F-420', 5, 1374, 1274, 3, '5', '["1","2","3","389"]', 0, '2018-10-05 18:00:00', '2018-10-06 10:29:16'),
(2, 'Dhoyair Modinatul Ulum Kowmiya Madrasha', '1. Pankunda: M-663: F-646', 3, 663, 646, 3, '5', '["4"]', 0, '2018-10-05 18:00:00', '2018-10-06 10:33:43'),
(3, 'Bahra Govt. Primary School (Bahra)', '1. Hatni: M-44: F-652. Char Hatni: M-24: F-233. Bahra: M-1265: F-1256', 5, 1333, 1334, 3, '5', '["5","390","391"]', 0, '2018-10-05 18:00:00', '2018-10-06 10:39:33'),
(4, 'Hatni Govt. Primary School (Hatni)', '1.Asta: M-1192: F-1197', 3, 1192, 1197, 3, '5', '["6"]', 0, '2018-10-05 18:00:00', '2018-10-06 10:40:52'),
(5, 'Dhoyair\n Govt. Primary School (Dhoyair) (Male and Female voter)', '1.West Dhoyair: M-769: F-722\n2. Middle Dhoyair: M-650: F-641\n', 6, 1419, 1363, 3, '5', '["7":"8"]', 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(6, 'East Dhoyair Private Primary School (East Dhoyair)', '1. East Dhoyair: M-741: F-768', 3, 741, 768, 3, '5', '["9"]', 0, '2018-10-05 18:00:00', '2018-10-06 10:56:34'),
(7, 'Shilakotha Madrasha: (Male voter): Center -1', '1. North shilakotha: M-8302. South Shilakotha:M-13763. Deovogh: M-95', 4, 2301, 0, 3, '6', '["10","11","12"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:17:29'),
(8, 'Shilakotha Madrasha: (Female voter): Center -2', '1. North shilakotha: F-9312. South Shilakotha: F-14313. Deovogh: F-87', 4, 0, 2449, 3, '6', '["10","11","12"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:16:53'),
(9, 'Shilakotha Govt. Primary School', '1. Charpurulia: M-931: F- 1742. Sundharipara: M- 1005: F-1125', 5, 1175, 1299, 3, '6', '["13","14"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:18:16'),
(10, 'Kartikpur High School', '1. Kartikpur: M-928: F-9842. Kusumhati: M-32: F-34', 4, 960, 1018, 3, '6', '["15","392"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:19:09'),
(11, 'Basta Etimkhana & Madrasha', '1. Boro Rasta Part (Part of 5no Ward): M-697: F-8052. Boro Rasta Part (Part of 6no Ward): M- 409: F-4833. Choto Rasta: M-131: F-1284. Babur Dangi: M-171: F-170', 6, 1408, 1586, 3, '6', '["20","17","18","19"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:21:45'),
(12, 'Kusumhati Family Planning Center (Male voter): Center -1', '1. Charkushai (part of ward no 7): M-11012. Charkushai (part of 8 no ward): M-5633. Joymangal (Puspakhali): M-222', 4, 1886, 0, 3, '6', '["393"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:22:59'),
(13, 'Kusumhati Family Planning Center (Female voter): Center -2', '1. Charkushai (part of ward no 7): F-11492. Charkushai (part of 8 no ward): F-6253. Joymangal (Puspakhali):F-222', 4, 0, 1996, 3, '6', '["23","21","393"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:23:35'),
(14, 'Awliabad govt. Primary School', '1. Awliyabad: M-369: F- 3962. Arita: M-252: F-2923. Imamnagar: M-188: F- 2194. Mahtabnagar: M-170: F-180', 4, 979, 1087, 3, '6', '["26","27","28","25"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:24:16'),
(15, 'Ikrashi Adarsha High School (Male voter): Center -1', '1. Ikrashi  north: M- 8312. Ikrashi south: M-1061', 3, 1892, 0, 3, '7', '["29","394"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:28:39'),
(16, 'ikrashi Adarsha High School (Female voter): Center -2', '1. Ikrashi  north: F-9032. Ikrashi south: F-1149', 4, 0, 2052, 3, '7', '["29","394"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:29:05'),
(17, 'Palamganj Govt. Primary School', '1. Palamganj: M-307: F-2712. Haturpara: M-138: F-492', 4, 897, 934, 3, '7', '["31","32"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:29:46'),
(18, 'Jamalchar Govt. Primary School', '1. Roghudebpur: M-134: F-1392. Karimganj: M-74: F-1083. Jamalchar: M- 438: F-457', 3, 646, 704, 3, '7', '["33","34","35"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:30:24'),
(19, 'Raipara Govt. Primary School (Male voter): Center-1', '1. Raipara north: M-13832. Raipara south: M- 918', 4, 2301, 0, 3, '7', '["36","395"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:31:48'),
(20, 'Raipara Govt. Primary School(Female voter): center -2', '1. Raipara north: F-13912. Raipara south: F-930', 4, 0, 2321, 3, '7', '["36","395"]', 0, '2018-10-05 18:00:00', '2018-10-06 11:32:05'),
(21, 'Lotakhola Govt. Primary School', '1. Nagorkanda: M-657: F-6802. Lotakhola Biler Par: M-810: F-804', 6, 1467, 1484, 3, '27', '["397","396"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:35:02'),
(22, 'Lotakhola Gigh School', '1. West Lota khola: M-667: F-7032. Moddho Lotakhola: M-661: F-667', 5, 1328, 1370, 3, '27', '["399","398"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:35:59'),
(23, 'Lotakhola Ajhar Ali Memorial High School (Male Voter): Center -1', '1. East Lotakhola: M-7832. North Char Joypara: M -6723. South Char Joypara: M- 815', 4, 2170, 0, 3, '27', '["400","401","402"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:38:49'),
(24, 'Lotakhola Ajhar Ali Memorial High School (Female Voter): Center -2', '1. East Lotakhola: F-7392. North Char Joypara: M -5593. South Char Joypara: M- 739', 4, 0, 2037, 3, '27', '["400","401","402"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:39:45'),
(25, 'Joypara Khalpar govt. Primary School (North Joypara: Khalpar)', '1. Kathalighata: M-327: F-3772. Khalpar (north part of the Khal): M-1118: F-11803. North Joypara Khal(North part of the Khal): M-64: F-71', 6, 1509, 1628, 3, '27', '["403","404","405"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:43:17'),
(26, 'Islampur govt primary school', '1. Islampur:M-649: M-6832. Khalpar (South part of Khal): M-266: F-276', 4, 915, 959, 3, '27', '["406","404"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:44:27'),
(27, 'Joypara Model govt. Primary School (Joypara Chowdhurypara)', '1. North Joypara khalpar (South part of khal): M- 590: F-5842. North Joypara gajikanda: M-245: F-2713. North Joypara Miyapara: M-369: F-3854. North Joypara Byangarchar: M- 308: F-319', 6, 1512, 1559, 3, '27', '["411","409","408","410"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:45:56'),
(28, 'Joypara College (South Joypara Kharakanda)', '1. North Joypara: M-455: F-4412. North Joypara Chowdhurypara: M-661: F-7063.  North Joypara Kutibari: M-379: F-360', 6, 1495, 1507, 3, '27', '["412","413"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:47:32'),
(29, 'Boit govt. Primary School', '1. Botia: M-556: F-5712. Nurpur (west side of WAPDA road ): M-378: F- 3503. South Joypara Gangpar (West side of WAPDA Road): M- 462: F- 417', 6, 1396, 1338, 3, '27', '["414","415","416"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:48:42'),
(30, 'Joypara Pilot High School (Male voter): center -1', '1. South Joypara ghona: M-3612. charlotakhola: M- 15843. South Joypara majhipara : M-667: 4. South Joypara gangpar (East side of WAPDA road): M-601 5. Murpur (East side of WAPDA road): M -242', 7, 3455, 0, 3, '27', '["418","420","416","417","419"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:52:19'),
(31, 'Joypara \nPilot High School (Joypara)(Female voter): center -2', '1. South Joypara ghona: F-327\n2. charlotakhola: F-1524\n3. South Joypara majhipara : F- 671: \n4. South Joypara gangpar (East side of WAPDA road): F-637\n 5. Murpur (East side of WAPDA road): F-227', 7, 0, 3436, 3, '8', '', 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(32, 'Begum Ayesh Pilot Girls High School', '1. South Joypara Kharakanda: M-1068: F-10472. South Joypara: M -454: F-471', 6, 1522, 1518, 3, '27', '["422","421"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:53:32'),
(33, 'Yousufpur govt Primary School', '1. Loskorkanda: M-273: F-3042. Rasulpur: M-164: F-1603. North Yousufpur: M- 751: F- 7874. South Yousufpur: M- 358: F- 350', 6, 1546, 1601, 3, '27', '["423","425","424","426"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:54:19'),
(34, 'Katakhaligovt. Primary School', '1. Katakhali M-707: F-7732. Nikra (West part)M- 344: F-329)', 4, 1051, 1102, 3, '8', '["44","427"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:55:46'),
(35, 'Ghata-2 govt. primary school', '1. Banaghata(West part): M- 1092: F-11652. Banaghata (East part)M-76: F-533. Nikra (East part): M-165: F-163', 5, 1333, 1381, 3, '8', '["428","46","429"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:57:01'),
(36, 'Ghata -1 govt. primary school (Male voter): center-1', '1. Dohar ghata: M-1875', 3, 1875, 0, 3, '8', '["47"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:57:41'),
(37, 'Ghata -1 govt. primary school (Female voter): center-2', '1. Dohar ghata: F- 1955', 4, 0, 1955, 3, '8', '["47"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:57:52'),
(38, 'Dohar govt. primary school', '1. Dohar: M- 920: F-1010', 4, 920, 1010, 3, '8', '["48"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:58:15'),
(39, 'Kalichar Mogoljan govt. primary school', '1. Kajirchar: M-599: F-6212. Modhurchar (east part): M- 412: F- 435', 4, 1011, 1056, 3, '8', '["51","49"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:58:40'),
(40, 'Hajrat Bilal () Madrasha (Principal Sattar Saheb Madrasha: West Sutarpar)', '1. West Sutarpara: M-1090: F-1213', 5, 1090, 1213, 3, '8', '["50"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:59:13'),
(41, 'Sutarpara abdul Hamed High School', '1. Gajirtek: M- 742: F- 857', 3, 742, 857, 3, '8', '["53"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:59:38'),
(42, 'Sutarpara Late Shamsul Haque Forkaniya Madrasha', '1. East Sutarpara (west part): M-525: F- 6052. East Sutarpara (East part): M- 403: F- 405', 4, 928, 1010, 3, '8', '["52"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:00:17'),
(43, 'Uttar Modhurchar govt. Primary School (Male voter): Center-1', 'Modhurchar (West part): M-2357', 4, 2357, 0, 3, '8', '["49"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:01:10'),
(44, 'Uttar Modhurchar govt. Primary Schoo (Female voter): Center-2', 'Modhurchar (West part): F- 2235', 4, 0, 2235, 3, '8', '["49"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:01:37'),
(45, 'Maruapota govt. Primary School', '1. Daiyarkum: M-440: F-4862. daiya Gojariya: M- 600: F- 6473. Gharmora: M- 274: F- 2714. Munshikanda: M- 253: F- 249', 6, 1567, 1753, 3, '8', '["55","54","57","58"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:05:13'),
(46, 'Jhanki govt. Primary School', '1. Jhanki: M-1452: F- 1626', 6, 1452, 1626, 3, '9', '["430"]', 0, '2018-10-05 18:00:00', '2018-10-06 12:34:28'),
(47, 'Shimuliya govt primary school', '1. Uttar shimuliya: M-1539: F- 1724', 6, 1539, 1724, 3, '9', '["431"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:03:04'),
(48, 'Malikanda High School (Male voter): Center-1', '1. Malikanda: M-1885', 4, 1885, 0, 3, '9', '["432"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:03:56'),
(49, 'Malikanda High School (Female voter): Center-2', '1. Malikanda: F-1901', 4, 0, 1901, 3, '9', '["432"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:28:13'),
(50, 'Meghula govt. Primary School', '1. Meghula: M- 1639: F-1512', 6, 1639, 1512, 3, '9', '["433"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:29:01'),
(51, 'South Shimuliya govt Primary School (Male voter): Center-1', '1. South Shimuliya: M- 1799', 4, 1799, 0, 3, '9', '["434"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:29:33'),
(52, 'South Shimuliya govt Primary School (Female voter): Center-2', '1. South Shimuliya: F-1998', 4, 0, 1998, 3, '9', '["434"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:29:59'),
(53, 'Narisha Pashimchar govt. Primary School (Male voter): Center -1', '1. Narisha Paschimchar: M-2708', 5, 2708, 0, 3, '9', '["435"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:30:42'),
(54, 'Narisha Pashimchar govt. Primary School (Female voter): Center -2', '1. Narisha Paschimchar: F-2736', 5, 0, 2736, 3, '9', '["435"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:30:58'),
(55, 'Narisha Gilrs High School', '1. Narisha: M-973: F-1072', 4, 973, 1072, 3, '9', '["436"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:31:28'),
(56, 'Narisha Khalpar Govt. Primary School', '1. Narisha Khalpar: M-790: F- 8432. Narisha Choitabator- M- 559: F- 623', 0, 1349, 1466, 3, '9', '["437","438"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:31:58'),
(57, 'Satvita govt.Primary School', '1. Ruikha: M- 187: F- 2142. Satvita: M- 1031: F- 1085', 5, 1218, 1299, 3, '9', '["439","440"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:32:37'),
(58, 'Purbachar govt. Primary School', '1. Purbachar: M- 313: F-3022. Mokshudpur (Part of 1 no ward): M-442: F-481', 3, 755, 783, 3, '10', '["59","455"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:32:36'),
(59, 'Mokshudpur Samsuddin Shikder High School', '1. Mokshudpur (part of 2 no Ward): M- 721: F- 7502. Mokshudpur (Part of 3 no ward): M -802: F- 916', 6, 1523, 1666, 3, '10', '["60","61"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:33:45'),
(60, 'Padma Mohabiddyaloy', '1. Khariya North: M-291: F-3042. Dhalarpar: M-447: F-4683. Khariya South: M- 186: F-1874. Ultadangi (shantinagar): M- 124: F- 1335. Gorabon: M-172: F- 121', 4, 1220, 1213, 3, '10', '["62","457","63","456","68"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:34:56'),
(61, 'Shinepukurgovt. Primary School', '1. Khasertek: M- 139: F- 1372. Baniyabari: M- 273: F-2673. Bethuya: M- 973: F- 958', 6, 1385, 1362, 3, '10', '["65","67","64"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:35:30'),
(62, 'Modhurkhola govt. Primary School', '1. Modhurkhola: M-559: F -5292. South Modhurkhola: M- 482: F- 493', 4, 1041, 1022, 3, '10', '["70","69"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:36:09'),
(63, 'Moitpara govt. Primary School', '1. Horiyar tek: M- 154: F- 1672. Moitpara: M- 939: F 9733. Ruita: M- 200: F-220', 5, 1293, 1360, 3, '10', '["458","71","459"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:36:42'),
(64, 'Moura govt. Primary School', '1. Chatravog: M-42: F-632. Moura south: M -1107: F - 1158', 4, 1149, 1221, 3, '10', '["74","460"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:37:13'),
(65, 'Dhitpur Shahid Shakil govt. Primary School', '1. Moura North: M- 938: F- 9142. Dhitpur: M- 1009: F-999', 7, 1947, 1913, 3, '10', '["76","77"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:37:47'),
(66, 'Charhosenpura govt. Primary School', '1. Purulia: M-142: F-1262. Moinot: M- 04: F-053. Shukhdebpur: M- 41: F - 324. Deovog: M - 02: F -015. Hosenpur: M- 26: F - 276. Char Kushumhati: M- 553: F - 5197. Charboita: M- 383: F - 417', 4, 1151, 1127, 3, '11', '["466","78","464","82","85","87","463"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:46:19'),
(67, 'NarayonpurBaytul Aman- Darul Ulum Madrasha', '1. Shrikrishnapur: M- 425: F-3812. Narayanpur : M-348: F-291', 3, 773, 672, 3, '11', '["86","90"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:46:52'),
(68, 'Chardeovoga govt. Primary School', '1. Charkushaichar (part of 4 no ward): M- 239: F- 2202. Charkushaichar (part of 5 no ward): M- 862: F- 886', 4, 1101, 1106, 3, '11', '["468","470"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:47:44'),
(69, 'Mahmudpur  Community Center', '1. Charkushaichar (part of 7 no ward): M-287: F - 729)2. Mahmudpur(north side of Katakhal): M-827: F- 729', 4, 1114, 1022, 3, '11', '["472","88"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:48:58'),
(70, 'Harichandia govt. Primary School', '1. Harichandi: M- 565: F- 5682. Char Bilashpur: M 57: F- 403. Mahmudpur south side of Katakhal: M-635: F - 617', 4, 1275, 1225, 3, '11', '["79","81","473"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:49:48'),
(71, 'Bilashpur Choket Ali Munshi Forkaniya Madrasha', '1. Bilashpur: M- 435: F- 4632. Choto Ramnathpur: M- 274: F- 2343. Hajharbigha: M- 339: F-2964. Alinagar: M- 95: F- 645. Char Roghudebpur: M- 14: F- 18', 4, 1157, 1075, 3, '12', '["95","91","480","92","93"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:57:14'),
(72, 'Majhirchar Baytul-Ulum Madrasha (Majhirchar): (male and female voter)', '1. Boro ramnathpur: M- 366: F - 3512. Majhirchar: M- 308: F- 2803. Debinagar North: M- 316: F- 381', 4, 990, 1012, 3, '12', '["98","104","97"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:58:03'),
(73, 'Majhirchargovt. Primary School', '1. Purbachar: M-298: F- 2722. Radhanagar North: M- 256: F- 351', 3, 654, 623, 3, '12', '["99","100"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:58:54'),
(74, 'Bilashpur govt. Primary School', '1. Alamkhachar: M- 95: F- 1232. Kutubpur: M- 479: F- 4673. Debinagar South: M- 872: F- 794', 5, 1446, 1384, 3, '12', '["94","104","103"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:59:51'),
(75, 'Krishnadebpur Saber Chokdar govt. Primary School', '1. Radhanagarsouth: M- 505: F- 5132. Kulchuri: M -496: F- 4313. Krishnadebpur: M- 148: F- 138', 4, 1149, 1082, 3, '12', '["106","105","101"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:00:33'),
(76, 'Monikanda govt. Primary School: Shikaripara', 'Monikanda:M-581: F- 603Anondanagar: M- 253: F- 226Bishompur: M- 367: F- 352', 5, 1201, 1181, 3, '13', '["107","110","108"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:12:25'),
(77, 'Shibpurgovt. Primary School', 'Shibpur: M- 187: F- 240Sonatola: M- 257: F- 258Moheshpur: M- 172: F- 181Vodrokanda: M- 75: F -82Shekher Nagar- M- 60: F- 65Nurpur- M- 139: F- 141Notti: M- 132: F- 149', 5, 1022, 1116, 3, '13', '["109","114","115","112","111","116"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:13:32'),
(78, 'Shikaripara High School', 'Shiakaripara: M- 838: F- 872Kondrabpur: M- 149: F- 161Charbaghuri: M- 61: F- 59Lashkarkanda: M- 190: F-191', 5, 1238, 1283, 3, '13', '["120","118","119","117"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:14:40'),
(79, 'Hagradi govt. Primary School', 'Narsinghapur: M- 85: F- 85hagradi: M- 578: F- 612Noyadangi: M- 84: F- 94Garibpur:M-339: F- 420Boktarnagar: M- 584: F- 626', 7, 1670, 1837, 3, '13', '["125","124","121","122","123"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:18:35'),
(80, 'Boktarnagargovt. Primary School (Panjiprohori)', 'Gongadiya:M- 230: F- 216Panjiprohori: M- 460: F- 456Daudpur: M- 409: F- 371', 4, 1099, 1043, 3, '13', '["129","131","130"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:19:20'),
(81, 'Sujapur govt. Primary School', 'Narayanpur: M- 287: M- 301Sherpur: M- 251: F- 286Sujapur: M- 392: 458', 4, 930, 1045, 3, '13', '["128","126","127"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:20:00'),
(82, 'Joykrishnanagar govt. Primary School', 'Shankardiya: M- 293: F- 296Shyampur: M- 66: F- 60Bilchuri: M- 19: F- 17Joykrishnapur: M- 174: F- 197Gojariay: M- 74: F- 68', 3, 626, 628, 3, '14', '["138","137","135","133","136"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:33:18'),
(83, 'Shonabaju govt. Primary School', 'Shonabajhu: M- 1144: F- 1151', 4, 1144, 1151, 3, '14', '["132"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:33:48'),
(84, 'Titopoldiya govt. Primary School', 'Titopoldiya: M- 277: F- 244Kollyanshri: M- 286: F- 308Bahadurpur: M- 202: F- 229Panikawr: M- 214: F- 197', 4, 979, 978, 3, '14', '["134","139","141","140"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:34:38'),
(85, 'Kuthuri govt. Primary School', 'Kuthuri: M -587: F- 601Ashoypur: M- 280: F- 271Ghatbazar: M- 29: F- 30Raypur: M- 293: F- 323Brahmangram: M- 61: F- 61', 5, 1250, 1286, 3, '14', '["145","144","497","142","146"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:35:39'),
(86, 'Ghoshail govt. Primary School', 'Ghoshail: M- 458: F -481Ar Ghoshail: M- 262: F- 251Kedharpur: M- 176: F- 182bahuyaghati: M- 152: F- 137', 4, 1048, 1051, 3, '14', '["149","143","148","147"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:36:29'),
(87, 'Rajhapur govt. Primary School', 'Rajhapur: M- 600: M 603Balenga: M- 535: F- 497Kastartek: M- 144: F- 144Dhoyair: M- 38: F- 44Rajhapur Noyadangi: M- 36: F- 38Batoijuri: M- 18: F- 29', 5, 1371, 1355, 3, '14', '["154","159","155","498","152"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:37:37'),
(88, 'Charakhali govt. Primary School', 'Purbachak-M- 42: F- 41Charakhali-M- 118: F -125Mothbari: M- 75: F- 59Bataimuri: M- 11: F- 9', 2, 246, 234, 3, '14', '["153","158","157","156"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:38:26'),
(89, 'Baruakhali govt. Primary School', 'Baruakhali North: M- 201: F- 194Madla: M- 216: F- 203Chakbarilla: M- 121: F- 129baruakhali South- M- 529: F- 546', 4, 1067, 1072, 3, '15', '["162","166","167","165"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:55:26'),
(90, 'Baherchar govt. Primary School', 'Baherchar: M- 342: F- 400Daktarkanda: M- 42: F- 36Bramhankhali: M- 410: F- 445Choto Nabagram: M- 382: F- 384', 4, 1176, 1265, 3, '15', '["161","504","506","160"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:56:34'),
(91, 'Kumar barilla govt. Primary School', 'Dharikandi: M- 101: F- 108Veramuriya: M- 252: F- 253Kumarbarilla : M- 377: F- 387kandabarilla:M - 497: F- 497Borobarilla: M- 118: F-109', 5, 1345, 1354, 3, '15', '["176","172","174","175","173"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:01:26'),
(92, 'Alalpur govt. Primary School', 'Ramnagar: M- 73: F- 75Boro Priti Noyadda: M- 68: F- 76Choto Priti Noyadda- M- 93: F- 45Alalpur South: M- 327: F- 357Munshinagar: M- 344: F- 409Alalpur North: M- 273: F- 275', 5, 1213, 1341, 3, '15', '["180","181","511","513","179","183"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:03:06'),
(93, 'Kar Para govt. Primary School', 'Kar Para: M- 227: F- 242Chatrapur: M- 165: F- 170Joinotpur: M- 108: F- 106Dhirrghogram: M- 325: F- 356Shiyaljaan: M- 29: F- 41Vangagora: M- 134: F- 124', 4, 988, 1039, 3, '15', '["177","168","169","171","178","170"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:04:04'),
(94, 'Kawniyakandi govt. Primary School', 'Boro kawniyakandi: M- 361: F- 362Choto Kawniyakandi: M- 149: F- 161Jahanabaad: M- 105: F- 106Baniyagram: M- 68: F- 93Kanchannagar: M- 78: F- 77', 3, 761, 799, 3, '15', '["522","187","188","185","186"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:04:58'),
(95, 'Ghoshpara govt. Primary School', 'Ghoshpara:M- 390: F- 402Vaowadubi: M- 395: F- 440Vurakhali: M- 158: F- 127', 4, 943, 969, 3, '16', '["191","192","193"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:52:22'),
(96, 'Nabinagar Uttar Bahra Dhakhil Madrasha', 'Sahebganj: M- 231: F- 231North Bahra: M- 547: F- 616', 3, 778, 847, 3, '16', '["190","189"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:53:09'),
(97, 'Chartashulla govt. Primary School', 'Choto Tashulla: M- 774: F- 819Bipulla: M- 680: F- 766', 6, 1454, 1585, 3, '16', '["196","194"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:53:49'),
(98, 'Tashulla High School', 'Boro Tashulla: M- 438: F- 459Char Tuitail: M- 204: F- 218Bipratashulla: M- 373: F- 396', 4, 1015, 1073, 3, '16', '["195","537","197"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:54:23'),
(99, 'Tuitail govt. Primary School', 'Tuitail: M-F1671Bokchar- F-M- 721Charbaghuri- M-F- 96Notun Tuitail: M-F- 97Afzalnagar: M-F- 290', 6, 1341, 1534, 3, '16', '["202","200","201","199","538"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:55:10'),
(100, 'Khanepur High School', 'Shoilla: M- 339: F- 369Kandakhanepur: M- 569: F- 590Khanepur Kanda: M- 169: F- 173Char Shoilla: M- 226: F- 251', 5, 1313, 1383, 3, '16', '["203","205","204","539"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:56:00'),
(101, 'Radhakantpur govt. Primary School', 'RadhakantopurM- 538: F- 601Rahutghati: M- 231: F- 327Char Khanepur: M- 197: F- 219Noyonshree: M- 583: F- 630', 6, 1549, 1777, 3, '16', '["206","209","207","208"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:56:57'),
(102, 'Golla Bolik govt. Primary School', 'Sap Leja: M-126: F- 118Choto Golla: M- 189: F- 258Boro Golla: M- 186: F- 253Kashinagar: M- 39: F- 43Padrikanda: M- 58: F- 79Mataber Tek: M- 14: F- 14Kumar Golla: M- 262: F- 311Krishnanagar: M-167: F- 184', 4, 1041, 1260, 3, '16', '["218","217","211","220","219","215","213","214"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:58:10'),
(103, 'Deotala govt. Primary School', 'Deotola: M- 503: F- 570Dhori Deotola : M- 97: F- 123Shahjadpur: M- 85: F-92Purba Shoytankathi: M- 381: F- 430', 4, 1066, 1215, 3, '16', '["212","542","216","210"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:59:15'),
(104, 'Uttar Balukhanda Govt. Primary School', 'North Balukhanda - M:1070F:1168', 4, 1070, 1168, 3, '17', '["222"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:24:17'),
(105, 'Dutta Khanda Govt. Primary School', 'Dutta Khanda - M:876 F:894', 3, 876, 894, 3, '17', '["237"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:24:44'),
(106, 'Mohijdia Govt. Primary School', 'Mohijdia - M: 390 F:405Cumulli - M: 320 F: 311', 3, 710, 716, 3, '17', '["223","556"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:25:38'),
(107, 'Dokkhin Balukhanda Govt. Primary School', 'Dudhghata - M: 396 F: 386Dokkhin Balukhanda - M: 1030 F: 990', 5, 1426, 1376, 3, '17', '["221","236"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:26:13'),
(108, 'Purbo Patiljhap Govt. Primary School', 'Patiljhap - M:1049 F:1111Sholla Nagar - M: 327 F: 342', 5, 1376, 1453, 3, '17', '["224","244"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:26:48'),
(109, 'Sholla Higher Secondary  School', 'Khatia - M: 1241: F: 1243', 5, 1241, 1243, 3, '17', '["229"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:27:10'),
(110, 'Dokkhin Sholla Govt. Primary School', 'Dokkhin Sholla: M: 1197 F: 1166Ajgora - M: 167 F: 162Jhonjhonia - M:112 F:112', 5, 1476, 1440, 3, '17', '["231","570","230"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:27:46'),
(111, 'Shinghora Govt. Primary School:Sholla', 'Shinghora - M: 656 F:649Parashora - M:145 F: 140Noyahati - M:281 F:260Abdani - M:180 F:199Dokkhin Purbo Shinghora M: 195 F:202Shinghora Aikbari - M: 265F:336', 6, 1722, 1786, 3, '17', '["227","571","242","226","574","241"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:30:31'),
(112, 'Chokoria Govt. Primary School', 'Chokoria - M: 555 F: 560Chokoria Chokbari - M:485 F: 449', 4, 1040, 1009, 3, '17', '["228","572"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:32:01'),
(113, 'Shingjor Govt. Primary School', 'Shingjor  - M:1358 F: 1392', 5, 1358, 1392, 3, '17', '["246"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:32:29'),
(114, 'Sultanpur Govt. Primary School', 'Sultanpur - M: 194 F: 216Modonmohonpur - M: 667 F: 708Konda - M: 348 F: 340', 5, 1209, 1264, 3, '17', '["233","232","248"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:33:05'),
(115, 'M Mohiuddin Secondary School', 'Auna - M: 356 F:366Hayatkanda - M: 372 F: 389Ulail - M: 421 F: 438 Kartikpur - M: 63 F: 50Chok Auna - M: 272 F: 246Bowali - M:261: F: 232', 5, 1745, 1721, 3, '17', '["234","245","250","573","247","249"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:33:56'),
(116, 'Jontraile Govt. Primary School', 'Jontraile (Part of Ward No. 1)-M: 987 F: 1056', 4, 987, 1056, 3, '18', '["251"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:41:57'),
(117, 'Ajijpur Govt. Primary School', 'Jontraile (Part of Ward No. 2)-M: 530 F: 570Ajijpur- M: 378 F: 372', 4, 908, 942, 3, '18', '["252","575"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:42:36'),
(118, 'Chndrokhola Govt. Primary School', 'Chondrokhola -M: 868 F: 820Vaoyaliya- M: 347 F: 377', 4, 1215, 1197, 3, '18', '["253","254"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:43:14'),
(119, 'Begum Hasiba High School', 'Debukhali- M: 128 F: 108Horishkul (Part of Ward No. 5)-M: 976 F: 998', 4, 1104, 1106, 3, '18', '["257","258"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:43:41'),
(120, 'Dokhhin Horishkul Govt. Primary School', 'Horishkul (Part of Ward No. 6)-M: 1168 F: 1196Jalalchor- M: 592 F: 620', 6, 1760, 1816, 3, '18', '["256","255"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:44:10'),
(121, 'Gobindopur Govt. Primary School', 'East Gobindopur-M: 1352 F: 1410Moymondi- M: 459 F: 466', 7, 1811, 1876, 3, '18', '["259","260"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:46:04'),
(122, 'Nobogram Govt. Primary School', 'Balidior -M: 143 F: 199Chorkhlosi- M: 617 F: 609Nobogram- M: 251 F: 246', 4, 1011, 1054, 3, '18', '["576","577","578"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:46:50'),
(123, 'Kirinchi Govt. Primary School', 'Nolgora- M: 271 F: 272Kirinchi- M: 746 F: 699', 4, 1017, 971, 3, '18', '["580","579"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:47:20'),
(124, 'Mohobbotpur Govt. Primary School', 'Mohobbotpur- M: 1213 F: 1327Imamnogor- M: 189 F: 200', 6, 1402, 1527, 3, '19', '["266","261"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:49:11'),
(125, 'Sadapur Govt. Primary School', 'Sadapur- M: 936 F: 979Hazratpur- M: 343 F: 326Alhadipur- M: 420 F: 481', 6, 1699, 1786, 3, '19', '["583","270","584","262","267","586"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:51:32'),
(126, 'Majhirkanda Govt. Primary School: Bandura: Nowabgonj: Dhaka ( Male & Female Voter)', 'Sayedpur- M: 500 F: 556\nBoro Harikanda- M: 257 F: 283\nMajhirkanda- M: 419 F: 403\nMridhakanda- M: 131 F: 159\nKathalighata- M: 443 F: 497\nShorupdi Kandi- M: 44 F: 78', 6, 1794, 1976, 3, '19', '', 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(127, 'Puran Bandura Govt. Primary School', 'Old Bandura- M: 1550 F: 1715', 6, 1550, 1715, 3, '19', '["587"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:52:30'),
(128, 'Bandura Govt. Boys\' Primary School', 'Molashikanda- M: 316 F: 335Hasanabad (Part of Ward No. 5)- M: 609 F: 747', 4, 925, 1082, 3, '19', '["272","273"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:55:27'),
(129, 'Molouvi Dangi Govt. Primary School', 'Molouvi Dangi- M: 443 F: 463Hasanabad (Part of Ward No. 6)- M: 882 F: 963', 5, 1325, 1426, 3, '19', '["589","596"]', 0, '2018-10-05 18:00:00', '2018-10-06 18:00:21'),
(130, 'Puyakoiyer Govt. Primary School: (Tinshed Building) Centre- 1 Woman Centre', 'New Bandura (Part of ward No. 8)- F: 582Mirer Dangi- F: 106Nurongor- F: 373Noyanogor- F: 801Baroduari- F: 201Champanogor- F: 83', 4, 0, 2146, 3, '19', '["281","282","591","590","595","592"]', 0, '2018-10-05 18:00:00', '2018-10-06 18:02:24'),
(131, 'Puyakoiyer Govt. Primary School: (Tinshed Building & Half- stoned Building) Centre- 2 Male Centre', 'New Bandura (Part of ward No. 8)- M: 508Mirer Dangi- M: 63Nurongor- M: 384Noyanogor- M: 721Baroduari- M: 220Champanogor- M: 87', 4, 1983, 0, 3, '19', '["281","282","591","590","592"]', 0, '2018-10-05 18:00:00', '2018-10-06 18:05:01'),
(132, 'Notun Bandura Govt. Primary School', 'New Bandura (Part of ward No. 7)- M: 1327 F: 1268', 5, 1327, 1268, 3, '19', '["597"]', 0, '2018-10-05 18:00:00', '2018-10-06 18:06:46'),
(133, 'Harekrishna Kusum Koli High School', 'Modhonogor- M: 115 F: 128Goyalnogor- M: 148 F: 105Gopalpur- M: 218 F: 238Bagahati- M: 75 F: 91Borongor- M: 167 F: 161Voirahati- M: 126 F: 118Rajarampur- M: 148 F: 177Gopikantopur- M: 76 F: 85Khandokar Noyadha- M: 69 F: 78Pukurpar- M: 125 F: 128R', 5, 1415, 1437, 3, '20', '["288","555","283","565","554","549","553","548"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:18:23'),
(134, 'Nowabgonbj Pilot High School', 'Panaliya- M: 530 F: 534West Somsabad- M: 625 F: 662Boro Somsabad- M: 651 F: 716Nowabgonj- M: 238 F: 212', 8, 2044, 2124, 3, '20', '["559","298","296","558"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:22:14'),
(135, 'Nowabgonbj Pilot Girls\' High School', 'Kashimpur- M: 1445 F:1590', 6, 1445, 1590, 3, '20', '["560"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:21:33'),
(136, 'Jalalpur Udayan Model Govt. Primary School', 'Baghmara- M: 295 F: 299Surgonj- M: 290 F: 297Jalalpur- M: 463 F: 446Boikusthopur- M: 108 F: 127Amirpur- M: 499 F: 540', 6, 1655, 1709, 3, '20', '["303","302","306","304","562"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:29:36'),
(137, 'Rajpara Govt. Primary School', 'Khondokerhati- M: 456 F: 463Madhobpur- M: 446 F: 483Rajpara- M: 550 F: 597Pirmamudiya- M: 346 F: 407Bibirchor- M: 130 F: 160', 7, 1928, 2110, 3, '20', '["309","564","565","308","307"]', 0, '2018-10-05 18:00:00', '2018-10-06 17:30:32'),
(138, 'Pathankanda Govt. Primary School', 'Pathankanda (Part of ward no. 1)- M: 288 F: 308Komorgonj- M: 700 F: 651Gajikhali- M: 135 F: 124', 4, 1123, 1083, 3, '21', '["526","525","524"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:33:41'),
(139, 'Bordhonpara Govt. Primary School', 'Pathankanda (Part of ward no. 2)- M: 107 F: 99Bordhonpara (Part of ward no. 2)- M: 487 F: 527Bordhonpara (Part of ward no. 3)- M: 714 F: 772', 5, 1308, 1398, 3, '21', '["535","536","527"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:36:59'),
(140, 'Bakshanagar High School', 'Choto Boxnogor- M: 936 F: 996', 4, 936, 996, 3, '21', '["528"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:39:41'),
(141, 'Boro Bakshanagar Govt. Primary School', 'Choto Rajpara-  M: 486 F: 467Boro Boxnogor Sabekhati-  M:295 F: 287Boro Boxnogor-  M: 513 F: 560Boxnogor Chowrahoti-  M: 281 F: 349Boxnogor Christianhati-  M: 221 F: 264', 7, 1796, 1927, 3, '21', '["531","532","530","529","312"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:41:04'),
(142, 'Dhighirpar Govt. Primary School', 'Dhighir par (Part of ward no. 7)- M: 949 F: 1023Dhighir para (Part of ward no. 8)- M: 958 F: 997', 7, 1907, 2020, 3, '21', '["533","316"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:43:16'),
(143, 'Balurchor Govt. Primary School:', 'Balurchor- M: 710 F: 772Surgonj Tukinikanda- M: 531 F: 524', 5, 1241, 1296, 3, '21', '["317","534"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:47:20'),
(144, 'Bolomonttochor Govt. Primary School', 'Boro Bolomonttochor- M: 1224 F: 1314Choto Bolomonttochor- M: 361 F: 400', 6, 1585, 1714, 3, '22', '["499","500"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:55:23'),
(145, 'Alamgirchor Govt. Primary School', 'Alamgirchor- M: 751 F: 769Urarchor- M: 73 F: 74', 3, 824, 843, 3, '22', '["501","502"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:03:54'),
(146, 'Kandamatra Govt. Primary School', 'Kandamatra- M: 907 F: 856Kahur- M: 223 F: 219Bagbari- M: 57 F: 55', 4, 1187, 1130, 3, '22', '["323","320","322"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:05:13'),
(147, 'Baharra Poschim Govt. Primary School', 'Shuvoriya- M: 418 F: 407Boro Baharra Chakuri Para- M: 1105 F: 1021', 5, 1523, 1428, 3, '22', '["507","326"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:08:01'),
(148, 'Baharra Oyasek Memorial High School: Baharra: Nowabgonj: Dhaka ( Male & Female Voter)', 'Baharra East Tati Para (Baharra East Rishi Para)- M: 553 F: 565\nBoro Baharra West Para- M: 613 F: 611\nWest Chok Baharra- M: 177 F: 226', 5, 1343, 1402, 3, '22', '', 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(149, 'Baharra Purbo Govt. Primary School', 'Baharra Mollakanda- M: 501 F: 560Mylaile- M: 430 F: 466Baharra Chorkanda- M: 881 F: 891', 7, 1812, 1917, 3, '22', '["515","512","514"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:09:58'),
(150, 'Agla Chowkighata Jonomongol High School', 'North Chowkighata- M: 790 F: 834South Chowkighata- M: 616 F:635', 5, 1406, 1469, 3, '22', '["516","517"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:11:51'),
(151, 'Baharra Ebetedayi Madrasha', 'Boro Baharra East & North- M: 322 F: 296East Chok Baharra- M: 163 F: 170Baharra Sonargaon- M: 188 F: 204Uluman Chandura- M: 120 F: 120Naopara- M: 241 F: 247', 4, 1034, 1037, 3, '22', '["520","518","519","523","521"]', 0, '2018-10-05 18:00:00', '2018-10-06 16:14:11'),
(152, 'Koilail Govt. Primary School', 'Koilail South- M: 252: F: 264Koilail Bagmara - M: 458 F: 460Telenga - M: 419: F: 402', 4, 1129, 1126, 3, '23', '["496","481","330"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:24:49'),
(153, 'Koilail Hanafia Madrasha', 'Koilail North - M:686 F:638Sonargaon - M: 209 F: 215Meleng Dokkhin - M: 198 F: 210Raipur - M: 344 F: 339', 5, 1437, 1402, 3, '23', '["483","485","337","484"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:26:33'),
(154, 'Meleng Govt. Primary School', 'Meleng North - M: 416 F:420Meleng East - M: 438: F: 420Meleng West (Khal Paar) - M: 218 F: 232', 4, 1072, 1072, 3, '23', '["487","486","488"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:27:27'),
(155, 'Matabpur Govt. Primary School:  Koilail: Nawabganj: Dhaka (Male & Female Voter)', 'Matabpur - M: 567 F: 634', 2, 567, 634, 3, '23', '', 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(156, 'Paragram Govt. Primary School', 'Dignara - M: 295 F: 241Paragram - M: 524 F: 531Mashail - M: 262 F: 226', 4, 1081, 998, 3, '23', '["489","490","331"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:28:56'),
(157, 'Dorikanda Govt. Primary School', 'Dorikanda - M: 1100 F: 1100Madhupur - M: 346 F: 323', 5, 1446, 1423, 3, '23', '["332","333"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:29:54'),
(158, 'Bhangavita Govt. Primary School', 'Molla Kanda - M: 222 F: 243Bhangavita - M: 971 F: 840', 4, 1193, 1083, 3, '23', '["493","492"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:30:43'),
(159, 'Malikanda Govt. Primary School', 'Malikanda - M: 651 F:684Noyakanda - M: 803 F: 778', 5, 1454, 1462, 3, '23', '["335","334"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:34:48'),
(160, 'Doulotpur Govt. Primary School', 'Doulotpur - M: 1184 F: 1167', 4, 1184, 1167, 3, '23', '["346"]', 0, '2018-10-05 18:00:00', '2018-10-06 15:35:40'),
(161, 'Majhpara Govt. Primary School', 'Mukimpur- M: 132 F: 124Hajikanda- M: 155 F: 160Agla- M: 337 F: 375Agla East para- M: 311 F: 319Majhpara- M: 880 F: 948', 7, 1815, 1926, 3, '24', '["354","355","462","465","461"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:54:05'),
(162, 'Mohakobi Kayokobad Girls\' High School (Front Building): Centre-1', 'Kaluyahati North- F: 1195Kaluyahati South- F: 352Tikorpur- F: 778', 4, 0, 2325, 3, '24', '["467","469","471"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:56:50'),
(163, 'Mohakobi Kayokobad Girls\' High School (Back Building): Centre-2', 'Kaluyahati North- M: 1186Kaluyahati South- M: 393Tikorpur- M: 840', 4, 2419, 0, 3, '24', '["467","469","471"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:56:34'),
(164, 'Chatiya Govt. Primary School', 'Mohonpur- M: 913 F: 1016Chatiya- M: 657 F: 697', 6, 1570, 1713, 3, '24', '["475","474"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:57:47'),
(165, 'Chandertek Govt. Primary School', 'Chorchoriya- M:605 F: 568Chor Modhuchoriya- M: 351 F: 341', 4, 956, 909, 3, '24', '["477","476"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:58:27'),
(166, 'BG Chorchoriya Govt. Primary School (Benukhali)', 'Gokulnogor- M: 291 F: 318Benukhali- M: 553 F: 578', 4, 844, 896, 3, '24', '["479","478"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:59:05'),
(167, 'Miyahati Mahabubiya Kaderiya Madrasha', 'Andharkotha- M: 499 F: 526Kuthibari- M: 97 F: 116Noyadda- M: 700 F: 687Miyahati- M: 182 F: 195Joynogor surgonj- M: 573 F: 630', 8, 2051, 2154, 3, '25', '["449","452","357","359","450"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:27:32'),
(168, 'Galimpur Rahmania High School', 'Borogram- M: 584 F: 604Surjokhali- M: 440 F: 434Sahabad- M: 241 F: 236Khanhati- M: 483 F: 470', 6, 1748, 1744, 3, '25', '["363","366","365","364"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:30:07'),
(169, 'Sonahazra Govt. Primary School', 'Chanhati- M: 173 F: 189Nogor- M: 147 F: 154Ramnathpur- M: 221 F: 245Sonahazara- M: 667 F: 715Shonkorkhali- M: 323 F: 339Paikesha- M: 201 F: 225', 6, 1732, 1867, 3, '25', '["369","367","372","368","370","371"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:33:57'),
(170, 'Kamarkhola Govt. Primary School', 'Kamarkhola- M: 848 F: 801', 4, 848, 801, 3, '26', '["445"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:25:05'),
(171, 'Munshinogor Govt. Primary School', 'West Munshinogor- M: 530 F: 580East Munshinogor- M: 572 F: 686', 4, 1102, 1166, 3, '26', '["444","443"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:24:30'),
(172, 'Morichpotti Govt. Primary School', 'Morichpotti- M: 676 F: 715Modonkhali- M: 150 F: 140', 4, 826, 855, 3, '26', '["387","388"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:21:58'),
(173, 'Durgapur Govt. Primary School', 'East Gobindopur- M: 739 F: 747Durgapur- M: 858 F: 843', 6, 1597, 1590, 3, '26', '["454","453"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:20:33'),
(174, 'Churain Tarini Bama High School (New Building), Centre- 1, Male Centre', 'West Churain- M: 1112East Churain- M: 940', 3, 2052, 0, 3, '26', '0', 0, '2018-10-05 18:00:00', '2018-10-06 14:14:57'),
(175, 'Churain Tarini Bama High School (Old Building), Centre- 2, Female Center', 'West Churain- M: 1098East Churain- M: 962', 4, 0, 2060, 3, '26', '["383","441"]', 0, '2018-10-05 18:00:00', '2018-10-06 14:14:50'),
(176, 'Gobindopur Agroni Govt. Primary School: Churain: Nawabganj: Dhaka', 'Sonahazra- M: 88 F: 96Chourahati- M: 207 F: 256Chanhati- M: 45 F: 39West Gobindopur- M: 673 F: 707', 4, 1013, 1098, 3, '26', '["380","374","379","373"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:48:34'),
(177, 'Sonatola Madrasha', 'Sonatola- M: 391 F: 362Muslimhati- M: 1065 F: 1068', 5, 1456, 1430, 3, '26', '["375","376"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:53:13'),
(178, 'Gobindopur Govt. Primary School: Churain: Nawabganj: Dhaka', 'Shonkorkhali- M: 347 F: 395Paiksha- M: 171 F: 156', 2, 518, 551, 3, '26', '["377","378"]', 0, '2018-10-05 18:00:00', '2018-10-06 13:41:36');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_organizations`
--

CREATE TABLE `bol_election_organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `bol_election_organizations`
--

INSERT INTO `bol_election_organizations` (`id`, `name`, `created_at`, `updated_at`) VALUES
(3, 'Awami league', '2018-10-02 05:39:04', '2018-10-02 05:39:04'),
(4, 'Government', '2018-10-04 03:58:29', '2018-10-04 03:58:29'),
(5, 'Police', '2018-10-04 06:25:23', '2018-10-06 04:09:50'),
(6, 'RAB', '2018-10-06 04:10:00', '2018-10-06 04:10:00'),
(7, 'UNO', '2018-10-06 05:49:35', '2018-10-06 05:49:35');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_people`
--

CREATE TABLE `bol_election_people` (
  `id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `union_id` int(11) DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `village_id` int(11) DEFAULT NULL,
  `peoplegroup_ids` text COLLATE utf8_unicode_ci NOT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `designation_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `phones` text COLLATE utf8_unicode_ci,
  `address` text COLLATE utf8_unicode_ci,
  `has_priority` tinyint(1) DEFAULT NULL,
  `is_al` tinyint(1) DEFAULT NULL,
  `extra_designation` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `in_fluential` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_people`
--

INSERT INTO `bol_election_people` (`id`, `district_id`, `upazila_id`, `union_id`, `ward_id`, `village_id`, `peoplegroup_ids`, `organization_id`, `designation_id`, `name`, `phones`, `address`, `has_priority`, `is_al`, `extra_designation`, `in_fluential`, `created_at`, `updated_at`) VALUES
(4, 18, 5, 0, 0, 0, '', 4, 19, 'Salma Khatun', '[{"type":"office","phone":"01933444057"}]', '', 0, 0, '', NULL, '2018-10-04 04:56:38', '2018-10-04 13:18:47'),
(5, 18, 5, 0, 0, 0, '', 4, 20, 'Dr. Md. Jasim Uddin', '[{"type":"office","phone":"01711016542"}]', '', 1, 0, '', NULL, '2018-10-04 04:57:22', '2018-10-04 14:28:39'),
(6, 18, 5, 0, 0, 0, '', 4, 21, 'ABM Wahidur Rahman', '[{"type":"office","phone":"01715785390"}]', '', 0, 0, '', NULL, '2018-10-04 04:57:57', '2018-10-04 13:18:45'),
(7, 18, 5, 0, 0, 0, '', 4, 22, 'Md. Sajjad Hossain', '[{"type":"office","phone":"01713373331"}]', '', 1, 0, '', NULL, '2018-10-04 04:58:40', '2018-10-06 07:57:23'),
(8, 18, 5, 0, 0, 0, '', 4, 23, 'Md. Kabir Uddin Shah', '[{"type":"office","phone":"01711908530"}]', '', 0, 0, '', NULL, '2018-10-04 04:59:34', '2018-10-05 13:39:51'),
(9, 18, 5, 0, 0, 0, '', 4, 24, 'Md. Jainul Abedin', '[{"type":"office","phone":"0172727508"}]', '', 1, 0, '', NULL, '2018-10-04 05:00:11', '2018-10-04 14:28:59'),
(10, 18, 5, 0, 0, 0, '', 4, 25, 'Md. Shahidul Islam', '[{"type":"office","phone":"01786490132"}]', '', NULL, 0, '', NULL, '2018-10-04 05:00:49', '2018-10-04 05:00:49'),
(11, 18, 5, 0, 0, 0, '', 4, 26, 'Mehedi Hasan', '[{"type":"office","phone":"01736725660"}]', '', NULL, 0, '', NULL, '2018-10-04 05:01:50', '2018-10-04 05:01:50'),
(12, 18, 5, 0, 0, 0, '', 4, 27, 'Alamgir Hossain', '[{"type":"office","phone":"01711522093"}]', '', 0, 0, '', NULL, '2018-10-04 05:02:44', '2018-10-04 09:46:57'),
(13, 18, 5, 0, 0, 0, '', 4, 28, 'Md. Masud Parvez', '[{"type":"office","phone":"01763634211"}]', '', NULL, 0, '', NULL, '2018-10-04 05:03:58', '2018-10-04 05:03:58'),
(14, 18, 5, 0, 0, 0, '', 4, 29, 'Shamima Rahim', '[{"type":"office","phone":"01713011825"}]', '', NULL, 0, '', NULL, '2018-10-04 05:04:33', '2018-10-04 05:04:33'),
(15, 18, 5, 5, 0, 0, '', 4, 30, 'Shamim Ahmad', '[]', '', 0, 0, '', NULL, '2018-10-04 05:05:40', '2018-10-04 09:46:09'),
(16, 18, 5, 5, 7, 7, '', 4, 31, 'Shamsuddin', '[{"type":"office","phone":"01711035017"}]', '', 0, 0, '', NULL, '2018-10-04 05:10:23', '2018-10-04 13:18:51'),
(17, 18, 5, 5, 0, 0, '', 3, 32, 'Sirajul Islam (Vulu)', '[{"type":"office","phone":"01711111111"}]', '', 0, 0, '', NULL, '2018-10-04 06:59:12', '2018-10-04 14:29:09'),
(18, 18, 5, 5, 0, 0, '', 3, 32, 'Bajlul Rahman Kamal', '[{"type":"office","phone":"01722222222"}]', '', 0, 0, '', NULL, '2018-10-04 07:00:31', '2018-10-04 10:45:38'),
(20, 18, 6, 0, 0, 0, '', 4, 34, 'Shahnaz Mithun Munni', '[{"type":"mobile","phone":"01933444056"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:09:41', '2018-10-06 06:32:12'),
(24, 18, 6, 0, 0, 0, '"4, 5"', 4, 37, 'Shohidul Amin', '[{"type":"office","phone":"027765018"},{"type":"mobile","phone":"01766568871"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:34:12', '2018-10-06 06:34:12'),
(22, 18, 6, 0, 0, 0, '', 4, 35, 'Mr. Ramanonda Sarker', '[{"type":"mobile","phone":"01713373360"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:27:27', '2018-10-06 06:31:43'),
(21, 18, 6, 0, 0, 0, '"4, 5"', 4, 33, 'Tofazzal Hossain', '[{"type":"office","phone":"027765001"},{"type":"home","phone":"027765005"},{"type":"mobile","phone":"01933444037"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:19:44', '2018-10-06 06:19:44'),
(23, 18, 6, 0, 0, 0, '"4, 5"', 4, 36, 'Shohidul Islam', '[{"type":"office","phone":"027765009"},{"type":"mobile","phone":"01911567415"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:31:14', '2018-10-06 06:31:14'),
(25, 18, 6, 0, 0, 0, '"4, 5"', 4, 38, 'Rakibul Ahsan', '[{"type":"mobile","phone":"01724418860"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:36:39', '2018-10-06 06:36:39'),
(26, 18, 6, 0, 0, 0, '"4, 5"', 4, 39, 'Azizul Islam', '[{"type":"office","phone":"027765249"},{"type":"mobile","phone":"01883948898"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:38:05', '2018-10-06 06:38:05'),
(27, 18, 6, 0, 0, 0, '"4, 5"', 4, 40, 'Zakir Hossain', '[{"type":"office","phone":"027765255"},{"type":"mobile","phone":"01716418937"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:39:39', '2018-10-06 06:39:39'),
(28, 18, 6, 0, 0, 0, '"4, 5"', 4, 41, 'Shaymol Chandra Poddar', '[{"type":"office","phone":"01714432399"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:41:25', '2018-10-06 06:41:25'),
(29, 18, 6, 0, 0, 0, '"4, 5"', 4, 42, 'Shajahan', '[{"type":"office","phone":"027765068"},{"type":"mobile","phone":"01711035870"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:43:17', '2018-10-06 06:43:17'),
(30, 18, 6, 0, 0, 0, '"4, 5"', 4, 43, 'Saif Md. Zulkar Nain', '[{"type":"mobile","phone":"01672947867"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:45:23', '2018-10-06 06:45:23'),
(31, 18, 6, 0, 0, 0, '"4, 5"', 4, 44, 'Harun-ur-Rashid Bhuiyan', '[{"type":"mobile","phone":"01680428862"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:46:39', '2018-10-06 06:46:39'),
(32, 18, 6, 0, 0, 0, '"4, 5"', 4, 45, 'Jesmin Ahmed', '[{"type":"office","phone":"027765123"},{"type":"mobile","phone":"01670160193"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:48:08', '2018-10-06 06:48:08'),
(33, 18, 6, 0, 0, 0, '"4, 5"', 4, 46, 'Abdus Salam', '[{"type":"office","phone":"027765128"},{"type":"mobile","phone":"01715908371"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:51:13', '2018-10-06 06:51:13'),
(34, 18, 5, 0, 0, 0, '"4, 5"', 4, 47, 'Afruza Akter Biva', '[{"type":"office","phone":"01795724915"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:51:37', '2018-10-06 06:51:37'),
(35, 18, 6, 0, 0, 0, '"4, 5"', 4, 48, 'Shanti Ranjan Debnath', '[{"type":"office","phone":"027765233"},{"type":"mobile","phone":"01761713051"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:55:37', '2018-10-06 06:55:37'),
(36, 18, 5, 0, 0, 0, '"4, 5"', 4, 49, 'Salma Khatun', '[{"type":"office","phone":"01933444057"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:55:54', '2018-10-06 06:55:54'),
(37, 18, 5, 0, 0, 0, '"4, 7"', 5, 50, 'Sajjat Hossain', '[{"type":"office","phone":"01713373331"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:58:07', '2018-10-06 06:58:07'),
(38, 18, 5, 0, 0, 0, '"4, 5"', 4, 51, 'Mahbuba Akter', '[{"type":"office","phone":"01554600344"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:16:36', '2018-10-06 07:16:36'),
(39, 18, 5, 0, 0, 0, '"4, 5"', 4, 36, 'Jasim Uddin', '[{"type":"office","phone":"01711016542"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:17:57', '2018-10-06 07:17:57'),
(40, 18, 5, 0, 0, 0, '"4, 5"', 4, 37, 'ABM Wahidur Rahman', '[{"type":"office","phone":"01715785390"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:18:45', '2018-10-06 07:18:45'),
(41, 18, 5, 0, 0, 0, '"4, 5"', 4, 40, 'AKM Mizanur Rahman', '[{"type":"office","phone":"01716637536"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:19:41', '2018-10-06 07:19:41'),
(42, 18, 5, 0, 0, 0, '"4, 5"', 4, 23, 'Kabir Udin Shah', '[{"type":"office","phone":"01711908530"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:20:51', '2018-10-06 07:20:51'),
(43, 18, 5, 0, 0, 0, '"4, 5"', 4, 24, 'Mohd. Joynul Abedin', '[{"type":"office","phone":"01721727508"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:22:22', '2018-10-06 07:22:22'),
(44, 18, 5, 0, 0, 0, '"4, 5"', 4, 79, 'Shahidul Islam', '[{"type":"office","phone":"01786490132"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:23:18', '2018-10-06 07:23:18'),
(45, 18, 5, 0, 0, 0, '"4, 5"', 4, 85, 'Mehedi Hasan', '[{"type":"office","phone":"01736725660"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:24:32', '2018-10-06 07:24:32'),
(46, 18, 5, 0, 0, 0, '"4, 5"', 4, 63, 'Momtaj Uddin', '[{"type":"office","phone":"01727756344"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:25:14', '2018-10-06 07:25:14'),
(47, 18, 5, 0, 0, 0, '"4, 5"', 4, 59, 'Mohd. Alauddin Mia', '[{"type":"office","phone":"01716928747"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:26:12', '2018-10-06 07:26:12'),
(48, 18, 5, 0, 0, 0, '"4, 5"', 4, 51, 'Mahbuba Akter', '[{"type":"office","phone":"01554600344"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:26:39', '2018-10-06 07:26:39'),
(49, 18, 5, 0, 0, 0, '"4, 5"', 4, 52, 'Kakon Rani Bosak', '[{"type":"office","phone":"01711144626"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:27:04', '2018-10-06 07:27:04'),
(83, 18, 5, 0, 0, 0, '"4, 5"', 4, 65, 'Joynal Abedin', '[{"type":"office","phone":"01746316418"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:38:00', '2018-10-06 07:38:00'),
(84, 18, 6, 0, 0, 0, '', 4, 80, 'Faruk Ahmed', '[{"type":"office","phone":"027765124"},{"type":"mobile","phone":"01712222939"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 08:02:17'),
(85, 18, 6, 0, 0, 0, '', 4, 81, 'Ashikur Rahman', '[{"type":"mobile","phone":"01711102211"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 08:01:30'),
(117, 18, 5, 0, 0, 0, '"4, 9"', 4, 61, 'Hamidur Rahman', '[{"type":"office","phone":"01716902211"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:12:59', '2018-10-06 08:12:59'),
(82, 18, 5, 0, 0, 0, '"4, 5"', 4, 87, 'Shilpi Naag', '[{"type":"office","phone":"01716504029"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:34:51', '2018-10-06 07:34:51'),
(86, 18, 6, 0, 0, 0, '', 4, 51, 'Farzana Abedin', '[{"type":"office","phone":"027765015"},{"type":"mobile","phone":"01711102211"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 08:00:19'),
(87, 18, 6, 0, 0, 0, '', 4, 77, 'Mostafa Kamal', '[{"type":"office","phone":"027765125"},{"type":"mobile","phone":"01713373330"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:59:17'),
(88, 18, 6, 0, 0, 0, '', 4, 78, 'AKM Shamim Hasan', '[{"type":"mobile","phone":"01711102211"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:58:37'),
(89, 18, 6, 0, 0, 0, '', 4, 79, 'Habibullah Mia', '[{"type":"office","phone":"027765122"},{"type":"mobile","phone":"01712363723"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:58:06'),
(90, 18, 6, 0, 0, 0, '', 4, 52, 'Akbar Hossain Mia', '[{"type":"mobile","phone":"01732074033"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:57:28'),
(91, 18, 6, 0, 0, 0, '', 4, 53, 'Aksaur Rahman', '[{"type":"mobile","phone":"01732466049"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:57:01'),
(118, 18, 5, 0, 0, 0, '"4, 5"', 4, 91, 'Kazi Sahida Begum', '[{"type":"office","phone":"01711930758"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:17:40', '2018-10-06 08:17:40'),
(92, 18, 6, 0, 0, 0, '', 4, 54, 'Ibn Mayaz Pramanik', '[{"type":"mobile","phone":"01722109822"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:56:32'),
(93, 18, 6, 0, 0, 0, '', 4, 55, 'H M Shahin', '[{"type":"mobile","phone":"01712255819"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:55:57'),
(94, 18, 6, 0, 0, 0, '', 4, 56, 'Shahjalal', '[{"type":"office","phone":"027765066"},{"type":"mobile","phone":"01720078857"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:54:59'),
(81, 18, 5, 0, 0, 0, '"4, 5"', 4, 86, 'Bolram', '[{"type":"office","phone":"01727058979"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:32:05', '2018-10-06 07:32:05'),
(95, 18, 6, 0, 0, 0, '', 4, 57, 'Nazrul Islam Sheikh', '[{"type":"mobile","phone":"01552631176"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:54:05'),
(119, 18, 5, 0, 0, 0, '"4, 5"', 4, 81, 'Liakot Ali', '[{"type":"office","phone":"01711337660"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:18:57', '2018-10-06 08:18:57'),
(96, 18, 6, 0, 0, 0, '', 4, 58, 'Rahima Begum', '[{"type":"office","phone":"027765126"},{"type":"mobile","phone":"01718144197"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:53:26'),
(97, 18, 6, 0, 0, 0, '', 4, 59, 'Nasir Uddin', '[{"type":"mobile","phone":"0000"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:52:51'),
(98, 18, 6, 0, 0, 0, '', 4, 60, 'Taibur Rahman', '[{"type":"office","phone":"01716048269"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:51:47'),
(99, 18, 6, 0, 0, 0, '', 4, 61, 'Z M Shamsul Alam', '[{"type":"office","phone":"027765127"},{"type":"mobile","phone":"01712919504"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:51:10'),
(100, 18, 6, 0, 0, 0, '', 4, 62, 'Mohd. Ayub Mia', '[{"type":"mobile","phone":"01716209933"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:50:24'),
(101, 18, 6, 0, 0, 0, '', 4, 63, 'Momtaz Uddin', '[{"type":"mobile","phone":"01727756344"},{"type":"office","phone":"01556452594"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:49:42'),
(102, 18, 6, 0, 0, 0, '', 4, 64, 'Ohidul Islam', '[{"type":"mobile","phone":"01746151447"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:49:02'),
(120, 18, 5, 0, 0, 0, '"4, 5"', 4, 60, 'Mansur Rahman', '[{"type":"office","phone":"01730992142"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:20:05', '2018-10-06 08:20:05'),
(103, 18, 6, 0, 0, 0, '', 4, 88, 'Matiur Rahman', '[{"type":"mobile","phone":"01726532247"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:48:06'),
(104, 18, 6, 0, 0, 0, '', 4, 66, 'Fazlul Karim', '[{"type":"mobile","phone":"01711019389"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:46:37'),
(105, 18, 6, 0, 0, 0, '', 4, 67, 'Asim Kumar Das', '[{"type":"mobile","phone":"01769400025"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:46:10'),
(106, 18, 6, 0, 0, 0, '', 4, 68, 'Mominul Islam', '[{"type":"mobile","phone":"01769400410"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:45:42'),
(107, 18, 6, 0, 0, 0, '', 4, 69, 'Arab Ali Sheikh', '[{"type":"mobile","phone":"01769400411"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:44:37'),
(121, 18, 5, 0, 0, 0, '"4, 5"', 4, 39, 'ABM Zakaria', '[{"type":"office","phone":"01717046936"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:21:27', '2018-10-06 08:21:27'),
(108, 18, 6, 0, 0, 0, '', 4, 70, 'Abdul Kader Jilani', '[{"type":"mobile","phone":"01769400412"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:43:54'),
(109, 18, 6, 0, 0, 0, '', 4, 71, 'Mahbubur Rahman', '[{"type":"mobile","phone":"01769400413"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:43:21'),
(110, 18, 6, 0, 0, 0, '', 4, 72, 'Anwarul Haq', '[{"type":"office","phone":"01716932614"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:42:52'),
(111, 18, 6, 0, 0, 0, '', 4, 73, 'Saidur Rahman', '[{"type":"office","phone":"01552301000"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:41:56'),
(112, 18, 6, 0, 0, 0, '', 4, 74, 'Shamishtha Kundu', '[{"type":"mobile","phone":"01931998238"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:42:20'),
(122, 18, 5, 0, 0, 0, '"4, 5"', 4, 64, 'Hafiza Hakim Ruma', '[{"type":"office","phone":"01715169595"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:22:19', '2018-10-06 08:22:19'),
(123, 18, 5, 0, 0, 0, '"4, 5"', 4, 45, 'Hindol Bari', '[{"type":"office","phone":"01711906438"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:25:04', '2018-10-06 08:25:04'),
(114, 18, 6, 0, 0, 0, '', 4, 76, 'Taomal Das', '[{"type":"mobile","phone":"01712524220"},{"type":"mobile","phone":"01737097107"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:40:11'),
(115, 18, 5, 0, 0, 0, '"4, 5"', 4, 53, 'Badsha Mia', '[{"type":"office","phone":"01913478419"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:38:57', '2018-10-06 07:38:57'),
(116, 18, 5, 0, 0, 0, '"4, 5"', 4, 48, 'Kamrul Islam Khan', '[{"type":"office","phone":"01781217589"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:39:47', '2018-10-06 07:39:47'),
(124, 18, 5, 0, 0, 0, '"4, 5"', 4, 46, 'Sabbir Hossain', '[{"type":"office","phone":"01676766368"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:26:30', '2018-10-06 08:26:30'),
(125, 18, 5, 0, 0, 0, '"4, 5"', 4, 92, 'Salah Uddin', '[{"type":"office","phone":"01743732835"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:27:57', '2018-10-06 08:27:57'),
(126, 18, 5, 0, 0, 0, '"4, 5"', 4, 58, 'Sadia Afrin', '[{"type":"office","phone":"01677057166, 01914139981"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:29:44', '2018-10-06 08:29:44'),
(127, 18, 5, 0, 0, 0, '"4, 5"', 4, 57, 'Samsunnahar Khan', '[{"type":"office","phone":"01711241246"}]', 'Youth Development Officer', NULL, NULL, NULL, NULL, '2018-10-06 08:33:57', '2018-10-06 08:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_people_groups`
--

CREATE TABLE `bol_election_people_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `bol_election_people_groups`
--

INSERT INTO `bol_election_people_groups` (`id`, `name`, `sort_order`, `created_at`, `updated_at`, `parent_id`, `nest_left`, `nest_right`, `nest_depth`) VALUES
(4, 'Government', 0, '2018-10-04 13:41:50', '2018-10-04 13:48:40', 0, 1, 12, 0),
(5, 'Administration', 0, '2018-10-04 13:42:04', '2018-10-04 13:45:07', 4, 2, 3, 1),
(6, 'Law Enforcement', 0, '2018-10-04 13:42:11', '2018-10-04 13:48:36', 4, 4, 11, 1),
(7, 'Police', 0, '2018-10-04 13:43:49', '2018-10-04 13:45:41', 6, 5, 6, 2),
(8, 'RAB', 0, '2018-10-04 13:43:59', '2018-10-04 13:45:58', 6, 7, 8, 2),
(9, 'Ansar & VDP', 0, '2018-10-04 13:44:08', '2018-10-04 13:48:36', 6, 9, 10, 2),
(10, 'Local Government', 0, '2018-10-04 13:44:16', '2018-10-04 13:48:40', 0, 13, 14, 0),
(11, 'Political Party', 0, '2018-10-04 13:44:27', '2018-10-04 13:44:27', 0, 15, 16, 0),
(12, 'Important People', 0, '2018-10-04 13:44:35', '2018-10-04 13:44:35', 0, 17, 18, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_union`
--

CREATE TABLE `bol_election_union` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_union`
--

INSERT INTO `bol_election_union` (`id`, `name`, `district_id`, `upazila_id`, `created_at`, `updated_at`) VALUES
(6, 'Kushumhati', 18, 5, '2018-10-03 09:56:31', '2018-10-06 10:44:03'),
(5, 'Nayabari', 18, 5, '2018-10-03 09:55:51', '2018-10-03 09:55:51'),
(7, 'Raipara', 18, 5, '2018-10-03 09:57:06', '2018-10-03 09:57:06'),
(8, 'Sutarpara', 18, 5, '2018-10-03 09:57:28', '2018-10-03 09:57:28'),
(9, 'Narisha', 18, 5, '2018-10-03 09:57:51', '2018-10-03 09:57:51'),
(10, 'Muksudpur', 18, 5, '2018-10-03 09:58:12', '2018-10-03 09:58:12'),
(11, 'Mahmudpur', 18, 5, '2018-10-03 09:58:36', '2018-10-03 09:58:36'),
(12, 'Bilaspur', 18, 5, '2018-10-03 09:59:05', '2018-10-03 12:58:15'),
(13, 'Shikaripara', 18, 6, '2018-10-03 12:46:37', '2018-10-03 12:46:37'),
(14, 'Joykrishnapur', 18, 6, '2018-10-03 12:48:01', '2018-10-03 12:48:01'),
(15, 'Baruakhali', 18, 6, '2018-10-03 12:48:37', '2018-10-03 12:48:37'),
(16, 'Nayansree', 18, 6, '2018-10-03 12:50:49', '2018-10-03 12:50:49'),
(17, 'Sholla', 18, 6, '2018-10-03 12:51:33', '2018-10-03 12:51:33'),
(18, 'Jantrail', 18, 6, '2018-10-03 12:52:00', '2018-10-03 12:52:00'),
(19, 'Bandura', 18, 6, '2018-10-03 12:52:20', '2018-10-03 12:52:20'),
(20, 'Kalakopa', 18, 6, '2018-10-03 12:52:53', '2018-10-03 12:52:53'),
(21, 'Bakshanagar', 18, 6, '2018-10-03 12:53:17', '2018-10-03 12:53:17'),
(22, 'Barrah', 18, 6, '2018-10-03 12:53:41', '2018-10-03 12:53:41'),
(23, 'Kailail', 18, 6, '2018-10-03 12:54:08', '2018-10-03 12:54:08'),
(24, 'Agla', 18, 6, '2018-10-03 12:54:33', '2018-10-03 12:54:33'),
(25, 'Galimpur', 18, 6, '2018-10-03 12:54:54', '2018-10-03 12:54:54'),
(26, 'Churain', 18, 6, '2018-10-03 12:55:20', '2018-10-03 12:55:20'),
(27, 'Dohar Pourashava', 18, 5, '2018-10-06 11:39:24', '2018-10-06 11:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_upazila`
--

CREATE TABLE `bol_election_upazila` (
  `id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_upazila`
--

INSERT INTO `bol_election_upazila` (`id`, `district_id`, `name`, `created_at`, `updated_at`) VALUES
(6, 18, 'Nawabganj', '2018-10-03 12:45:38', '2018-10-03 12:50:58'),
(5, 18, 'Dohar', '2018-10-03 09:55:09', '2018-10-03 09:55:09');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_village`
--

CREATE TABLE `bol_election_village` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `union_id` int(11) DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `female_voter` int(11) NOT NULL,
  `male_voter` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_village`
--

INSERT INTO `bol_election_village` (`id`, `name`, `district_id`, `upazila_id`, `union_id`, `ward_id`, `order`, `female_voter`, `male_voter`, `created_at`, `updated_at`) VALUES
(1, 'North Awranggabad', 18, 5, 5, 1, 0, 0, 0, '2018-10-02 18:00:00', '2018-10-06 10:37:15'),
(2, 'Moddho Awranggabad', 18, 5, 5, 2, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:36:33'),
(3, 'South Awranggabad', 18, 5, 5, 3, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:36:06'),
(4, 'Pankundu', 18, 5, 5, 4, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Barrah', 18, 5, 5, 5, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Asta', 18, 5, 5, 6, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:40:18'),
(9, 'East Dhuyair', 18, 5, 5, 9, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:37:04'),
(10, 'North Shilakotha', 18, 5, 6, 10, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:44:40'),
(11, 'South Shilakotha', 18, 5, 6, 11, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:45:14'),
(12, 'Deovogh', 18, 5, 6, 12, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:46:21'),
(13, 'Charpurulia', 18, 5, 6, 12, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:46:47'),
(14, 'Sundharipara', 18, 5, 6, 12, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:47:04'),
(15, 'Kartikpur', 18, 5, 6, 13, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Boro Rasta Part (Part of 5no Ward)', 18, 5, 6, 14, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:57:16'),
(18, 'Boro Rasta Part (Part of 6no Ward)', 18, 5, 6, 15, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:57:29'),
(19, 'Choto Rasta', 18, 5, 6, 15, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:57:46'),
(20, 'Babur Dangi', 18, 5, 6, 15, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:58:05'),
(21, 'Charkushai (part of ward no 7)', 18, 5, 6, 16, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:59:19'),
(23, 'Charkushai (part of 8 no ward)', 18, 5, 6, 17, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 10:59:29'),
(25, 'Mahtabnagar', 18, 5, 6, 18, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 11:01:38'),
(26, 'Arita', 18, 5, 6, 18, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Awliyabad', 18, 5, 6, 18, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 11:01:06'),
(28, 'Imamnagar', 18, 5, 6, 18, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 11:01:25'),
(29, 'Ikrashi  North', 18, 5, 7, 19, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 11:25:19'),
(31, 'Haturpara', 18, 5, 7, 20, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Palamganj', 18, 5, 7, 20, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 11:26:02'),
(33, 'Jamalchar', 18, 5, 7, 21, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Karimganj', 18, 5, 7, 21, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 11:26:51'),
(35, 'Roghudebpur', 18, 5, 7, 22, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 11:26:34'),
(36, 'Raipara North', 18, 5, 7, 22, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 11:27:14'),
(44, 'Katakhali', 18, 5, 8, 28, 0, 773, 707, '0000-00-00 00:00:00', '2018-10-06 19:13:31'),
(46, 'Banaghata (West part)', 18, 5, 8, 30, 0, 1165, 1092, '0000-00-00 00:00:00', '2018-10-06 19:14:42'),
(47, 'Doharghata', 18, 5, 8, 31, 0, 1955, 1875, '0000-00-00 00:00:00', '2018-10-06 19:15:44'),
(48, 'Dohar', 18, 5, 8, 32, 0, 1010, 920, '0000-00-00 00:00:00', '2018-10-06 19:16:26'),
(49, 'Modhurchar (east part)', 18, 5, 8, 33, 0, 335, 412, '0000-00-00 00:00:00', '2018-10-06 19:18:32'),
(50, 'West Sutarpara', 18, 5, 8, 34, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 12:22:19'),
(51, 'Kajirchar', 18, 5, 8, 35, 0, 621, 599, '0000-00-00 00:00:00', '2018-10-06 19:16:44'),
(52, 'East Sutarpara', 18, 5, 8, 36, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 12:22:05'),
(53, 'Gajirtek', 18, 5, 8, 36, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 12:22:35'),
(54, 'Daiyarkum', 18, 5, 8, 36, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 12:22:51'),
(55, 'Daiya Gojariya', 18, 5, 8, 36, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 12:23:26'),
(57, 'Gharmora', 18, 5, 8, 36, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'Munsikanda', 18, 5, 8, 36, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'Mokshudpur (Part of 1 no ward)', 18, 5, 10, 37, 0, 481, 442, '0000-00-00 00:00:00', '2018-10-06 14:23:48'),
(60, 'Mokshudpur (part of 2 no Ward)', 18, 5, 10, 38, 0, 750, 721, '0000-00-00 00:00:00', '2018-10-06 14:24:09'),
(61, 'Mokshudpur (Part of 3 no ward)', 18, 5, 10, 39, 0, 916, 802, '0000-00-00 00:00:00', '2018-10-06 14:24:32'),
(62, 'Dhalarpar', 18, 5, 10, 40, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'Khariya  North', 18, 5, 10, 40, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:24:59'),
(64, 'Khasertek', 18, 5, 10, 40, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'Baniyabari', 18, 5, 10, 40, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:27:24'),
(67, 'Bethuya', 18, 5, 10, 41, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'Ultadangi (shantinagar)', 18, 5, 10, 42, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:26:18'),
(69, 'South Modhurkhola', 18, 5, 10, 42, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:27:57'),
(70, 'Modhurkhula', 18, 5, 10, 42, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'Moitpara', 18, 5, 10, 43, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'Chatravog', 18, 5, 10, 44, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:29:35'),
(76, 'Dhitpur', 18, 5, 10, 45, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:31:30'),
(77, 'Moura North', 18, 5, 10, 45, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:30:42'),
(78, 'Charboita', 18, 5, 11, 46, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:42:30'),
(79, 'Char Bilashpur', 18, 5, 11, 47, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:44:53'),
(81, 'Harichandi', 18, 5, 11, 49, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:44:35'),
(82, 'Hosenpur', 18, 5, 11, 50, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:41:34'),
(85, 'Moinot', 18, 5, 11, 53, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'Narayanpur', 18, 5, 11, 54, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'Purulia', 18, 5, 11, 54, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'Mahmudpur(north side of Katakhal)', 18, 5, 11, 54, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:44:21'),
(90, 'Shrikrishnapur', 18, 5, 11, 54, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:42:42'),
(91, 'Bilaspur', 18, 5, 12, 55, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'Choto Ramnathpur', 18, 5, 12, 55, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:50:37'),
(93, 'Hajharbigha', 18, 5, 12, 56, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:50:56'),
(94, 'Alamkha Char', 18, 5, 12, 56, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:53:00'),
(95, 'Alinagar', 18, 5, 12, 56, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Majhirchar', 18, 5, 12, 57, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'Boro Ramnathpur', 18, 5, 12, 57, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:51:51'),
(99, 'Purbachar', 18, 5, 12, 58, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:52:25'),
(100, 'Radhanagar North', 18, 5, 12, 58, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:52:39'),
(101, 'Radhanagar South', 18, 5, 12, 59, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:55:00'),
(103, 'Kutubpur', 18, 5, 12, 61, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'Debinagar South', 18, 5, 12, 62, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:54:38'),
(105, 'Kulchuri', 18, 5, 12, 63, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'Krishnadebpur', 18, 5, 12, 63, 0, 0, 0, '0000-00-00 00:00:00', '2018-10-06 14:55:34'),
(107, 'Anondanagar', 18, 6, 13, 64, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:02:14'),
(108, 'Monikanda', 18, 6, 13, 64, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(109, 'Moheshpur', 18, 6, 13, 65, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:03:19'),
(110, 'Bishompur', 18, 6, 13, 65, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:02:24'),
(111, 'Sonatola', 18, 6, 13, 65, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(112, 'Shibpur', 18, 6, 13, 66, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:02:50'),
(114, 'Notti', 18, 6, 13, 66, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(115, 'Nurpur', 18, 6, 13, 66, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(116, 'Vodrokanda', 18, 6, 13, 66, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:03:31'),
(117, 'Shikaripara', 18, 6, 13, 67, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(118, 'Komduppur', 18, 6, 13, 67, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(119, 'Loskorkanda', 18, 6, 13, 68, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(120, 'Charbaguni', 18, 6, 13, 68, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(121, 'Hagradi', 18, 6, 13, 68, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(122, 'Norshikhopur', 18, 6, 13, 68, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(123, 'Noyadangi', 18, 6, 13, 68, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:09:12'),
(124, 'Garibpur', 18, 6, 13, 69, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:09:25'),
(125, 'Boktarnagar', 18, 6, 13, 69, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:09:35'),
(126, 'Sherpur', 18, 6, 13, 70, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(127, 'Sujapur', 18, 6, 13, 70, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(128, 'Narayanpur', 18, 6, 13, 71, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(129, 'Daudpur', 18, 6, 13, 71, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(130, 'Panjiprohori', 18, 6, 13, 72, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:10:08'),
(131, 'Gongadiya', 18, 6, 13, 72, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:09:48'),
(132, 'Shonabajhu', 18, 6, 14, 73, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:22:39'),
(133, 'Shankardiya', 18, 6, 14, 74, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:21:38'),
(134, 'Bahadurpur', 18, 6, 14, 74, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:23:22'),
(135, 'Joykrishnapur', 18, 6, 14, 74, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(136, 'Shyampur', 18, 6, 14, 74, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:21:51'),
(137, 'Gojariay', 18, 6, 14, 74, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:22:25'),
(138, 'Bilchuri', 18, 6, 14, 74, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:22:04'),
(139, 'Kollyanshri', 18, 6, 14, 75, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:23:09'),
(140, 'Titopoldiya', 18, 6, 14, 75, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:22:53'),
(141, 'Panikawr', 18, 6, 14, 75, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:23:34'),
(142, 'Kuthuri', 18, 6, 14, 76, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(143, 'Bahuyaghati', 18, 6, 14, 76, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:28:14'),
(144, 'Brahmangram', 18, 6, 14, 76, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:26:52'),
(145, 'Ashoypur', 18, 6, 14, 76, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:24:49'),
(146, 'Raypur', 18, 6, 14, 77, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:26:27'),
(147, 'Kedharpur', 18, 6, 14, 77, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:27:34'),
(148, 'Ghoshail', 18, 6, 14, 78, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:27:15'),
(149, 'Ar Ghoshai', 18, 6, 14, 78, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:27:23'),
(152, 'Rajhapur Noyadangi', 18, 6, 14, 79, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:30:15'),
(153, 'Bataimuri', 18, 6, 14, 79, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:31:32'),
(154, 'Balenga', 18, 6, 14, 80, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:28:44'),
(155, 'Dhoyair', 18, 6, 14, 80, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:30:04'),
(156, 'Purbachak', 18, 6, 14, 81, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:30:46'),
(157, 'Mothbari', 18, 6, 14, 81, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(158, 'Charakhali', 18, 6, 14, 81, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(159, 'Batoijuri', 18, 6, 14, 81, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:31:53'),
(160, 'Daktarkanda', 18, 6, 15, 82, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:45:05'),
(161, 'Baherchar', 18, 6, 15, 82, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:44:56'),
(162, 'Baruakhali North', 18, 6, 15, 82, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:44:01'),
(165, 'Madla', 18, 6, 15, 83, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:44:14'),
(166, 'Baruakhali South', 18, 6, 15, 83, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:44:44'),
(167, 'Chakbarilla', 18, 6, 15, 84, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:44:23'),
(168, 'Dhirrghogram', 18, 6, 15, 84, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:51:13'),
(169, 'Joinotpur', 18, 6, 15, 84, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(170, 'Vangagora', 18, 6, 15, 84, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:51:45'),
(171, 'Kar Para', 18, 6, 15, 84, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:50:32'),
(172, 'Dharikandi', 18, 6, 15, 85, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:46:31'),
(173, 'Veramuriya', 18, 6, 15, 85, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:46:45'),
(174, 'Kandabarilla', 18, 6, 15, 85, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:47:56'),
(175, 'Kumarbarilla', 18, 6, 15, 85, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:47:24'),
(176, 'Borobarilla', 18, 6, 15, 86, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:48:06'),
(177, 'Chatrapur', 18, 6, 15, 86, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:50:47'),
(178, 'Shiyaljaan', 18, 6, 15, 86, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:51:25'),
(179, 'Munshinagar', 18, 6, 15, 87, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:50:03'),
(180, 'Alalpur North', 18, 6, 15, 87, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:50:15'),
(181, 'Alalpur South', 18, 6, 15, 88, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:49:48'),
(183, 'Ramnagar', 18, 6, 15, 89, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(185, 'Jahanabaad', 18, 6, 15, 89, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:52:40'),
(186, 'Kanchannagar', 18, 6, 15, 90, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:53:44'),
(187, 'Boro Kawniyakandi', 18, 6, 15, 90, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:52:20'),
(188, 'Choto Kawniyakandi', 18, 6, 15, 90, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 15:52:31'),
(189, 'Sahebganj', 18, 6, 16, 91, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:41:17'),
(190, 'North Bahra', 18, 6, 16, 91, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:41:28'),
(191, 'Ghoshpara', 18, 6, 16, 91, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:40:29'),
(192, 'Vaowadubi', 18, 6, 16, 92, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:40:43'),
(193, 'Vurakhali', 18, 6, 16, 92, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(194, 'Choto Tashulla', 18, 6, 16, 92, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:41:42'),
(195, 'Bipratashulla', 18, 6, 16, 92, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:43:04'),
(196, 'Bipulla', 18, 6, 16, 93, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:41:59'),
(197, 'Char Tuitail', 18, 6, 16, 93, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:42:55'),
(199, 'Notun Tuitail', 18, 6, 16, 93, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:44:12'),
(200, 'Bokchar', 18, 6, 16, 93, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:43:41'),
(201, 'Charbaghuri', 18, 6, 16, 94, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:43:59'),
(202, 'Afzalnagar', 18, 6, 16, 94, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:44:22'),
(203, 'Char Shoilla', 18, 6, 16, 94, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:48:24'),
(204, 'Khanepur Kanda', 18, 6, 16, 94, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:48:04'),
(205, 'Kandakhanepur', 18, 6, 16, 95, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:47:48'),
(206, 'Char Khanepur', 18, 6, 16, 95, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:49:19'),
(207, 'Radhakantopur', 18, 6, 16, 95, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(208, 'Rahutghati', 18, 6, 16, 96, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:49:07'),
(209, 'Nayansree', 18, 6, 16, 96, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(210, 'Shahjadpur', 18, 6, 16, 97, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:59:40'),
(211, 'Krishnanagar', 18, 6, 16, 97, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:50:59'),
(212, 'Deotola', 18, 6, 16, 98, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:51:10'),
(213, 'Padrikanda', 18, 6, 16, 98, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:50:24'),
(214, 'Sap Leja', 18, 6, 16, 98, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:49:41'),
(215, 'Mataber Tek', 18, 6, 16, 99, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:50:39'),
(216, 'Purba Shoytankathi', 18, 6, 16, 99, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:59:30'),
(217, 'Choto Golla', 18, 6, 16, 99, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(218, 'Boro Golla', 18, 6, 16, 99, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(219, 'Kumar Golla', 18, 6, 16, 99, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 16:50:50'),
(220, 'Krishnonagar', 18, 6, 16, 99, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(221, 'Dokkhin Balukhanda', 18, 6, 17, 100, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:04:06'),
(222, 'North Balukhanda', 18, 6, 17, 100, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:00:46'),
(223, 'Cumulli', 18, 6, 17, 100, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:02:22'),
(224, 'Patiljhap', 18, 6, 17, 101, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:04:55'),
(226, 'Parashora', 18, 6, 17, 101, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:10:01'),
(227, 'Abdani', 18, 6, 17, 0, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:10:22'),
(228, 'Chokoria', 18, 6, 17, 103, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(229, 'Khatia', 18, 6, 17, 103, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:05:53'),
(230, 'Jhonjhonia', 18, 6, 17, 103, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:09:38'),
(231, 'Ajgora', 18, 6, 17, 103, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(232, 'Modonmohonpur', 18, 6, 17, 103, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:12:37'),
(233, 'Konda', 18, 6, 17, 104, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(234, 'Auna', 18, 6, 17, 104, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(236, 'Dudhghata', 18, 6, 17, 104, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:02:35'),
(237, 'Dutta Khanda', 18, 6, 17, 105, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:01:07'),
(241, 'Shinghora Aikbari', 18, 6, 17, 106, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:11:05'),
(242, 'Noyahati', 18, 6, 17, 107, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:10:11'),
(244, 'Sholla Nagar', 18, 6, 17, 108, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:05:35'),
(245, 'Bowali', 18, 6, 17, 108, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:14:57'),
(246, 'Shingjor', 18, 6, 17, 108, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:11:54'),
(247, 'Kartikpur', 18, 6, 17, 0, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:13:49'),
(248, 'Sultanpur', 18, 6, 17, 0, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:12:22'),
(249, 'Ulail', 18, 6, 17, 0, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:13:32'),
(250, 'Chok Auna', 18, 6, 17, 100, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:14:46'),
(251, 'Jantrail(Part of Ward No. 1)', 18, 6, 18, 110, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:35:16'),
(252, 'Ajijpur', 18, 6, 18, 110, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:42:47'),
(253, 'Chondrokhola', 18, 6, 18, 111, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:36:11'),
(254, 'Vaoyaliya', 18, 6, 18, 112, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:36:21'),
(255, 'Jalalchor', 18, 6, 18, 113, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(256, 'Horishkul (Part of Ward No. 6)', 18, 6, 18, 114, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:37:04'),
(257, 'Debukhali', 18, 6, 18, 115, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(258, 'Horishkul (Part of Ward No. 5)', 18, 6, 18, 116, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:36:53'),
(259, 'East Gobindopur', 18, 6, 18, 117, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 13:59:30'),
(260, 'Moymondi', 18, 6, 18, 117, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(261, 'Mohobbotpur', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(262, 'Majirkanda', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(266, 'Imamnagar', 18, 6, 19, 119, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(267, 'Sayedpur', 18, 6, 19, 120, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(270, 'Khatalighata', 18, 6, 19, 120, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(272, 'Hasanabad (Part of Ward No. 5)', 18, 6, 19, 122, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 17:54:49'),
(273, 'Molasikanda', 18, 6, 19, 122, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(281, 'Bardurari', 18, 6, 19, 126, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(282, 'Champanagar', 18, 6, 19, 126, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(283, 'Goalnagar', 18, 6, 20, 127, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(558, 'West Somsabad', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(288, 'Baghati', 18, 6, 20, 128, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(570, 'Dokkhin Sholla', 18, 6, 17, 100, 0, 0, 0, '2018-10-06 17:08:12', '2018-10-06 17:08:12'),
(296, 'Panalia', 18, 6, 20, 130, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(298, 'Nowabganj', 18, 6, 20, 131, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(572, 'Chokoria Chokbari', 18, 6, 17, 100, 0, 0, 0, '2018-10-06 17:11:40', '2018-10-06 17:11:40'),
(302, 'Baghmara', 18, 6, 20, 134, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(303, 'Amirpur', 18, 6, 20, 134, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(304, 'Jalalpur', 18, 6, 20, 134, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(306, 'Boikontopur', 18, 6, 20, 134, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(307, 'Rajpara', 18, 6, 20, 135, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(308, 'Pirmamudia', 18, 6, 20, 135, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(309, 'Bibirchor', 18, 6, 20, 135, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(530, 'Boro Bakshanagar', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:28:00', '2018-10-06 16:28:00'),
(312, 'Choto Rajpara', 18, 6, 21, 138, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(316, 'Digirpar', 18, 6, 21, 142, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(317, 'Balurchor', 18, 6, 21, 143, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(320, 'Kahur', 18, 6, 22, 146, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(322, 'Kandamatra', 18, 6, 22, 148, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(323, 'Baghbari', 18, 6, 22, 149, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(326, 'Shovoria', 18, 6, 22, 152, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(330, 'Telenga', 18, 6, 23, 156, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(331, 'Paragram', 18, 6, 23, 157, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(332, 'Dorikanda', 18, 6, 23, 158, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(333, 'Modhupur', 18, 6, 23, 159, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(334, 'Nayakanda', 18, 6, 23, 160, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(335, 'Malikanda', 18, 6, 23, 161, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(337, 'Raipur', 18, 6, 23, 162, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(346, 'Doulatpur', 18, 6, 23, 162, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(465, 'Majhpara', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:41:53', '2018-10-06 14:41:53'),
(464, 'Deovog', 18, 5, 11, 46, 0, 0, 0, '2018-10-06 14:41:22', '2018-10-06 14:41:22'),
(463, 'Shukhdebpur', 18, 5, 11, 46, 0, 0, 0, '2018-10-06 14:41:02', '2018-10-06 14:41:02'),
(354, 'Agla', 18, 6, 24, 170, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(355, 'Agla East para', 18, 6, 24, 171, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 14:41:28'),
(357, 'Kuthibari', 18, 6, 25, 173, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(359, 'Miahati', 18, 6, 25, 175, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(363, 'Borogram', 18, 6, 25, 179, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(364, 'Surjokhali', 18, 6, 25, 180, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(365, 'Shahbad', 18, 6, 25, 180, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(366, 'Khanhati', 18, 6, 25, 180, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(367, 'Nagar', 18, 6, 25, 180, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(368, 'Ramnathpur', 18, 6, 25, 180, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(369, 'Chanhati', 18, 6, 25, 180, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(370, 'Sonahajra', 18, 6, 25, 180, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(371, 'Sonkarkhali', 18, 6, 25, 180, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(372, 'Paiksha', 18, 6, 25, 180, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(373, 'West  Gobindopur', 18, 6, 26, 181, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 13:47:53'),
(374, 'Churahati', 18, 6, 26, 181, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(375, 'Muslemhati', 18, 6, 26, 182, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(376, 'Sonatola', 18, 6, 26, 183, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(377, 'Paiksha', 18, 6, 26, 184, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(378, 'Songarkhali', 18, 6, 26, 185, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(379, 'Sona Hajra', 18, 6, 26, 186, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(380, 'Chanhati', 18, 6, 26, 187, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(383, 'East Churain', 18, 6, 26, 188, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-06 13:57:18'),
(387, 'Modonkhali', 18, 6, 26, 188, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(388, 'Morispotti', 18, 6, 26, 188, 0, 0, 0, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(389, 'Nosratpur', 18, 5, 5, 1, 0, 0, 0, '2018-10-06 10:27:14', '2018-10-06 10:27:14'),
(390, 'Hatni', 18, 5, 5, 0, 0, 0, 0, '2018-10-06 10:38:08', '2018-10-06 10:38:08'),
(391, 'Char Hatni', 18, 5, 5, 1, 0, 0, 0, '2018-10-06 10:38:33', '2018-10-06 10:38:33'),
(392, 'Kusumhati', 18, 5, 6, 10, 0, 0, 0, '2018-10-06 10:48:40', '2018-10-06 10:48:40'),
(393, 'Joymangal', 18, 5, 6, 10, 0, 0, 0, '2018-10-06 11:00:27', '2018-10-06 11:00:27'),
(394, 'Ikrashi  South', 18, 5, 7, 19, 0, 0, 0, '2018-10-06 11:25:46', '2018-10-06 11:25:46'),
(395, 'Raipara South', 18, 5, 7, 19, 0, 0, 0, '2018-10-06 11:27:47', '2018-10-06 11:27:47'),
(396, 'Nagorkanda', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:40:40', '2018-10-06 11:40:40'),
(397, 'Lotakhola Biler Par', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:41:10', '2018-10-06 11:41:10'),
(398, 'West Lota khola', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:41:38', '2018-10-06 11:41:38'),
(399, 'Moddho Lotakhola', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:41:58', '2018-10-06 11:41:58'),
(400, 'East Lotakhola', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:42:15', '2018-10-06 11:42:15'),
(401, 'North Char Joypara', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:42:36', '2018-10-06 11:42:36'),
(402, 'South Char Joypara', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:42:58', '2018-10-06 11:42:58'),
(403, 'Kathalighata', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:43:27', '2018-10-06 11:43:27'),
(404, 'Khalpar (north part of the Khal)', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:43:58', '2018-10-06 11:43:58'),
(405, 'North Joypara Khal(North part of the Khal)', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:44:41', '2018-10-06 11:44:41'),
(406, 'Islampur', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:45:00', '2018-10-06 11:45:00'),
(408, 'North Joypara khalpar (South part of khal)', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:46:29', '2018-10-06 11:46:29'),
(409, 'North Joypara gajikanda', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:46:48', '2018-10-06 11:46:48'),
(410, 'North Joypara Miyapara', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:47:08', '2018-10-06 11:47:08'),
(411, 'North Joypara Byangarchar', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:47:29', '2018-10-06 11:47:29'),
(412, 'North Joypara Chowdhurypara', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:47:54', '2018-10-06 11:47:54'),
(413, 'North Joypara Kutibari', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:48:12', '2018-10-06 11:48:12'),
(414, 'Botia', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:48:29', '2018-10-06 11:48:29'),
(415, 'Nurpur (west side of WAPDA road )', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:48:49', '2018-10-06 11:48:49'),
(416, 'South Joypara Gangpar', 18, 5, 27, 190, 0, 0, 0, '2018-10-06 11:49:44', '2018-10-06 11:49:44'),
(417, 'South Joypara ghona', 18, 5, 27, 190, 0, 327, 361, '2018-10-06 11:50:04', '2018-10-06 19:06:37'),
(418, 'Charlotakhola', 18, 5, 27, 190, 0, 1524, 1584, '2018-10-06 11:50:45', '2018-10-06 19:07:02'),
(419, 'South Joypara majhipara', 18, 5, 27, 190, 0, 671, 667, '2018-10-06 11:51:20', '2018-10-06 19:07:25'),
(420, 'Murpur', 18, 5, 27, 190, 0, 227, 242, '2018-10-06 11:52:14', '2018-10-06 19:07:59'),
(421, 'South Joypara Kharakanda', 18, 5, 27, 190, 0, 1047, 1068, '2018-10-06 11:52:49', '2018-10-06 19:08:20'),
(422, 'South Joypara', 18, 5, 27, 190, 0, 471, 454, '2018-10-06 11:53:19', '2018-10-06 19:08:43'),
(423, 'Loskorkanda', 18, 5, 27, 190, 0, 304, 273, '2018-10-06 11:54:09', '2018-10-06 19:09:04'),
(424, 'Rasulpur', 18, 5, 27, 190, 0, 160, 164, '2018-10-06 11:54:28', '2018-10-06 19:09:23'),
(425, 'North Yousufpur', 18, 5, 27, 190, 0, 787, 771, '2018-10-06 11:54:47', '2018-10-06 19:09:51'),
(426, 'South Yousufpur', 18, 5, 27, 190, 0, 350, 358, '2018-10-06 11:55:04', '2018-10-06 19:10:05'),
(427, 'Nikra (West part)', 18, 5, 8, 28, 0, 329, 344, '2018-10-06 12:19:51', '2018-10-06 19:13:52'),
(428, 'Banaghata (East part)', 18, 5, 8, 30, 0, 53, 76, '2018-10-06 12:20:36', '2018-10-06 19:14:55'),
(429, 'Nikra (East part)', 18, 5, 8, 30, 0, 163, 165, '2018-10-06 12:20:59', '2018-10-06 19:15:16'),
(430, 'Jhanki', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:32:10', '2018-10-06 12:32:10'),
(431, 'Uttar shimuliya', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:32:36', '2018-10-06 12:32:36'),
(432, 'Malikanda', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:35:50', '2018-10-06 12:35:50'),
(433, 'Meghula', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:36:42', '2018-10-06 12:36:42'),
(434, 'South Shimuliya', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:37:07', '2018-10-06 12:37:07'),
(435, 'Narisha Paschimchar', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:37:30', '2018-10-06 12:37:30'),
(436, 'Narisha', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:37:57', '2018-10-06 12:37:57'),
(437, 'Narisha  Khalpar', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:38:18', '2018-10-06 12:38:18'),
(438, 'Narisha Choitabator', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:38:38', '2018-10-06 12:38:38'),
(439, 'Ruikha', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:38:58', '2018-10-06 12:38:58'),
(440, 'Satvita', 18, 5, 9, 191, 0, 0, 0, '2018-10-06 12:39:16', '2018-10-06 12:39:16'),
(441, 'West Churain', 18, 6, 26, 181, 0, 0, 0, '2018-10-06 13:56:51', '2018-10-06 13:56:51'),
(455, 'Purbachar', 18, 5, 10, 37, 0, 302, 323, '2018-10-06 14:23:12', '2018-10-06 14:23:12'),
(443, 'West Munshinogor', 18, 6, 26, 181, 0, 0, 0, '2018-10-06 14:02:35', '2018-10-06 14:02:35'),
(444, 'East Munshinogor', 18, 6, 26, 181, 0, 0, 0, '2018-10-06 14:05:19', '2018-10-06 14:05:19'),
(445, 'Kamarkhola', 18, 6, 26, 181, 0, 0, 0, '2018-10-06 14:06:08', '2018-10-06 14:06:08'),
(461, 'Mukimpur', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:38:21', '2018-10-06 14:38:21'),
(460, 'Moura south', 18, 5, 10, 37, 0, 0, 0, '2018-10-06 14:30:13', '2018-10-06 14:30:13'),
(449, 'Andharkotha', 18, 6, 25, 172, 0, 0, 0, '2018-10-06 14:11:00', '2018-10-06 14:11:00'),
(450, 'Noyadda', 18, 6, 25, 172, 0, 0, 0, '2018-10-06 14:11:35', '2018-10-06 14:11:35'),
(458, 'Horiyar tek', 18, 5, 10, 37, 0, 0, 0, '2018-10-06 14:28:44', '2018-10-06 14:28:44'),
(452, 'Joynogor surgonj', 18, 6, 25, 172, 0, 0, 0, '2018-10-06 14:12:53', '2018-10-06 14:12:53'),
(453, 'East Gobindopur', 18, 6, 26, 181, 0, 0, 0, '2018-10-06 14:19:02', '2018-10-06 14:19:02'),
(454, 'Durgapur', 18, 6, 26, 181, 0, 0, 0, '2018-10-06 14:19:59', '2018-10-06 14:19:59'),
(456, 'Khariya South', 18, 5, 10, 37, 0, 0, 0, '2018-10-06 14:25:51', '2018-10-06 14:25:51'),
(457, 'Gorabon', 18, 5, 10, 37, 0, 0, 0, '2018-10-06 14:26:55', '2018-10-06 14:26:55'),
(459, 'Ruita', 18, 5, 10, 37, 0, 0, 0, '2018-10-06 14:29:20', '2018-10-06 14:29:20'),
(462, 'Hajikanda', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:38:47', '2018-10-06 14:38:47'),
(466, 'Char Kushumhati', 18, 5, 11, 46, 0, 0, 0, '2018-10-06 14:42:15', '2018-10-06 14:42:15'),
(467, 'Kaluyahati North', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:42:35', '2018-10-06 14:43:05'),
(468, 'Charkushaichar (part of 4 no ward)', 18, 5, 11, 46, 0, 0, 0, '2018-10-06 14:43:24', '2018-10-06 14:43:24'),
(469, 'Kaluyahati South', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:43:31', '2018-10-06 14:43:31'),
(470, 'Charkushaichar (part of 5 no ward)', 18, 5, 11, 46, 0, 0, 0, '2018-10-06 14:43:47', '2018-10-06 14:43:47'),
(471, 'Tikorpur', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:44:03', '2018-10-06 14:44:03'),
(472, 'Charkushaichar (part of 7 no ward)', 18, 5, 11, 46, 0, 0, 0, '2018-10-06 14:44:09', '2018-10-06 14:44:09'),
(473, 'Mahmudpur south side of Katakha', 18, 5, 11, 46, 0, 0, 0, '2018-10-06 14:45:12', '2018-10-06 14:45:12'),
(474, 'Mohonpur', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:45:35', '2018-10-06 14:45:35'),
(475, 'Chatiya', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:46:17', '2018-10-06 14:46:17'),
(476, 'Chorchoriya', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:46:43', '2018-10-06 14:46:43'),
(477, 'Chor Modhuchoriya', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:47:32', '2018-10-06 14:47:32'),
(478, 'Gokulnogor', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:48:10', '2018-10-06 14:48:10'),
(479, 'Benukhali', 18, 6, 24, 163, 0, 0, 0, '2018-10-06 14:49:17', '2018-10-06 14:49:17'),
(480, 'Char Roghudebpur', 18, 5, 12, 55, 0, 0, 0, '2018-10-06 14:51:36', '2018-10-06 14:51:36'),
(481, 'Koilail South', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:08:01', '2018-10-06 15:08:01'),
(483, 'Koilail North', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:09:42', '2018-10-06 15:09:42'),
(484, 'Sonargaon', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:10:13', '2018-10-06 15:10:13'),
(485, 'Meleng Dokkhin', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:10:50', '2018-10-06 15:10:50'),
(486, 'Meleng North', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:11:33', '2018-10-06 15:11:33'),
(487, 'Meleng East', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:12:00', '2018-10-06 15:12:00'),
(488, 'Meleng West (Khal Paar)', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:12:28', '2018-10-06 15:12:28'),
(489, 'Dignara', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:15:17', '2018-10-06 15:15:17'),
(490, 'Mashail', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:15:58', '2018-10-06 15:15:58'),
(492, 'Molla Kanda', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:18:04', '2018-10-06 15:18:04'),
(493, 'Bhangavita', 18, 6, 23, 154, 0, 0, 0, '2018-10-06 15:18:40', '2018-10-06 15:18:40'),
(499, 'Boro Bolomonttochor', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:40:56', '2018-10-06 15:40:56'),
(496, 'Koilail Bagmara', 18, 6, 23, 0, 0, 0, 0, '2018-10-06 15:24:02', '2018-10-06 15:24:02'),
(497, 'Ghatbazar', 18, 6, 14, 73, 0, 0, 0, '2018-10-06 15:26:14', '2018-10-06 15:26:14'),
(498, 'Kastartek', 18, 6, 14, 73, 0, 0, 0, '2018-10-06 15:29:49', '2018-10-06 15:29:49'),
(500, 'Choto Bolomonttochor', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:42:40', '2018-10-06 15:42:40'),
(501, 'Alamgirchor', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:43:08', '2018-10-06 15:43:08'),
(502, 'Urarchor', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:43:39', '2018-10-06 15:43:39'),
(525, 'Komorgonj', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:21:01', '2018-10-06 16:21:01'),
(504, 'Bramhankhali', 18, 6, 15, 82, 0, 0, 0, '2018-10-06 15:45:39', '2018-10-06 15:45:39'),
(524, 'Pathankanda (Part of ward no. 1)', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:20:21', '2018-10-06 16:21:49'),
(506, 'Choto Nabagram', 18, 6, 15, 0, 0, 0, 0, '2018-10-06 15:46:13', '2018-10-06 15:46:13'),
(507, 'Boro Baharra Chakuri Para', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:46:35', '2018-10-06 15:46:35'),
(511, 'Boro Priti Noyadda', 18, 6, 15, 82, 0, 0, 0, '2018-10-06 15:48:58', '2018-10-06 15:48:58'),
(512, 'Baharra Mollakanda', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:49:01', '2018-10-06 15:49:16'),
(513, 'Choto Priti Noyadda', 18, 6, 15, 82, 0, 0, 0, '2018-10-06 15:49:24', '2018-10-06 15:49:24'),
(514, 'Mylaile', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:49:52', '2018-10-06 15:49:52'),
(515, 'Baharra Chorkanda', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:50:24', '2018-10-06 15:50:24'),
(516, 'North Chowkighata', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:50:46', '2018-10-06 15:50:46'),
(517, 'South Chowkighata', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:51:24', '2018-10-06 15:51:24'),
(518, 'Boro Baharra East & North', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:51:49', '2018-10-06 15:51:49'),
(519, 'East Chok Baharra', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:52:26', '2018-10-06 15:52:26'),
(520, 'Baharra Sonargaon', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:52:56', '2018-10-06 15:52:56'),
(521, 'Uluman Chandura', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:53:27', '2018-10-06 15:53:27'),
(522, 'Baniyagram', 18, 6, 15, 82, 0, 0, 0, '2018-10-06 15:53:31', '2018-10-06 15:53:31'),
(523, 'Naopara', 18, 6, 22, 145, 0, 0, 0, '2018-10-06 15:54:15', '2018-10-06 15:54:15'),
(526, 'Gajikhali', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:21:21', '2018-10-06 16:21:21'),
(527, 'Pathankanda (Part of ward no. 2)', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:22:51', '2018-10-06 16:22:51'),
(528, 'Choto Bakshanagar', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:23:59', '2018-10-06 16:23:59'),
(529, 'Boro Bakshanagar Sabekhati', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:24:45', '2018-10-06 16:24:45'),
(531, 'Bakshanagar Chowrahoti', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:29:31', '2018-10-06 16:29:31'),
(532, 'Bakshanagar Christianhati', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:30:34', '2018-10-06 16:30:34'),
(533, 'Dhighir para', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:31:07', '2018-10-06 16:31:07'),
(534, 'Surgonj Tukinikanda', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:31:48', '2018-10-06 16:31:48'),
(535, 'Bordhonpara  (Part of ward no. 2)', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:35:38', '2018-10-06 16:35:38'),
(536, 'Bordhonpara (Part of ward no. 3)', 18, 6, 21, 136, 0, 0, 0, '2018-10-06 16:36:11', '2018-10-06 16:36:11'),
(537, 'Boro Tashulla', 18, 6, 16, 91, 0, 0, 0, '2018-10-06 16:42:36', '2018-10-06 16:42:36'),
(538, 'Tuitail', 18, 6, 16, 91, 0, 0, 0, '2018-10-06 16:43:31', '2018-10-06 16:43:31'),
(539, 'Shoilla', 18, 6, 16, 91, 0, 0, 0, '2018-10-06 16:47:34', '2018-10-06 16:47:34'),
(542, 'Dhori Deotola', 18, 6, 16, 91, 0, 0, 0, '2018-10-06 16:51:47', '2018-10-06 16:51:47'),
(575, 'Jantrail (Part of Ward No. 2)', 18, 6, 18, 109, 0, 0, 0, '2018-10-06 17:35:54', '2018-10-06 17:35:54'),
(556, 'Mohijdia', 18, 6, 17, 100, 0, 0, 0, '2018-10-06 17:02:11', '2018-10-06 17:02:11'),
(548, 'Voirahati', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(549, 'Rajarampur', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(553, 'Rayhati', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(554, 'Mohajonpur', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(555, 'Bhuiyahati', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(559, 'Boro Somsabad', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(560, 'Kashimpur', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(571, 'Dokkhin Purbo Shinghora', 18, 6, 17, 0, 0, 0, 0, '2018-10-06 17:10:46', '2018-10-06 17:10:46'),
(562, 'Surgonj', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(564, 'Khondokerhati', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(565, 'Madhobpur', 18, 6, 20, 17, 0, 0, 0, '2018-10-01 18:00:00', '2018-10-05 18:00:00'),
(574, 'Shinghora', 18, 6, 17, 100, 0, 0, 0, '2018-10-06 17:30:23', '2018-10-06 17:30:23'),
(573, 'Hayatkanda', 18, 6, 17, 100, 0, 0, 0, '2018-10-06 17:13:24', '2018-10-06 17:13:24'),
(576, 'Balidior', 18, 6, 18, 109, 0, 0, 0, '2018-10-06 17:38:08', '2018-10-06 17:38:08'),
(577, 'Chorkhlosi', 18, 6, 18, 109, 0, 0, 0, '2018-10-06 17:40:13', '2018-10-06 17:40:13'),
(578, 'Nobogram', 18, 6, 18, 109, 0, 0, 0, '2018-10-06 17:40:35', '2018-10-06 17:40:35'),
(579, 'Nolgora', 18, 6, 18, 109, 0, 0, 0, '2018-10-06 17:40:54', '2018-10-06 17:40:54'),
(580, 'Kirinchi', 18, 6, 18, 109, 0, 0, 0, '2018-10-06 17:41:13', '2018-10-06 17:41:13'),
(583, 'Boro Harikanda', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 12:00:00', '2018-10-05 12:00:00'),
(586, 'Shorupdi Kandi', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 12:00:00', '2018-10-05 12:00:00'),
(587, 'Old Bandura', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 12:00:00', '2018-10-05 12:00:00'),
(596, 'Molouvi Dangi', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 12:00:00', '2018-10-05 12:00:00'),
(589, 'Hasanabad  (Part of Ward No. 6)', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 12:00:00', '2018-10-05 12:00:00'),
(590, 'New Bandura (Part of ward No. 8)', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 12:00:00', '2018-10-05 12:00:00'),
(591, 'Mirer Dangi', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 12:00:00', '2018-10-05 12:00:00'),
(592, 'Nurongor', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 12:00:00', '2018-10-05 12:00:00'),
(597, 'New Bandura (Part of ward No. 7)', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 12:00:00', '2018-10-05 12:00:00'),
(595, 'Noyanogor', 18, 6, 19, 118, 0, 0, 0, '2018-10-05 12:00:00', '2018-10-05 12:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_ward`
--

CREATE TABLE `bol_election_ward` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `union_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_ward`
--

INSERT INTO `bol_election_ward` (`id`, `name`, `district_id`, `upazila_id`, `union_id`, `created_at`, `updated_at`) VALUES
(1, 'Ward 1', 18, 5, 5, '2018-10-02 18:00:00', '2018-10-02 18:00:00'),
(2, 'Ward 2', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Ward 3', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Ward 4', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Ward 5', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Ward 6', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Ward 7', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Ward 8', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Ward 9', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Ward 1', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Ward 2', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Ward 3', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Ward 4', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Ward 5', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Ward 6', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Ward 7', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Ward 8', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Ward 9', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Ward 1', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Ward 2', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Ward 3', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Ward 4', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Ward 5', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Ward 6', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Ward 7', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Ward 8', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Ward 9', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Ward 1', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Ward 2', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Ward 3', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Ward 4', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Ward 5', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Ward 6', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Ward 7', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Ward 8', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'Ward 9', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Ward 1', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Ward 2', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Ward 3', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'Ward 4', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'Ward 5', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'Ward 6', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'Ward 7', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Ward 8', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'Ward 9', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'Ward 1', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'Ward 2', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'Ward 3', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'Ward 4', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'Ward 5', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'Ward 6', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'Ward 7', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'Ward 8', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'Ward 9', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'Ward 1', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'Ward 2', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'Ward 3', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'Ward 4', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'Ward 5', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'Ward 6', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'Ward 7', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'Ward 8', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'Ward 9', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'Ward 1', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(65, 'Ward 2', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(66, 'Ward 3', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(67, 'Ward 4', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(68, 'Ward 5', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(69, 'Ward 6', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(70, 'Ward 7', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(71, 'Ward 8', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(72, 'Ward 9', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(73, 'Ward 1', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(74, 'Ward 2', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(75, 'Ward 3', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(76, 'Ward 4', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(77, 'Ward 5', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(78, 'Ward 6', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(79, 'Ward 7', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(80, 'Ward 8', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(81, 'Ward 9', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(82, 'Ward 1', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(83, 'Ward 2', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(84, 'Ward 3', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(85, 'Ward 4', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(86, 'Ward 5', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(87, 'Ward 6', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(88, 'Ward 7', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(89, 'Ward 8', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(90, 'Ward 9', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(91, 'Ward 1', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(92, 'Ward 2', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(93, 'Ward 3', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(94, 'Ward 4', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(95, 'Ward 5', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(96, 'Ward 6', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(97, 'Ward 7', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(98, 'Ward 8', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(99, 'Ward 9', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(100, 'Ward 1', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(101, 'Ward 2', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(102, 'Ward 3', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(103, 'Ward 4', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(104, 'Ward 5', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(105, 'Ward 6', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(106, 'Ward 7', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(107, 'Ward 8', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(108, 'Ward 9', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(109, 'Ward 1', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(110, 'Ward 2', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(111, 'Ward 3', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(112, 'Ward 4', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(113, 'Ward 5', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(114, 'Ward 6', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(115, 'Ward 7', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(116, 'Ward 8', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(117, 'Ward 9', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(118, 'Ward 1', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(119, 'Ward 2', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(120, 'Ward 3', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(121, 'Ward 4', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(122, 'Ward 5', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(123, 'Ward 6', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(124, 'Ward 7', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(125, 'Ward 8', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(126, 'Ward 9', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(127, 'Ward 1', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(128, 'Ward 2', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(129, 'Ward 3', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(130, 'Ward 4', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(131, 'Ward 5', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(132, 'Ward 6', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(133, 'Ward 7', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(134, 'Ward 8', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(135, 'Ward 9', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(136, 'Ward 1', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(137, 'Ward 2', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(138, 'Ward 3', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(139, 'Ward 4', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(140, 'Ward 5', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(141, 'Ward 6', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(142, 'Ward 7', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(143, 'Ward 8', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(144, 'Ward 9', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(145, 'Ward 1', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(146, 'Ward 2', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(147, 'Ward 3', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(148, 'Ward 4', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(149, 'Ward 5', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(150, 'Ward 6', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(151, 'Ward 7', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(152, 'Ward 8', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(153, 'Ward 9', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(154, 'Ward 1', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(155, 'Ward 2', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(156, 'Ward 3', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(157, 'Ward 4', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(158, 'Ward 5', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(159, 'Ward 6', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(160, 'Ward 7', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(161, 'Ward 8', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(162, 'Ward 9', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(163, 'Ward 1', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(164, 'Ward 2', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(165, 'Ward 3', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(166, 'Ward 4', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(167, 'Ward 5', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(168, 'Ward 6', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(169, 'Ward 7', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(170, 'Ward 8', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(171, 'Ward 9', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(172, 'Ward 1', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(173, 'Ward 2', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(174, 'Ward 3', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(175, 'Ward 4', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(176, 'Ward 5', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(177, 'Ward 6', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(178, 'Ward 7', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(179, 'Ward 8', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(180, 'Ward 9', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(181, 'Ward 1', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(182, 'Ward 2', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(183, 'Ward 3', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(184, 'Ward 4', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(185, 'Ward 5', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(186, 'Ward 6', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(187, 'Ward 7', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(188, 'Ward 8', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(189, 'Ward 9', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(190, 'Ward 1', 18, 5, 27, '2018-10-06 11:40:21', '2018-10-06 11:40:21'),
(191, 'Ward 1', 18, 5, 9, '2018-10-06 12:31:51', '2018-10-06 12:31:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bol_election_agent`
--
ALTER TABLE `bol_election_agent`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_designations`
--
ALTER TABLE `bol_election_designations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_districts`
--
ALTER TABLE `bol_election_districts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_election_area`
--
ALTER TABLE `bol_election_election_area`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_election_center`
--
ALTER TABLE `bol_election_election_center`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_organizations`
--
ALTER TABLE `bol_election_organizations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_people`
--
ALTER TABLE `bol_election_people`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_people_groups`
--
ALTER TABLE `bol_election_people_groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_union`
--
ALTER TABLE `bol_election_union`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_upazila`
--
ALTER TABLE `bol_election_upazila`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_village`
--
ALTER TABLE `bol_election_village`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_ward`
--
ALTER TABLE `bol_election_ward`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bol_election_agent`
--
ALTER TABLE `bol_election_agent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bol_election_designations`
--
ALTER TABLE `bol_election_designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `bol_election_districts`
--
ALTER TABLE `bol_election_districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `bol_election_election_area`
--
ALTER TABLE `bol_election_election_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bol_election_election_center`
--
ALTER TABLE `bol_election_election_center`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;
--
-- AUTO_INCREMENT for table `bol_election_organizations`
--
ALTER TABLE `bol_election_organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bol_election_people`
--
ALTER TABLE `bol_election_people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `bol_election_people_groups`
--
ALTER TABLE `bol_election_people_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `bol_election_union`
--
ALTER TABLE `bol_election_union`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `bol_election_upazila`
--
ALTER TABLE `bol_election_upazila`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bol_election_village`
--
ALTER TABLE `bol_election_village`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=598;
--
-- AUTO_INCREMENT for table `bol_election_ward`
--
ALTER TABLE `bol_election_ward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
