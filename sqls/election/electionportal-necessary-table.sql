-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 06, 2018 at 04:24 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `electionportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_agent`
--

CREATE TABLE `bol_election_agent` (
  `id` int(11) NOT NULL,
  `electioncenter_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_agent`
--

INSERT INTO `bol_election_agent` (`id`, `electioncenter_id`, `people_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2018-10-03 04:47:55', '2018-10-03 04:47:55');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_designations`
--

CREATE TABLE `bol_election_designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `bol_election_designations`
--

INSERT INTO `bol_election_designations` (`id`, `name`, `organization_id`, `sort_order`, `created_at`, `updated_at`) VALUES
(18, 'General Secretory', 3, 18, '2018-10-02 05:41:48', '2018-10-02 05:41:48'),
(19, 'Assistant Commissioner', 4, 19, '2018-10-04 04:53:34', '2018-10-04 04:53:34'),
(20, 'Upazilla health officer', 4, 20, '2018-10-04 04:53:45', '2018-10-04 04:53:45'),
(21, 'Upazilla Agriculture officer', 4, 21, '2018-10-04 04:53:55', '2018-10-04 04:53:55'),
(22, 'Officer Incharge', 4, 22, '2018-10-04 04:54:07', '2018-10-04 04:54:07'),
(23, 'Upazilla Engineer', 4, 23, '2018-10-04 04:54:28', '2018-10-04 04:54:28'),
(24, 'Upazilla Family Planning Officer', 4, 24, '2018-10-04 04:54:38', '2018-10-04 04:54:38'),
(25, 'Upazilla Project Officer', 4, 25, '2018-10-04 04:54:46', '2018-10-04 04:54:46'),
(26, 'Upazilla Engineer, Health', 4, 26, '2018-10-04 04:54:54', '2018-10-04 04:54:54'),
(27, 'Upazilla Chairman', 4, 27, '2018-10-04 04:55:01', '2018-10-04 04:55:01'),
(28, 'Upazilla Vice Chairman', 4, 28, '2018-10-04 04:55:11', '2018-10-04 04:55:11'),
(29, 'Upazilla Vice Chairman, Female', 4, 29, '2018-10-04 04:55:18', '2018-10-04 04:55:18'),
(30, 'Union Chariman', 4, 30, '2018-10-04 04:55:31', '2018-10-04 04:55:31'),
(31, 'Union Member', 4, 31, '2018-10-04 04:55:39', '2018-10-04 04:55:39'),
(32, 'Executive Member', 3, 32, '2018-10-04 06:58:09', '2018-10-04 06:58:09'),
(33, 'UNO- Nawabganj', 4, 33, '2018-10-06 05:50:00', '2018-10-06 06:02:45'),
(34, 'AC Land', 4, 34, '2018-10-06 06:04:53', '2018-10-06 06:04:53'),
(35, 'ASP', 4, 35, '2018-10-06 06:24:14', '2018-10-06 06:24:29'),
(36, 'Upozila Health and Family Planning Officer', 4, 36, '2018-10-06 06:28:07', '2018-10-06 06:28:07'),
(37, 'Agricultural Officer', 4, 37, '2018-10-06 06:32:53', '2018-10-06 06:32:53'),
(38, 'Agricultural Extension Officer', 4, 38, '2018-10-06 06:34:35', '2018-10-06 06:34:35'),
(39, 'Fisheries Officer', 4, 39, '2018-10-06 06:36:56', '2018-10-06 06:36:56'),
(40, 'Livestock Officer', 4, 40, '2018-10-06 06:38:22', '2018-10-06 06:38:22'),
(41, 'Veterinary Surgeon', 4, 41, '2018-10-06 06:40:03', '2018-10-06 06:40:03'),
(42, 'Engineer, LGED', 4, 42, '2018-10-06 06:41:51', '2018-10-06 06:41:51'),
(43, 'Asst. Programmer', 4, 43, '2018-10-06 06:44:30', '2018-10-06 06:44:30'),
(44, 'Asst. Engineer, LGED', 4, 44, '2018-10-06 06:45:40', '2018-10-06 06:45:40'),
(45, 'Education Officer', 4, 45, '2018-10-06 06:46:57', '2018-10-06 06:46:57'),
(46, 'Social Service Officer', 4, 46, '2018-10-06 06:48:31', '2018-10-06 06:48:31'),
(47, 'UNO- Dohar', 4, 47, '2018-10-06 06:50:49', '2018-10-06 06:50:49'),
(48, 'Accounts Officer', 4, 48, '2018-10-06 06:51:41', '2018-10-06 06:51:41'),
(49, 'AC Land, Dohar', 4, 49, '2018-10-06 06:54:37', '2018-10-06 06:54:37'),
(50, 'OC, Dohar', 5, 50, '2018-10-06 06:56:49', '2018-10-06 06:56:49'),
(51, 'Election Officer', 4, 51, '2018-10-06 06:57:08', '2018-10-06 06:57:08'),
(52, 'Food Controller', 4, 52, NULL, NULL),
(53, 'Statistics Officer', 4, 53, NULL, NULL),
(54, 'Asst. Engineer, Public Health', 4, 54, NULL, NULL),
(55, 'Deputy Asst. Engineer, Public Health', 4, 55, NULL, NULL),
(56, 'Family Planning Officer', 4, 56, NULL, NULL),
(57, 'Youth Development Officer', 4, 57, NULL, NULL),
(58, 'Women affairs officer', 4, 58, NULL, NULL),
(59, 'Cooperative Officer', 4, 59, NULL, NULL),
(60, 'Rural Development Officer', 4, 60, NULL, NULL),
(61, 'Ansar & VDP Officer', 4, 61, NULL, NULL),
(62, 'Instructor, Resource Center,', 4, 62, NULL, NULL),
(63, 'Forest Officer', 4, 63, NULL, NULL),
(64, 'Sub- Register', 4, 64, NULL, NULL),
(65, 'Poverty Alleviation Officer,', 4, 65, NULL, NULL),
(66, 'Settlement Officer', 4, 66, NULL, NULL),
(67, 'Sr. GM, Dhaka Palli Bidyut Samiti-2', 4, 67, NULL, NULL),
(68, 'AGM (MS), Dhaka Palli Bidyut Samiti-2', 4, 68, NULL, NULL),
(69, 'AGM (O&M)', 4, 69, NULL, NULL),
(70, 'AGM (E&C)', 4, 70, NULL, NULL),
(71, 'AGM (Finance)', 4, 71, NULL, NULL),
(72, 'Deputy Asst. Engineer (BADC)(Acting)', 4, 72, NULL, NULL),
(73, 'Divisional Engineer, TNT (Keraniganj)', 4, 73, NULL, NULL),
(74, 'Project Implementation Officer (BRDB)', 4, 74, NULL, NULL),
(75, 'Coordinator, One house one farm', 4, 76, NULL, NULL),
(76, 'Asst. Engineer (BADC)', 4, 79, NULL, NULL),
(77, 'OC, Nawabganj', 4, 80, NULL, NULL),
(78, 'OC- Inquiry, Nawabganj', 4, 81, NULL, NULL),
(79, 'Project Implementation Officer', 4, 82, NULL, NULL),
(80, 'Secondary Education Officer', 4, 83, NULL, NULL),
(81, 'Academic Supervisor Secondary Office', 4, 84, NULL, NULL),
(82, 'Election Officer', 4, 85, NULL, NULL),
(83, 'Upozila Engineer', 4, 83, '2018-10-06 07:20:20', '2018-10-06 07:20:20'),
(84, 'Upozila Family Planning Officer', 4, 84, '2018-10-06 07:21:39', '2018-10-06 07:21:39'),
(85, 'Deputy Asst. Enginner, Public health', 4, 85, '2018-10-06 07:23:41', '2018-10-06 07:23:41'),
(86, 'Coordinator, One house one farm, Dohar', 4, 86, '2018-10-06 07:28:56', '2018-10-06 07:28:56'),
(87, 'Instructor, Resource Center', 4, 87, '2018-10-06 07:33:39', '2018-10-06 07:33:39'),
(88, 'Poverty Alleviation Officer', 4, 88, '2018-10-06 07:35:34', '2018-10-06 07:35:34'),
(89, 'Poverty Alleviation Officer', 4, 89, '2018-10-06 07:37:15', '2018-10-06 07:37:15'),
(90, 'Project Officer', 4, 90, '2018-10-06 08:15:03', '2018-10-06 08:15:03'),
(91, 'Project Officer, BRDB', 4, 91, '2018-10-06 08:16:16', '2018-10-06 08:16:16'),
(92, 'Liaison Officer', 4, 92, '2018-10-06 08:27:05', '2018-10-06 08:27:05');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_districts`
--

CREATE TABLE `bol_election_districts` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_districts`
--

INSERT INTO `bol_election_districts` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Barguna', NULL, NULL),
(2, 'Barisal', NULL, NULL),
(3, 'Bhola', NULL, NULL),
(4, 'Jhalokati', NULL, NULL),
(5, 'Patuakhali', NULL, NULL),
(6, 'Pirojpur', NULL, NULL),
(7, 'Bandarban', NULL, NULL),
(8, 'Brahmanbaria', NULL, NULL),
(9, 'Chandpur', NULL, NULL),
(10, 'Chittagong', NULL, NULL),
(11, 'Comilla', NULL, NULL),
(12, 'Coxs Bazar', NULL, NULL),
(13, 'Feni', NULL, NULL),
(14, 'Khagrachari', NULL, NULL),
(15, 'Lakshmipur', NULL, NULL),
(16, 'Noakhali', NULL, NULL),
(17, 'Rangamati', NULL, NULL),
(18, 'Dhaka', NULL, NULL),
(19, 'Faridpur', NULL, NULL),
(20, 'Gazipur', NULL, NULL),
(21, 'Gopalganj', NULL, NULL),
(22, 'Jamalpur', NULL, NULL),
(23, 'Kishoreganj', NULL, NULL),
(24, 'Madaripur', NULL, NULL),
(25, 'Manikganj', NULL, NULL),
(26, 'Munshiganj', NULL, NULL),
(27, 'Mymensingh', NULL, NULL),
(28, 'Narayanganj', NULL, NULL),
(29, 'Narsingdi', NULL, NULL),
(30, 'Netrakona', NULL, NULL),
(31, 'Rajbari', NULL, NULL),
(32, 'Shariatpur', NULL, NULL),
(33, 'Sherpur', NULL, NULL),
(34, 'Tangail', NULL, NULL),
(35, 'Bagerhat', NULL, NULL),
(36, 'Chuadanga', NULL, NULL),
(37, 'Jessore', NULL, NULL),
(38, 'Jhenaidah', NULL, NULL),
(39, 'Khulna', NULL, NULL),
(40, 'Kushtia', NULL, NULL),
(41, 'Satkhira', NULL, NULL),
(42, 'Narail', NULL, NULL),
(43, 'Magura', NULL, NULL),
(44, 'Meherpur', NULL, NULL),
(45, 'Bogra', NULL, NULL),
(46, 'Dinajpur', NULL, NULL),
(47, 'Gaibandha', NULL, NULL),
(48, 'Joypurhat', NULL, NULL),
(49, 'Kurigram', NULL, NULL),
(50, 'Lalmonirhat ', NULL, NULL),
(51, 'Naogaon', NULL, NULL),
(52, 'Natore', NULL, NULL),
(53, 'Nawabganj', NULL, NULL),
(54, 'Nilphamari', NULL, NULL),
(55, 'Pabna', NULL, NULL),
(56, 'Panchagarh ', NULL, NULL),
(57, 'Rajshahi', NULL, NULL),
(58, 'Rangpur', NULL, NULL),
(59, 'Sirajganj', NULL, NULL),
(60, 'Thakurgaon', NULL, NULL),
(61, 'Habiganj', NULL, NULL),
(62, 'Maulvibazar', NULL, NULL),
(63, 'Sunamganj', NULL, NULL),
(64, 'Sylhet', NULL, NULL),
(77, 'test 1', '2018-10-02 06:24:13', '2018-10-02 06:24:13');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_election_area`
--

CREATE TABLE `bol_election_election_area` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazila_ids` text COLLATE utf8_unicode_ci,
  `union_ids` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_election_area`
--

INSERT INTO `bol_election_election_area` (`id`, `name`, `district_id`, `upazila_ids`, `union_ids`, `created_at`, `updated_at`) VALUES
(3, 'Dhaka 1', 18, '["6","5"]', '["6","5","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26"]', '2018-10-03 14:05:55', '2018-10-04 06:23:37');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_election_center`
--

CREATE TABLE `bol_election_election_center` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `local_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_room` int(11) DEFAULT NULL,
  `male_voter` int(11) DEFAULT NULL,
  `female_voter` int(11) DEFAULT NULL,
  `electionarea_id` int(11) NOT NULL,
  `union_ids` text COLLATE utf8_unicode_ci,
  `village_ids` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_election_center`
--

INSERT INTO `bol_election_election_center` (`id`, `name`, `local_name`, `total_room`, `male_voter`, `female_voter`, `electionarea_id`, `union_ids`, `village_ids`, `created_at`, `updated_at`) VALUES
(1, 'Orongabad govt. Primary school\n(Male and female Voter) \n\n', '1. North Arongabad: M-399: F- 364\n2. South Arongabad: M-329: F-321 \n3.Nosratpur:M-169: F-169\n4.Moddho Orongabad: M-477: F-420', 5, 1374, 1274, 3, '["5"]', '["1":"2":"3"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(2, 'Dhoyair Modinatul Ulum Kowmiya Madrasha: (Male and female Voter) \n', '1. Pankunda: M-663: F-646\n', 3, 663, 646, 3, '["5"]', '["4"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(3, 'Bahra Govt. Primary School (Bahra) (Male and Female voter)', '1. Hatni: M-44: F-65\n2. Char Hatni: M-24: F-23\n3. Bahra: M-1265: F-1256', 5, 1333, 1334, 3, '["5"]', '["5"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(4, 'Hatni Govt. Primary School (Hatni) (Male and Female voter)', '1.Asta: M-1192: F-1197', 3, 1192, 1197, 3, '["5"]', '["6"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(5, 'Dhoyair\n Govt. Primary School (Dhoyair) (Male and Female voter)', '1.West Dhoyair: M-769: F-722\n2. Middle Dhoyair: M-650: F-641\n', 6, 1419, 1363, 3, '["5"]', '["7":"8"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(6, 'East Dhoyair\nPrivate Primary School (East Dhoyair) (Male and Female voter)', '1. East Dhoyair: M-741: F-768\n', 3, 741, 768, 3, '["5"]', '["9"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(7, 'ShilakoTha\nMadrasha (Shilakotha): (Male voter): Center -1', '1. North shilakotha: M-830\n2. South Shilakotha:M-1376\n3. Deovogh: M-95 ', 4, 2301, 0, 3, '["6"]', '["10":"11":"12"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(8, 'ShilakoTha\nMadrasha (Shilakotha): (Female voter): Center -2', '1. North shilakotha: F-931\n2. South Shilakotha: F-1431\n3. Deovogh: F-87', 4, 0, 2449, 3, '["6"]', '["10":"11":"12"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(9, 'Shilakotha \nGovt. Primary School (Shilakotha) (Male and Female voter)', '1. Charpurulia: M-931: F- 174\n2. Sundharipara: M- 1005: F-1125\n', 5, 1175, 1299, 3, '["6"]', '["13":"14"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(10, 'Kartikpur High School (Kartikpur) (Male and Female voter)', '1. Kartikpur: M-928: F-984\n2. Kusumhati: M-32: F-34\n', 4, 960, 1018, 3, '["6"]', '["15":"16"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(11, 'Basta Etimkhana & Madrasha (Kartikpur) (Male and Female voter)\n', '1. Boro Rasta Part (Part of 5no Ward): M-697: F-805\n2. Boro Rasta Part (Part of 6no Ward): M- 409: F-483\n3. Choto Rasta: M-131: F-128\n4. Babur Dangi: M-171: F-170 \n', 6, 1408, 1586, 3, '["6"]', '["17":"19":"20"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(12, 'Kusumhati \nFamily Planning Center (Charkushai) (Male voter): Center -1', '1. Charkushai (part of ward no 7): M-1101\n2. Charkushai (part of 8 no ward): M-563\n3. Joymangal (Puspakhali): M-222', 4, 1886, 0, 3, '["6"]', '["21"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(13, 'Kusumhati \nFamily Planning Center (Charkushai) (Female voter): Center -2', '1. Charkushai (part of ward no 7): F-1149\n2. Charkushai (part of 8 no ward): F-625\n3. Joymangal (Puspakhali):F-222', 4, 0, 1996, 3, '["6"]', '["21"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(14, 'Awliabad govt. Primary School (Awliyabad): (male and Female voter)\n', '1. Awliyabad: M-369: F- 396\n2. Arita: M-252: F-292\n3. Imamnagar: M-188: F- 219\n4. Mahtabnagar: M-170: F-180', 4, 979, 1087, 3, '["6"]', '["26":"27":"28"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(15, 'Ikrashi Adarsha High School (Ikrashi) (Male voter): Center -1', '1. Ikrashi  north: M- 831\n2. Ikrashi south: M-1061', 3, 1892, 0, 3, '["7"]', '["29":"30"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(16, 'ikrashi Adarsha High School (Ikrashi) (Female voter): Center -2', '1. Ikrashi  north: F-903\n2. Ikrashi south: F-1149', 4, 0, 2052, 3, '["7"]', '["29":"30"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(17, 'Palamganj Govt. Primary School (Palamganj)\n(Male and Female voter)', '1. Palamganj: M-307: F-271\n2. Haturpara: M-138: F-492 \n', 4, 897, 934, 3, '["7"]', '["31":"32"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(18, 'Jamalchar Govt. Primary School (Jamalchar)\n(Male and Female voter)', '1. Roghudebpur: M-134: F-139\n2. Karimganj: M-74: F-108\n3. Jamalchar: M- 438: F-457 \n', 3, 646, 704, 3, '["7"]', '["33":"34":"35"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(19, 'Raipara \nGovt. Primary School (Raipara)\n(Male voter): Center-1', '1. Raipara \nnorth: M-1383\n2. Raipara south: M- 918', 4, 2301, 0, 3, '["7"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(20, 'Raipara \nGovt. Primary School (Raipara)\n(Female voter): center -2', '1. Raipara north: F-1391\n2. Raipara south: F-930', 4, 0, 2321, 3, '["7"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(21, 'Lotakhola Govt. Primary School (Lotakhola)\n(Male and Female voter)', '1. Nagorkanda: M-657: F-680\n2. Lotakhola Biler Par: M-810: F-804', 6, 1467, 1484, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(22, 'Lotakhola Gigh School (Lotakhola): Male and Female voter', '1. West Lota khola: M-667: F-703\n2. Moddho Lotakhola: M-661: F-667', 5, 1328, 1370, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(23, 'Lotakhola Ajhar Ali Memorial High School (Moddho Lotakhola) (Male Voter: Center -1\n', '1. East Lotakhola: M-783\n2. North Char Joypara: M -672\n3. South Char Joypara: M- 815', 4, 2170, 0, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(24, 'Lotakhola Ajhar Ali Memorial High School (Moddho Lotakhola) (Female Voter: Center -2\n', '1. East Lotakhola: F-739\n2. North Char Joypara: M -559\n3. South Char Joypara: M- 739', 4, 0, 2037, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(25, 'Joypara \nKhalpar govt. Primary School (North Joypara: Khalpar) Male and Female voter', '1. Kathalighata: M-327: F-377\n2. Khalpar (north part of the Khal): M-1118: F-1180\n3. North Joypara Khal(North part of the Khal): M-64: F-71', 6, 1509, 1628, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(26, 'Islampur \ngovt primary school (Islampur)\n(Male and Female voter) ', '1. Islampur:\nM-649: M-683\n2. Khalpar (South part of Khal): M-266: F-276', 4, 915, 959, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(27, 'Joypara Model govt. Primary School (Joypara Chowdhurypara) (Male and Female voter) ', '1. North Joypara khalpar (South part of khal): M- 590: F-584\n2. North Joypara gajikanda: M-245: F-271\n3. North Joypara Miyapara: M-369: F-385\n4. North Joypara Byangarchar: M- 308: F-319', 6, 1512, 1559, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(28, 'Joypara College (South Joypara Kharakanda) (Male and Female voter) ', '1. North Joypara: M-455: F-441\n2. North Joypara Chowdhurypara: M-661: F-706\n3.  North Joypara Kutibari: M-379: F-360', 6, 1495, 1507, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(29, 'Boit govt. \nPrimary School (Boita) (Male and Female voter)', '1. Botia: M-556: F-571\n2. Nurpur (west side of WAPDA road ): M-378: F- 350\n3. South Joypara Gangpar (West side of WAPDA Road): M- 462: F- 417 ', 6, 1396, 1338, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(30, 'Joypara \nPilot High School (Joypara)(Male voter): center -1', '1. South Joypara ghona: M-361\n2. charlotakhola: M- 1584\n3. South Joypara majhipara : M-667: \n4. South Joypara gangpar (East side of WAPDA road): M-601\n 5. Murpur (East side of WAPDA road): M -242', 7, 3455, 0, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(31, 'Joypara \nPilot High School (Joypara)(Female voter): center -2', '1. South Joypara ghona: F-327\n2. charlotakhola: F-1524\n3. South Joypara majhipara : F- 671: \n4. South Joypara gangpar (East side of WAPDA road): F-637\n 5. Murpur (East side of WAPDA road): F-227', 7, 0, 3436, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(32, 'Begum Ayesh Pilot Girls High School (Joypara) (male and Female voter)', '1. South Joypara Kharakanda: M-1068: F-1047\n2. South Joypara: M -454: F-471', 6, 1522, 1518, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(33, 'Yousufpur \ngovt Primary School (Yousufpur)\n(Male and Female voter)', '1. Loskorkan\nda: M-273: F-304\n2. Rasulpur: M-164: F-160\n3. North Yousufpur: M- 751: F- 787\n4. South Yousufpur: M- 358: F- 350', 6, 1546, 1601, 3, '["8"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(34, 'Katakhali\ngovt. Primary School (Katakhali): (Male and Female voter)', '1. Katakhali \nM-707: F-773\n2. Nikra (West part)\nM- 344: F-329) ', 4, 1051, 1102, 3, '["8"]', '["44"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(35, 'Ghata-2 \ngovt. primary school: (Banaghata): (Male and Female voter) ', '1. Banaghata\n(West part): M- 1092: F-1165\n2. Banaghata (East part)\nM-76: F-53\n3. Nikra (East part): M-165: F-163', 5, 1333, 1381, 3, '["8"]', '["46"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(36, 'Ghata -1 \ngovt. primary school (Doharghata) (Male voter): center-1', '1. Dohar ghata: M-1875', 3, 1875, 0, 3, '["8"]', '["47"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(37, 'Ghata -1 \ngovt. primary school (Doharghata) (Female voter): center-2', '1. Dohar ghata: F- 1955', 4, 0, 1955, 3, '["8"]', '["47"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(38, 'Dohar govt. primary school (Dohar) (Male and Female voter)', '1. Dohar: \nM- 920: F-1010', 4, 920, 1010, 3, '["8"]', '["48"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(39, 'Kalichar Mogoljan govt. primary school (Male and Female voter)', '1. Kajirchar: M-599: F-621\n2. Modhurchar (east part): M- 412: F- 435', 4, 1011, 1056, 3, '["8"]', '["51"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(40, 'Hajrat Bilal () Madrasha (Principal Sattar Saheb Madrasha: West Sutarpar): (Male and Female voter) \n', '1. West Sutarpara: M-1090: F-1213', 5, 1090, 1213, 3, '["8"]', '["52"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(41, 'Sutarpara \nabdul Hamed High School (Sutarpara): (Male and Female voter)', '1. Gajirtek: M- 742: F- 857', 3, 742, 857, 3, '["8"]', '["53"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(42, 'Sutarpara Late Shamsul Haque Forkaniya Madrasha (Sutarpara) (Male and Female Voter) ', '1. East Sutarpara (west part): M-525: F- 605\n2. East Sutarpara (East part): M- 403: F- 405', 4, 928, 1010, 3, '["8"]', '["52"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(43, 'Uttar Modhurchar govt. Primary School: (Modhurchar) - (Male voter): Center-1', 'Modhurchar (West part): M-2357', 4, 2357, 0, 3, '["8"]', '["49"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(44, 'Uttar Modhurchar govt. Primary School: (Modhurchar) - (Female voter): Center-2', 'Modhurchar (West part): F- 2235', 4, 0, 2235, 3, '["8"]', '["49"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(45, 'Maruapota \ngovt. Primary School (daiyarkum) (Male and Female voter)', '1. Daiyarkum: M-440: F-486\n2. daiya Gojariya: M- 600: F- 647\n3. Gharmora: M- 274: F- 271\n4. Munshikanda: M- 253: F- 249', 6, 1567, 1753, 3, '["8"]', '["54":"55":"57":"58"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(46, 'Jhanki govt. Primary School (Jhanki): (male and Female voter)\n', '1. Jhanki: M-1452: F- 1626', 6, 1452, 1626, 3, '["9"]', '["49"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(47, 'Shimuliya \ngovt primary school (Uttar shimuliya): (Male and Female voter)', '1. Uttar shimuliya: M-1539: F- 1724', 6, 1539, 1724, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(48, 'Malikanda \nHigh School: (malikanda): (Male voter): Center-1', '1. Malikanda: M-1885', 4, 1885, 0, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(49, 'Malikanda \nHigh School: (malikanda): (Female voter): Center-2', '1. Malikanda: F-1901', 4, 0, 1901, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(50, 'Meghula \ngovt. Primary School (Meghula): (Male and Female voter)', '1. Meghula: M- 1639: F-1512', 6, 1639, 1512, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(51, 'South Shimuliya govt Primary School -(South Shimuliya) (Male voter): Center-1', '1. South Shimuliya: M- 1799', 4, 1799, 0, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(52, 'South Shimuliya govt Primary School -(South Shimuliya) (Female voter): Center-2', '1. South Shimuliya: F-1998', 4, 0, 1998, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(53, 'Narisha Pashimchar govt. Primary School (Narisha Pashimchar) (Male voter): Center -1', '1. Narisha Paschimchar: M-2708', 5, 2708, 0, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(54, 'Narisha Pashimchar govt. Primary School (Narisha Pashimchar) (Female voter): Center -2\n', '1. Narisha Paschimchar: F-2736', 5, 0, 2736, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(55, 'Narisha \nGilrs High School(Narisha): (Male and Female voter)', '1. Narisha: M-973: F-1072', 4, 973, 1072, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(56, 'Narisha \nKhalpar Govt. Primary School (Narisha Khalpar) (male and Female voter)', '1. Narisha \nKhalpar: M-790: F- 843\n2. Narisha Choitabator- M- 559: F- 623', 0, 1349, 1466, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(57, 'Satvita govt.\nPrimary School (Satvita): (Male and Female Voter)', '1. Ruikha: M- 187: F- 214\n2. Satvita: M- 1031: F- 1085', 5, 1218, 1299, 3, '["9"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(58, 'Purbachar \ngovt. Primary School (Purbachar): (Male and Female voter)', '1. Purbachar: M- 313: F-302\n2. Mokshudpur (Part of 1 no ward): M-442: F-481', 3, 755, 783, 3, '["10"]', '["59"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(59, 'Mokshudpur \nSamsuddin Shikder High School (mokshudpur): (Male and Female Voter)', '1. Mokshudpur (part of 2 no Ward): M- 721: F- 750\n2. Mokshudpur (Part of 3 no ward): M -802: F- 916', 6, 1523, 1666, 3, '["10"]', '["59"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(60, 'Padma \nMohabiddyaloy (Mokshudpur): (male and Female voter) ', '1. Khariya \nNorth: M-291: F-304\n2. Dhalarpar: M-447: F-468\n3. Khariya South: M- 186: F-187\n4. Ultadangi (shantinagar): M- 124: F- 133\n5. Gorabon: M-172: F- 121', 4, 1220, 1213, 3, '["10"]', '["62":"63":"66":"68"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(61, 'Shinepukur\ngovt. Primary School (bethuya): (male and female voter)', '1. Khasertek: M- 139: F- 137\n2. Baniyabari: M- 273: F-267\n3. Bethuya: M- 973: F- 958', 6, 1385, 1362, 3, '["10"]', '["64":"65":"67"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(62, 'Modhurkhola govt. Primary School (Modhurkhola): (male and female voter) ', '1. Modhurkhola: M-559: F -529\n2. South Modhurkhola: M- 482: F- 493', 4, 1041, 1022, 3, '["10"]', '["69":"70"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(63, 'Moitpara govt. Primary School (Moitpara): (male and female voter) ', '1. Horiyar tek: M- 154: F- 167\n2. Moitpara: M- 939: F 973\n3. Ruita: M- 200: F-220\n', 5, 1293, 1360, 3, '["10"]', '["71"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(64, 'Moura govt. Primary School (Moura): (male and female voter) ', '1. Chatravog: M-42: F-63\n2. Moura south: M -1107: F - 1158', 4, 1149, 1221, 3, '["10"]', '["73":"74"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(65, 'Dhitpur Shahid Shakil govt. Primary School (Dhitpur): (male and female voter) ', '1. Moura North: M- 938: F- 914\n2. Dhitpur: M- 1009: F-999', 7, 1947, 1913, 3, '["10"]', '["74"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(66, 'Charhosenpura govt. Primary School (Charboita): (male and female voter) ', '1. Purulia: M-142: F-126\n2. Moinot: M- 04: F-05\n3. Shukhdebpur: M- 41: F - 32\n4. Deovog: M - 02: F -01\n5. Hosenpur: M- 26: F - 27\n6. Char Kushumhati: M- 553: F - 519\n7. Charboita: M- 383: F - 417', 4, 1151, 1127, 3, '["11"]', '["78":"82":"85":"87"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(67, 'Narayonpur\nBaytul Aman- Darul Ulum Madrasha (Narayonpur)\n(male and Female voter)', '1. Shrikrishnapur: M- 425: F-381\n2. Narayanpur : M-348: F-291', 3, 773, 672, 3, '["11"]', '["89":"90"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(68, 'Chardeovog\na govt. Primary School (Modhurkhola): (male and female voter) ', '1. Charkushaichar (part of 4 no ward): M- 239: F- 220\n2. Charkushaichar (part of 5 no ward): M- 862: F- 886', 4, 1101, 1106, 3, '["11"]', '["80"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(69, 'Mahmudpur  Community Center (Mahmudpur): (male and female voter) ', '1. Charkushaichar (part of 7 no ward): M-287: F - 729)\n2. Mahmudpur(north side of Katakhal): M-827: F- 729', 4, 1114, 1022, 3, '["11"]', '["80"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(70, 'Harichandi\na govt. Primary School (Harichandi): (male and female voter) ', '1. Harichandi: M- 565: F- 568\n2. Char Bilashpur: M 57: F- 40\n3. Mahmudpur south side of Katakhal: M-635: F - 617\n', 4, 1275, 1225, 3, '["11"]', '["79":"81"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(71, 'Bilashpur \nChoket Ali Munshi Forkaniya Madrasha (Bilashpur)(male and female voter) ', '1. Bilashpur: M- 435: F- 463\n2. Choto Ramnathpur: M- 274: F- 234\n3. Hajharbigha: M- 339: F-296\n4. Alinagar: M- 95: F- 64\n5. Char Roghudebpur: M- 14: F- 18\n', 4, 1157, 1075, 3, '["12"]', '["91":"92":"93":"95":"96"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(72, 'Majhirchar \nBaytul-Ulum Madrasha (Majhirchar): (male and female voter) ', '1. Boro ramnathpur: M- 366: F - 351\n2. Majhirchar: M- 308: F- 280\n3. Debinagar North: M- 316: F- 381 ', 4, 990, 1012, 3, '["12"]', '["97":"98":"102"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(73, 'Majhirchar\ngovt. Primary School (Purbachar):(male and female voter) ', '1. Purbachar: M-298: F- 272\n2. Radhanagar North: M- 256: F- 351\n', 3, 654, 623, 3, '["12"]', '["99":"100"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(74, 'Bilashpur govt. Primary School (Kutubpur): (male and female voter) ', '1. Alamkha\nchar: M- 95: F- 123\n2. Kutubpur: M- 479: F- 467\n3. Debinagar South: M- 872: F- 794', 5, 1446, 1384, 3, '["12"]', '["103":"104"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(75, 'Krishnadebpur Saber Chokdar govt. Primary School (Kilchuri): (male and female voter) \n', '1. Radhanagar\nsouth: M- 505: F- 513\n2. Kulchuri: M -496: F- 431\n3. Krishnadebpur: M- 148: F- 138', 4, 1149, 1082, 3, '["12"]', '["101":"105":"106"]', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(76, 'Monikanda govt. Primary School: Shikaripara: Nawabganj: Dhaka\n(male and female voter) ', 'Monikanda:\nM-581: F- 603\nAnondanagar: M- 253: F- 226\nBishompur: M- 367: F- 352', 5, 1201, 1181, 3, '["13"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(77, 'Shibpur\ngovt. Primary School \nShikaripara: Nawabganj: Dhaka\n(male and female voter) ', 'Shibpur: M- 187: F- 240\nSonatola: M- 257: F- 258\nMoheshpur: M- 172: F- 181\nVodrokanda: M- 75: F -82\nShekher Nagar- M- 60: F- 65\nNurpur- M- 139: F- 141\nNotti: M- 132: F- 149', 5, 1022, 1116, 3, '["13"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(78, 'Shikaripara\nHigh School \nShikaripara: Nawabganj: Dhaka\n(male and female voter) \n', 'Shiakaripara: M- 838: F- 872\nKondrabpur: M- 149: F- 161\nCharbaghuri: M- 61: F- 59\nLashkarkanda: M- 190: F-191', 5, 1238, 1283, 3, '["13"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(79, 'Hagradi govt. Primary School \nShikaripara: Nawabganj: Dhaka\n(male and female voter) ', 'Narsinghapur: M- 85: F- 85\nhagradi: M- 578: F- 612\nNoyadangi: M- 84: F- 94\nGaribpur:M-339: F- 420\nBoktarnagar: M- 584: F- 626', 7, 1670, 1837, 3, '["13"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(80, 'Boktarnagar\ngovt. Primary School (Panjiprohori)\nShikaripara: Nawabganj: Dhaka\n(male and female voter) ', 'Gongadiya:\nM- 230: F- 216\nPanjiprohori: M- 460: F- 456\nDaudpur: M- 409: F- 371', 4, 1099, 1043, 3, '["13"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(81, 'Sujapur govt. Primary School \nShikaripara: Nawabganj: Dhaka\n(male and female voter) \n', 'Narayanpur: M- 287: M- 301\nSherpur: M- 251: F- 286\nSujapur: M- 392: 458', 4, 930, 1045, 3, '["13"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(82, 'Joykrishnanagar govt. Primary School \nJoykrishnanagar: Nawabganj: Dhaka\n(male and female voter) ', 'Shankardiya: M- 293: F- 296\nShyampur: M- 66: F- 60\nBilchuri: M- 19: F- 17\nJoykrishnapur: M- 174: F- 197\nGojariay: M- 74: F- 68', 3, 626, 628, 3, '["14"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(83, 'Shonabaju govt. Primary School \nJoykrishnanagar: Nawabganj: Dhaka\n(male and female voter) ', 'Shonabajhu: M- 1144: F- 1151', 4, 1144, 1151, 3, '["14"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(84, 'Titopoldiya govt. Primary School \nJoykrishnanagar: Nawabganj: Dhaka\n(male and female voter) ', 'Titopoldiya: M- 277: F- 244\nKollyanshri: M- 286: F- 308\nBahadurpur: M- 202: F- 229\nPanikawr: M- 214: F- 197', 4, 979, 978, 3, '["14"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(85, 'Kuthuri govt. Primary School \nJoykrishnanagar: Nawabganj: Dhaka\n(male and female voter) ', 'Kuthuri: M \n-587: F- 601\nAshoypur: M- 280: F- 271\nGhatbazar: M- 29: F- 30\nRaypur: M- 293: F- 323\nBrahmangram: M- 61: F- 61', 5, 1250, 1286, 3, '["14"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(86, 'Ghoshail govt. Primary School \nJoykrishnanagar: Nawabganj: Dhaka\n(male and female voter) ', 'Ghoshail: M- 458: F -481\nAr Ghoshail: M- 262: F- 251\nKedharpur: M- 176: F- 182\nbahuyaghati: M- 152: F- 137\n', 4, 1048, 1051, 3, '["14"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(87, 'Rajhapur govt. Primary School \nJoykrishnanagar: Nawabganj: Dhaka\n(male and female voter) ', 'Rajhapur: \nM- 600: M 603\nBalenga: M- 535: F- 497\nKastartek: M- 144: F- 144\nDhoyair: M- 38: F- 44\nRajhapur Noyadangi: M- 36: F- 38\nBatoijuri: M- 18: F- 29', 5, 1371, 1355, 3, '["14"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(88, 'Charakhali govt. Primary School \nJoykrishnanagar: Nawabganj: Dhaka\n(male and female voter) ', 'Purbachak-\nM- 42: F- 41\nCharakhali-M- 118: F -125\nMothbari: M- 75: F- 59\nBataimuri: M- 11: F- 9', 2, 246, 234, 3, '["14"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(89, 'baruakhali govt. Primary School \nBaruakhali:  Nawabganj: Dhaka\n(male and female voter) ', 'Baruakhali North: M- 201: F- 194\nMadla: M- 216: F- 203\nChakbarilla: M- 121: F- 129\nbaruakhali South- M- 529: F- 546', 4, 1067, 1072, 3, '["15"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(90, 'Baherchar govt. Primary School \nBaruakhali:  Nawabganj: Dhaka\n(male and female voter) ', 'Baherchar: M- 342: F- 400\nDaktarkanda: M- 42: F- 36\nBramhankhali: M- 410: F- 445\nChoto Nabagram: M- 382: F- 384 \n', 4, 1176, 1265, 3, '["15"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(91, 'Kumar barilla govt. Primary School \nBaruakhali:  Nawabganj: Dhaka\n(male and female voter) ', 'Dharikandi: \nM- 101: F- 108\nVeramuriya: M- 252: F- 253\nKumarbarilla : M- 377: F- 387\nkandabarilla:M - 497: F- 497\nBorobarilla: M- 118: F-109', 5, 1345, 1354, 3, '["15"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(92, 'Alalpur govt. Primary School \nBaruakhali:  Nawabganj: Dhaka\n(male and female voter) ', 'Ramnagar: M- 73: F- 75\nBoro Priti Noyadda: M- 68: F- 76\nChoto Priti Noyadda- M- 93: F- 45\nAlalpur South: M- 327: F- 357\nMunshinagar: M- 344: F- 409\nAlalpur North: M- 273: F- 275', 5, 1213, 1341, 3, '["15"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(93, 'Kar Para govt. Primary School \nBaruakhali:  Nawabganj: Dhaka\n(male and female voter) ', 'Kar Para: M- 227: F- 242\nChatrapur: M- 165: F- 170\nJoinotpur: M- 108: F- 106\nDhirrghogram: M- 325: F- 356\nShiyaljaan: M- 29: F- 41\nVangagora: M- 134: F- 124', 4, 988, 1039, 3, '["15"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(94, 'Kawniyakandi govt. Primary School \nBaruakhali:  Nawabganj: Dhaka\n(male and female voter) ', 'Boro \nkawniyakandi: M- 361: F- 362\nChoto Kawniyakandi: M- 149: F- 161\nJahanabaad: M- 105: F- 106\nBaniyagram: M- 68: F- 93\nKanchannagar: M- 78: F- 77', 3, 761, 799, 3, '["15"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(95, 'Ghoshpara \ngovt. Primary School \nNoyonshree:  Nawabganj: Dhaka\n(male and female voter) ', 'Ghoshpara:M- 390: F- 402\nVaowadubi: M- 395: F- 440\nVurakhali: M- 158: F- 127', 4, 943, 969, 3, '["16"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(96, 'Nabinagar \nUttar Bahra Dhakhil Madrasha: Noyonshree:  Nawabganj: Dhaka\n(male and female voter) ', 'Sahebganj: M- 231: F- 231\nNorth Bahra: M- 547: F- 616', 3, 778, 847, 3, '["16"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(97, 'Chartashulla govt. Primary School \nNoyonshree:  Nawabganj: Dhaka\n(male and female voter) ', 'Choto Tashulla: \nM- 774: F- 819\nBipulla: M- 680: F- 766', 6, 1454, 1585, 3, '["16"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(98, 'Tashulla High School Noyonshree:  Nawabganj: Dhaka\n(male and female voter) ', 'Boro Tashulla: M- 438: F- 459\nChar Tuitail: M- 204: F- 218\nBipratashulla: M- 373: F- 396', 4, 1015, 1073, 3, '["16"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(99, 'Tuitail govt. Primary School \nNoyonshree:  Nawabganj: Dhaka\n(male and female voter) ', 'Tuitail: M-F\n1671\nBokchar- F-M- 721\nCharbaghuri- M-F- 96\nNotun Tuitail: M-F- 97\nAfzalnagar: M-F- 290', 6, 1341, 1534, 3, '["16"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(100, 'Khanepur High School: Noyonshree:  Nawabganj: Dhaka\n(male and female voter) ', 'Shoilla: M- 339: F- 369\nKandakhanepur: M- 569: F- 590\nKhanepur Kanda: M- 169: F- 173\nChar Shoilla: M- 226: F- 251', 5, 1313, 1383, 3, '["16"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(101, 'Radhakantpur govt. Primary School \nNoyonshree:  Nawabganj: Dhaka\n(male and female voter) ', 'Radhakantopur\nM- 538: F- 601\nRahutghati: M- 231: F- 327\nChar Khanepur: M- 197: F- 219\nNoyonshree: M- 583: F- 630', 6, 1549, 1777, 3, '["16"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(102, 'Golla Bolik govt. Primary School \nNoyonshree:  Nawabganj: Dhaka\n(male and female voter) ', 'Sap Leja: M-\n126: F- 118\nChoto Golla: M- 189: F- 258\nBoro Golla: M- 186: F- 253\nKashinagar: M- 39: F- 43\nPadrikanda: M- 58: F- 79\nMataber Tek: M- 14: F- 14\nKumar Golla: M- 262: F- 311\nKrishnanagar: M-167: F- 184', 4, 1041, 1260, 3, '["16"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(103, 'Deotala govt. Primary School \nNoyonshree:  Nawabganj: Dhaka\n(male and female voter) ', 'Deotola: M- 503: F- 570\nDhori Deotola : M- 97: F- 123\nShahjadpur: M- 85: F-92\nPurba Shoytankathi: M- 381: F- 430', 4, 1066, 1215, 3, '["16"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(104, 'Uttar Balukhanda Govt. Primary School:\nSholla: Nawabganj : Dhaka (Male & Female Voter) ', 'North Balukhanda - M:1070\nF:1168', 4, 1070, 1168, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(105, 'Dutta Khanda Govt. Primary School:\nSholla: Nawabganj: Dhaka (Male & Female Voter)', 'Dutta Khanda - M:876 F:894', 3, 876, 894, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(106, 'Mohijdia Govt. Primary School: \nSholla: Nawabganj: Dhaka (Male & Female Voter)', 'Mohijdia - M: 390 F:405\nCumulli - M: 320 F: 311', 3, 710, 716, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(107, 'Dokkhin Balukhanda Govt. Primary School: \nSholla: Nawabganj : Dhaka (Male & Female Voter) \n', 'Dudhghata - M: 396 F: 386\nDokkhin Balukhanda - \nM: 1030 F: 990', 5, 1426, 1376, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(108, 'Purbo Patiljhap Govt. Primary School:\nSholla: Nawabganj : Dhaka (Male & Female Voter) ', 'Patiljhap - M:1049 F:1111\nSholla Nagar - M: 327 F: 342', 5, 1376, 1453, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(109, 'Sholla Higher Secondary  School:\nSholla: Nawabganj : Dhaka (Male & Female Voter) ', 'Khatia - M: 1241: F: 1243', 5, 1241, 1243, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(110, 'Dokkhin Sholla Govt. Primary School:\nSholla: Nawabganj : Dhaka (Male & Female Voter) ', 'Dokkhin Sholla: M: 1197 \nF: 1166\nAjgora - M: 167 F: 162\nJhonjhonia - M:112 F:112', 5, 1476, 1440, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(111, 'Shinghora Govt. Primary School:\nSholla: Nawabganj : Dhaka (Male & Female Voter) ', 'Shinghora - M: 656 F:649\nParashora - M:145 F: 140\nNoyahati - M:281 F:260\nAbdani - M:180 F:199\nDokkhin Purbo Shinghora \nM: 195 F:202\nShinghora Aikbari - M: 265\nF:336', 6, 1722, 1786, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(112, 'Chokoria Govt. Primary School: \nSholla: Nawabganj : Dhaka (Male & Female Voter) ', 'Chokoria - M: 555 F: 560\nChokoria Chokbari - M:485 \nF: 449', 4, 1040, 1009, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(113, 'Shingjor Govt. Primary School:\nSholla: Nawabganj : Dhaka (Male & Female Voter) ', 'Shingjor  - M:1358 F: 1392', 5, 1358, 1392, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(114, 'Sultanpur Govt. Primary School:\nSholla: Nawabganj : Dhaka (Male & Female Voter) ', 'Sultanpur - M: 194 F: 216\nModonmohonpur - M: 667 \nF: 708\nKonda - M: 348 F: 340 ', 5, 1209, 1264, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(115, 'M Mohiuddin Secondary School:\nSholla: Nawabganj : Dhaka (Male & Female Voter) ', 'Auna - M: 356 F:366\nHayatkanda - M: 372 F: 389\nUlail - M: 421 F: 438 \nKartikpur - M: 63 F: 50\nChok Auna - M: 272 F: 246\nBowali - M:261: F: 232', 5, 1745, 1721, 3, '["17"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(116, 'Jontraile Govt. Primary School: Jontraile: Nowabgonj: Dhaka ( Male & Female Voter)', 'Jontraile (Part of Ward No. 1)\n-M: 987 F: 1056\n', 4, 987, 1056, 3, '["18"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(117, 'Ajijpur Govt. Primary School: Jontraile: Nowabgonj: Dhaka ( Male & Female Voter)', 'Jontraile (Part of Ward No. 2)\n-M: 530 F: 570\nAjijpur- M: 378 F: 372\n', 4, 908, 942, 3, '["18"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(118, 'Chndrokhola Govt. Primary School: Jontraile: Nowabgonj: Dhaka ( Male & Female Voter)', 'Chondrokhola -M: 868 F: 820\nVaoyaliya- M: 347 F: 377\n', 4, 1215, 1197, 3, '["18"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(119, 'Begum Hasiba High School: Jontraile: Nowabgonj: Dhaka ( Male & Female Voter)', 'Debukhali- M: 128 F: 108\nHorishkul (Part of Ward No. 5)\n-M: 976 F: 998', 4, 1104, 1106, 3, '["18"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(120, 'Dokhhin Horishkul Govt. Primary School: Jontraile: Nowabgonj: Dhaka ( Male & Female Voter)', 'Horishkul (Part of Ward No. 6)\n-M: 1168 F: 1196\nJalalchor- M: 592 F: 620', 6, 1760, 1816, 3, '["18"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(121, 'Gobindopur Govt. Primary School: Jontraile: Nowabgonj: Dhaka ( Male & Female Voter)', 'East Gobindopur\n-M: 1352 F: 1410\nMoymondi- M: 459 F: 466', 7, 1811, 1876, 3, '["18"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(122, 'Nobogram Govt. Primary School: Jontraile: Nowabgonj: Dhaka ( Male & Female Voter)', 'Balidior \n-M: 143 F: 199\nChorkhlosi- M: 617 F: 609\nNobogram- M: 251 F: 246', 4, 1011, 1054, 3, '["18"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(123, 'Kirinchi Govt. Primary School: Jontraile: Nowabgonj: Dhaka ( Male & Female Voter)', 'Nolgora- M: 271 F: 272\nKirinchi- M: 746 F: 699\n', 4, 1017, 971, 3, '["18"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(124, 'Mohobbotpur Govt. Primary School: Bandura: Nowabgonj: Dhaka ( Male & Female Voter)', 'Mohobbotpur- M: 1213 F: 1327\nImamnogor- M: 189 F: 200\n', 6, 1402, 1527, 3, '["19"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(125, 'Sadapur Govt. Primary School: Bandura: Nowabgonj: Dhaka ( Male & Female Voter)', 'Sadapur- M: 936 F: 979\nHazratpur- M: 343 F: 326\nAlhadipur- M: 420 F: 481\n', 6, 1699, 1786, 3, '["19"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(126, 'Majhirkanda Govt. Primary School: Bandura: Nowabgonj: Dhaka ( Male & Female Voter)', 'Sayedpur- M: 500 F: 556\nBoro Harikanda- M: 257 F: 283\nMajhirkanda- M: 419 F: 403\nMridhakanda- M: 131 F: 159\nKathalighata- M: 443 F: 497\nShorupdi Kandi- M: 44 F: 78', 6, 1794, 1976, 3, '["19"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(127, 'Puran Bandura Govt. Primary School: Bandura: Nowabgonj: Dhaka ( Male & Female Voter)', 'Old Bandura- M: 1550 F: 1715\n', 6, 1550, 1715, 3, '["19"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(128, 'Bandura Govt. Boys\' Primary School: Bandura: Nowabgonj: Dhaka ( Male & Female Voter)', 'Molashikanda- M: 316 F: 335\nHasanabad (Part of Ward No. 5)- M: 609 F: 747\n', 4, 925, 1082, 3, '["19"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(129, 'Molouvi Dangi Govt. Primary School: Bandura: Nowabgonj: Dhaka ( Male & Female Voter)', 'Molouvi Dangi- M: 443 F: 463\nHasanabad (Part of Ward No. 6)- M: 882 F: 963\n', 5, 1325, 1426, 3, '["19"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(130, 'Puyakoiyer Govt. Primary School: (Tinshed Building) Centre- 1 Woman Centre: Bandura: Nowabgonj: Dhaka ', 'New Bandura (Part of ward No. 8)- F: 582\nMirer Dangi- F: 106\nNurongor- F: 373\nNoyanogor- F: 801\nBaroduari- F: 201\nChampanogor- F: 83', 4, 0, 2146, 3, '["19"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(131, 'Puyakoiyer Govt. Primary School: (Tinshed Building & Half- stoned Building) Centre- 2 Male Centre: Bandura: Nowabgonj: Dhaka ', 'New Bandura (Part of ward No. 8)- M: 508\nMirer Dangi- M: 63\nNurongor- M: 384\nNoyanogor- M: 721\nBaroduari- M: 220\nChampanogor- M: 87', 4, 1983, 0, 3, '["19"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(132, 'Notun Bandura Govt. Primary School: Bandura: Nowabgonj: Dhaka ( Male & Female Voter)', 'New Bandura (Part of ward No. 7)- M: 1327 F: 1268', 5, 1327, 1268, 3, '["19"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(133, 'Harekrishna Kusum Koli High School: Kolakopa: Nowabgonj: Dhaka ( Male & Female Voter)', 'Modhonogor- M: 115 F: 128\nGoyalnogor- M: 148 F: 105\nGopalpur- M: 218 F: 238\nBagahati- M: 75 F: 91\nBorongor- M: 167 F: 161\nVoirahati- M: 126 F: 118\nRajarampur- M: 148 F: 177\nGopikantopur- M: 76 F: 85\nKhandokar Noyadha- M: 69 F: 78\nPukurpar- M: 125 F: 128\nR', 5, 1415, 1437, 3, '["20"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(134, 'Nowabgonbj Pilot High School: Kolakopa: Nowabgonj: Dhaka ( Male & Female Voter)', 'Panaliya- M: 530 F: 534\nWest Somsabad- M: 625 F: 662\nBoro Somsabad- M: 651 F: 716\nNowabgonj- M: 238 F: 212', 8, 2044, 2124, 3, '["20"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(135, 'Nowabgonbj Pilot Girls\' High School: Kolakopa: Nowabgonj: Dhaka ( Male & Female Voter)', 'Kashimpur- M: 1445 F:1590', 6, 1445, 1590, 3, '["20"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(136, 'Jalalpur Udayan Model Govt. Primary School: Kolakopa: Nowabgonj: Dhaka ( Male & Female Voter)', 'Baghmara- M: 295 F: 299\nSurgonj- M: 290 F: 297\nJalalpur- M: 463 F: 446\nBoikusthopur- M: 108 F: 127\nAmirpur- M: 499 F: 540', 6, 1655, 1709, 3, '["20"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(137, 'Rajpara Govt. Primary School: Kolakopa: Nowabgonj: Dhaka ( Male & Female Voter)', 'Khondokerhati- M: 456 F: 463\nMadhobpur- M: 446 F: 483\nRajpara- M: 550 F: 597\nPirmamudiya- M: 346 F: 407\nBibirchor- M: 130 F: 160', 7, 1928, 2110, 3, '["20"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(138, 'Pathankanda Govt. Primary School: Boxnogor: Nowabgonj: Dhaka ( Male & Female Voter)', 'Pathankanda (Part of ward no. 1)- M: 288 F: 308\nKomorgonj- M: 700 F: 651\nGajikhali- M: 135 F: 124', 4, 1123, 1083, 3, '["21"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(139, 'Bordhonpara Govt. Primary School: Boxnogor: Nowabgonj: Dhaka ( Male & Female Voter)', 'Pathankanda (Part of ward no. 2)- M: 107 F: 99\nBordhonpara (Part of ward no. 2)- M: 487 F: 527\nBordhonpara (Part of ward no. 3)- M: 714 F: 772', 5, 1308, 1398, 3, '["21"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(140, 'Boxnogor High School: Boxnogor: Nowabgonj: Dhaka ( Male & Female Voter)', 'Choto Boxnogor- M: 936 F: 996', 4, 936, 996, 3, '["21"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(141, 'Boro Boxnogor Govt. Primary School: Boxnogor: Nowabgonj: Dhaka ( Male & Female Voter)', 'Choto Rajpara-  M: 486 F: 467\nBoro Boxnogor Sabekhati-  M:295 F: 287\nBoro Boxnogor-  M: 513 F: 560\nBoxnogor Chowrahoti-  M: 281 F: 349\nBoxnogor Christianhati-  M: 221 F: 264', 7, 1796, 1927, 3, '["21"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(142, 'Dhighirpar Govt. Primary School: Boxnogor: Nowabgonj: Dhaka ( Male & Female Voter)', 'Dhighir par (Part of ward no. 7)- M: 949 F: 1023\nDhighir para (Part of ward no. 8)- M: 958 F: 997', 7, 1907, 2020, 3, '["21"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(143, 'Balurchor Govt. Primary School: Boxnogor: Nowabgonj: Dhaka ( Male & Female Voter)', 'Balurchor- M: 710 F: 772\nSurgonj Tukinikanda- M: 531 F: 524', 5, 1241, 1296, 3, '["21"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(144, 'Bolomonttochor Govt. Primary School: Baharra: Nowabgonj: Dhaka ( Male & Female Voter)', 'Boro Bolomonttochor- M: 1224 F: 1314\nChoto Bolomonttochor- M: 361 F: 400', 6, 1585, 1714, 3, '["22"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(145, 'Alamgirchor Govt. Primary School: Baharra: Nowabgonj: Dhaka ( Male & Female Voter)', 'Alamgirchor- M: 751 F: 769\nUrarchor- M: 73 F: 74', 3, 824, 843, 3, '["22"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(146, 'Kandamatra Govt. Primary School: Baharra: Nowabgonj: Dhaka ( Male & Female Voter)', 'Kandamatra- M: 907 F: 856\nKahur- M: 223 F: 219\nBagbari- M: 57 F: 55', 4, 1187, 1130, 3, '["22"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(147, 'Baharra Poschim Govt. Primary School: Baharra: Nowabgonj: Dhaka ( Male & Female Voter)', 'Shuvoriya- M: 418 F: 407\nBoro Baharra Chakuri Para- M: 1105 F: 1021', 5, 1523, 1428, 3, '["22"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(148, 'Baharra Oyasek Memorial High School: Baharra: Nowabgonj: Dhaka ( Male & Female Voter)', 'Baharra East Tati Para (Baharra East Rishi Para)- M: 553 F: 565\nBoro Baharra West Para- M: 613 F: 611\nWest Chok Baharra- M: 177 F: 226', 5, 1343, 1402, 3, '["22"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(149, 'Baharra Purbo Govt. Primary School: Baharra: Nowabgonj: Dhaka ( Male & Female Voter)', 'Baharra Mollakanda- M: 501 F: 560\nMylaile- M: 430 F: 466\nBaharra Chorkanda- M: 881 F: 891', 7, 1812, 1917, 3, '["22"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(150, 'Agla Chowkighata Jonomongol High School: Baharra: Nowabgonj: Dhaka ( Male & Female Voter)', 'North Chowkighata- M: 790 F: 834\nSouth Chowkighata- M: 616 F:635', 5, 1406, 1469, 3, '["22"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(151, 'Baharra Ebetedayi Madrasha: Baharra: Nowabgonj: Dhaka ( Male & Female Voter)', 'Boro Baharra East & North- M: 322 F: 296\nEast Chok Baharra- M: 163 F: 170\nBaharra Sonargaon- M: 188 F: 204\nUluman Chandura- M: 120 F: 120\nNaopara- M: 241 F: 247', 4, 1034, 1037, 3, '["22"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(152, 'Koilail Govt. Primary School:\nKoilail: Nawabganj: Dhaka (Male & Female Voter) ', 'Koilail South- M: 252: F: 264\nKoilail Bagmara - M: 458 F: 460\nTelenga - M: 419: F: 402', 4, 1129, 1126, 3, '["23"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(153, 'Koilail Hanafia Madrasha: Koilail: Nawabganj: Dhaka (Male & Female Voter) ', 'Koilail North - M:686 F:638\nSonargaon - M: 209 F: 215\nMeleng Dokkhin - M: 198 \nF: 210\nRaipur - M: 344 F: 339', 5, 1437, 1402, 3, '["23"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(154, 'Meleng Govt. Primary School: Koilail: Nawabganj: Dhaka (Male & Female Voter)\n', 'Meleng North - M: 416 F:420\nMeleng East - M: 438: F: 420\nMeleng West (Khal Paar) - \nM: 218 F: 232', 4, 1072, 1072, 3, '["23"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(155, 'Matabpur Govt. Primary School:  Koilail: Nawabganj: Dhaka (Male & Female Voter)', 'Matabpur - M: 567 F: 634', 2, 567, 634, 3, '["23"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(156, 'Paragram Govt. Primary School: Koilail: Nawabganj: Dhaka (Male & Female Voter)', 'Dignara - M: 295 F: 241\nParagram - M: 524 F: 531\nMashail - M: 262 F: 226 ', 4, 1081, 998, 3, '["23"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(157, 'Dorikanda Govt. Primary School: Koilail: Nawabganj: Dhaka (Male & Female Voter)', 'Dorikanda - M: 1100 F: 1100\nMadhupur - M: 346 F: 323', 5, 1446, 1423, 3, '["23"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(158, 'Bhangavita Govt. Primary School: Koilail: Nawabganj: Dhaka (Male & Female Voter)', 'Molla Kanda - M: 222 F: 243\nBhangavita - M: 971 F: 840', 4, 1193, 1083, 3, '["23"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(159, 'Malikanda Govt. Primary School: Koilail: Nawabganj: Dhaka (Male & Female Voter)', 'Malikanda - M: 651 F:684\nNoyakanda - M: 803 F: 778', 5, 1454, 1462, 3, '["23"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(160, 'Doulotpur Govt. Primary School: Koilail: Nawabganj: Dhaka (Male & Female Voter)', 'Doulotpur - M: 1184 F: 1167\n', 4, 1184, 1167, 3, '["23"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(161, 'Majhpara Govt. Primary School: Agla: Nawabganj: Dhaka (Male & Female Voter)', 'Mukimpur- M: 132 F: 124\nHajikanda- M: 155 F: 160\nAgla- M: 337 F: 375\nAgla East para- M: 311 F: 319\nMajhpara- M: 880 F: 948', 7, 1815, 1926, 3, '["24"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(162, 'Mohakobi Kayokobad Girls\' High School (Front Building): Centre-1: Agla: Nawabganj: Dhaka (Female Centre)', 'Kaluyahati North- F: 1195\nKaluyahati South- F: 352\nTikorpur- F: 778', 4, 0, 2325, 3, '["24"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(163, 'Mohakobi Kayokobad Girls\' High School (Back Building): Centre-2: Agla: Nawabganj: Dhaka (Female Centre)', 'Kaluyahati North- M: 1186\nKaluyahati South- M: 393\nTikorpur- M: 840', 4, 2419, 0, 3, '["24"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(164, 'Chatiya Govt. Primary School: Agla: Nawabganj: Dhaka (Male & Female Voter)', 'Mohonpur- M: 913 F: 1016\nChatiya- M: 657 F: 697', 6, 1570, 1713, 3, '["24"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(165, 'Chandertek Govt. Primary School: Agla: Nawabganj: Dhaka (Male & Female Voter)', 'Chorchoriya- M:605 F: 568\nChor Modhuchoriya- M: 351 F: 341', 4, 956, 909, 3, '["24"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(166, 'BG Chorchoriya Govt. Primary School (Benukhali): Agla: Nawabganj: Dhaka (Male & Female Voter)', 'Gokulnogor- M: 291 F: 318\nBenukhali- M: 553 F: 578', 4, 844, 896, 3, '["24"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(167, 'Miyahati Mahabubiya Kaderiya Madrasha: Galimpur: Nawabganj: Dhaka (Male & Female Voter)', 'Andharkotha- M: 499 F: 526\nKuthibari- M: 97 F: 116\nNoyadda- M: 700 F: 687\nMiyahati- M: 182 F: 195\nJoynogor surgonj- M: 573 F: 630', 8, 2051, 2154, 3, '["25"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(168, 'Galimpur Rahmania High School: Galimpur: Nawabganj: Dhaka (Male & Female Voter)', 'Borogram- M: 584 F: 604\nSurjokhali- M: 440 F: 434\nSahabad- M: 241 F: 236\nKhanhati- M: 483 F: 470', 6, 1748, 1744, 3, '["25"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(169, 'Sonahazra Govt. Primary School: Galimpur: Nawabganj: Dhaka (Male & Female Voter)', 'Chanhati- M: 173 F: 189\nNogor- M: 147 F: 154\nRamnathpur- M: 221 F: 245\nSonahazara- M: 667 F: 715\nShonkorkhali- M: 323 F: 339\nPaikesha- M: 201 F: 225', 6, 1732, 1867, 3, '["25"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(170, 'Kamarkhola Govt. Primary School: Churain: Nawabganj: Dhaka (Male & Female Voter)', 'Kamarkhola- M: 848 F: 801', 4, 848, 801, 3, '["26"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(171, 'Munshinogor Govt. Primary School: Churain: Nawabganj: Dhaka (Male & Female Voter)', 'West Munshinogor- M: 530 F: 580\nEast Munshinogor- M: 572 F: 686', 4, 1102, 1166, 3, '["26"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(172, 'Morichpotti Govt. Primary School: Churain: Nawabganj: Dhaka (Male & Female Voter)', 'Morichpotti- M: 676 F: 715\nModonkhali- M: 150 F: 140', 4, 826, 855, 3, '["26"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(173, 'Durgapur Govt. Primary School: Churain: Nawabganj: Dhaka (Male & Female Voter)', 'East Gobindopur- M: 739 F: 747\nDurgapur- M: 858 F: 843', 6, 1597, 1590, 3, '["26"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(174, 'Churain Tarini Bama High School (New Building): Centre- 1: Churain: Nawabganj: Dhaka. Male Centre.', 'West Churain- M: 1112\nEast Churain- M: 940', 3, 2052, 0, 3, '["26"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(175, 'Churain Tarini Bama High School (Old Building): Centre- 2: Churain: Nawabganj: Dhaka. Female Centre.', 'West Churain- M: 1098\nEast Churain- M: 962', 4, 0, 2060, 3, '["26"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(176, 'Gobindopur Agroni Govt. Primary School: Churain: Nawabganj: Dhaka (Male & Female Voter)', 'Sonahazra- M: 88 F: 96\nChourahati- M: 207 F: 256\nChanhati- M: 45 F: 39\nWest Gobindopur- M: 673 F: 707', 4, 1013, 1098, 3, '["26"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(177, 'Sonatola Madrasha: Churain: Nawabganj: Dhaka (Male & Female Voter)', 'Sonatola- M: 391 F: 362\nMuslimhati- M: 1065 F: 1068', 5, 1456, 1430, 3, '["26"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(178, 'Gobindopur Govt. Primary School: Churain: Nawabganj: Dhaka (Male & Female Voter)', 'Shonkorkhali- M: 347 F: 395\nPaiksha- M: 171 F: 156', 2, 518, 551, 3, '["26"]', '', '2018-10-05 18:00:00', '2018-10-05 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_organizations`
--

CREATE TABLE `bol_election_organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `bol_election_organizations`
--

INSERT INTO `bol_election_organizations` (`id`, `name`, `created_at`, `updated_at`) VALUES
(3, 'Awami league', '2018-10-02 05:39:04', '2018-10-02 05:39:04'),
(4, 'Government', '2018-10-04 03:58:29', '2018-10-04 03:58:29'),
(5, 'Police', '2018-10-04 06:25:23', '2018-10-06 04:09:50'),
(6, 'RAB', '2018-10-06 04:10:00', '2018-10-06 04:10:00'),
(7, 'UNO', '2018-10-06 05:49:35', '2018-10-06 05:49:35');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_people`
--

CREATE TABLE `bol_election_people` (
  `id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `union_id` int(11) DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `village_id` int(11) DEFAULT NULL,
  `peoplegroup_ids` text COLLATE utf8_unicode_ci NOT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `designation_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `phones` text COLLATE utf8_unicode_ci,
  `address` text COLLATE utf8_unicode_ci,
  `has_priority` tinyint(1) DEFAULT NULL,
  `is_al` tinyint(1) DEFAULT NULL,
  `extra_designation` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `in_fluential` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_people`
--

INSERT INTO `bol_election_people` (`id`, `district_id`, `upazila_id`, `union_id`, `ward_id`, `village_id`, `peoplegroup_ids`, `organization_id`, `designation_id`, `name`, `phones`, `address`, `has_priority`, `is_al`, `extra_designation`, `in_fluential`, `created_at`, `updated_at`) VALUES
(4, 18, 5, 0, 0, 0, '', 4, 19, 'Salma Khatun', '[{"type":"office","phone":"01933444057"}]', '', 0, 0, '', NULL, '2018-10-04 04:56:38', '2018-10-04 13:18:47'),
(5, 18, 5, 0, 0, 0, '', 4, 20, 'Dr. Md. Jasim Uddin', '[{"type":"office","phone":"01711016542"}]', '', 1, 0, '', NULL, '2018-10-04 04:57:22', '2018-10-04 14:28:39'),
(6, 18, 5, 0, 0, 0, '', 4, 21, 'ABM Wahidur Rahman', '[{"type":"office","phone":"01715785390"}]', '', 0, 0, '', NULL, '2018-10-04 04:57:57', '2018-10-04 13:18:45'),
(7, 18, 5, 0, 0, 0, '', 4, 22, 'Md. Sajjad Hossain', '[{"type":"office","phone":"01713373331"}]', '', 1, 0, '', NULL, '2018-10-04 04:58:40', '2018-10-06 07:57:23'),
(8, 18, 5, 0, 0, 0, '', 4, 23, 'Md. Kabir Uddin Shah', '[{"type":"office","phone":"01711908530"}]', '', 0, 0, '', NULL, '2018-10-04 04:59:34', '2018-10-05 13:39:51'),
(9, 18, 5, 0, 0, 0, '', 4, 24, 'Md. Jainul Abedin', '[{"type":"office","phone":"0172727508"}]', '', 1, 0, '', NULL, '2018-10-04 05:00:11', '2018-10-04 14:28:59'),
(10, 18, 5, 0, 0, 0, '', 4, 25, 'Md. Shahidul Islam', '[{"type":"office","phone":"01786490132"}]', '', NULL, 0, '', NULL, '2018-10-04 05:00:49', '2018-10-04 05:00:49'),
(11, 18, 5, 0, 0, 0, '', 4, 26, 'Mehedi Hasan', '[{"type":"office","phone":"01736725660"}]', '', NULL, 0, '', NULL, '2018-10-04 05:01:50', '2018-10-04 05:01:50'),
(12, 18, 5, 0, 0, 0, '', 4, 27, 'Alamgir Hossain', '[{"type":"office","phone":"01711522093"}]', '', 0, 0, '', NULL, '2018-10-04 05:02:44', '2018-10-04 09:46:57'),
(13, 18, 5, 0, 0, 0, '', 4, 28, 'Md. Masud Parvez', '[{"type":"office","phone":"01763634211"}]', '', NULL, 0, '', NULL, '2018-10-04 05:03:58', '2018-10-04 05:03:58'),
(14, 18, 5, 0, 0, 0, '', 4, 29, 'Shamima Rahim', '[{"type":"office","phone":"01713011825"}]', '', NULL, 0, '', NULL, '2018-10-04 05:04:33', '2018-10-04 05:04:33'),
(15, 18, 5, 5, 0, 0, '', 4, 30, 'Shamim Ahmad', '[]', '', 0, 0, '', NULL, '2018-10-04 05:05:40', '2018-10-04 09:46:09'),
(16, 18, 5, 5, 7, 7, '', 4, 31, 'Shamsuddin', '[{"type":"office","phone":"01711035017"}]', '', 0, 0, '', NULL, '2018-10-04 05:10:23', '2018-10-04 13:18:51'),
(17, 18, 5, 5, 0, 0, '', 3, 32, 'Sirajul Islam (Vulu)', '[{"type":"office","phone":"01711111111"}]', '', 0, 0, '', NULL, '2018-10-04 06:59:12', '2018-10-04 14:29:09'),
(18, 18, 5, 5, 0, 0, '', 3, 32, 'Bajlul Rahman Kamal', '[{"type":"office","phone":"01722222222"}]', '', 0, 0, '', NULL, '2018-10-04 07:00:31', '2018-10-04 10:45:38'),
(20, 18, 6, 0, 0, 0, '', 4, 34, 'Shahnaz Mithun Munni', '[{"type":"mobile","phone":"01933444056"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:09:41', '2018-10-06 06:32:12'),
(24, 18, 6, 0, 0, 0, '"4, 5"', 4, 37, 'Shohidul Amin', '[{"type":"office","phone":"027765018"},{"type":"mobile","phone":"01766568871"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:34:12', '2018-10-06 06:34:12'),
(22, 18, 6, 0, 0, 0, '', 4, 35, 'Mr. Ramanonda Sarker', '[{"type":"mobile","phone":"01713373360"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:27:27', '2018-10-06 06:31:43'),
(21, 18, 6, 0, 0, 0, '"4, 5"', 4, 33, 'Tofazzal Hossain', '[{"type":"office","phone":"027765001"},{"type":"home","phone":"027765005"},{"type":"mobile","phone":"01933444037"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:19:44', '2018-10-06 06:19:44'),
(23, 18, 6, 0, 0, 0, '"4, 5"', 4, 36, 'Shohidul Islam', '[{"type":"office","phone":"027765009"},{"type":"mobile","phone":"01911567415"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:31:14', '2018-10-06 06:31:14'),
(25, 18, 6, 0, 0, 0, '"4, 5"', 4, 38, 'Rakibul Ahsan', '[{"type":"mobile","phone":"01724418860"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:36:39', '2018-10-06 06:36:39'),
(26, 18, 6, 0, 0, 0, '"4, 5"', 4, 39, 'Azizul Islam', '[{"type":"office","phone":"027765249"},{"type":"mobile","phone":"01883948898"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:38:05', '2018-10-06 06:38:05'),
(27, 18, 6, 0, 0, 0, '"4, 5"', 4, 40, 'Zakir Hossain', '[{"type":"office","phone":"027765255"},{"type":"mobile","phone":"01716418937"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:39:39', '2018-10-06 06:39:39'),
(28, 18, 6, 0, 0, 0, '"4, 5"', 4, 41, 'Shaymol Chandra Poddar', '[{"type":"office","phone":"01714432399"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:41:25', '2018-10-06 06:41:25'),
(29, 18, 6, 0, 0, 0, '"4, 5"', 4, 42, 'Shajahan', '[{"type":"office","phone":"027765068"},{"type":"mobile","phone":"01711035870"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:43:17', '2018-10-06 06:43:17'),
(30, 18, 6, 0, 0, 0, '"4, 5"', 4, 43, 'Saif Md. Zulkar Nain', '[{"type":"mobile","phone":"01672947867"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:45:23', '2018-10-06 06:45:23'),
(31, 18, 6, 0, 0, 0, '"4, 5"', 4, 44, 'Harun-ur-Rashid Bhuiyan', '[{"type":"mobile","phone":"01680428862"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:46:39', '2018-10-06 06:46:39'),
(32, 18, 6, 0, 0, 0, '"4, 5"', 4, 45, 'Jesmin Ahmed', '[{"type":"office","phone":"027765123"},{"type":"mobile","phone":"01670160193"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:48:08', '2018-10-06 06:48:08'),
(33, 18, 6, 0, 0, 0, '"4, 5"', 4, 46, 'Abdus Salam', '[{"type":"office","phone":"027765128"},{"type":"mobile","phone":"01715908371"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:51:13', '2018-10-06 06:51:13'),
(34, 18, 5, 0, 0, 0, '"4, 5"', 4, 47, 'Afruza Akter Biva', '[{"type":"office","phone":"01795724915"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:51:37', '2018-10-06 06:51:37'),
(35, 18, 6, 0, 0, 0, '"4, 5"', 4, 48, 'Shanti Ranjan Debnath', '[{"type":"office","phone":"027765233"},{"type":"mobile","phone":"01761713051"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:55:37', '2018-10-06 06:55:37'),
(36, 18, 5, 0, 0, 0, '"4, 5"', 4, 49, 'Salma Khatun', '[{"type":"office","phone":"01933444057"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:55:54', '2018-10-06 06:55:54'),
(37, 18, 5, 0, 0, 0, '"4, 7"', 5, 50, 'Sajjat Hossain', '[{"type":"office","phone":"01713373331"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 06:58:07', '2018-10-06 06:58:07'),
(38, 18, 5, 0, 0, 0, '"4, 5"', 4, 51, 'Mahbuba Akter', '[{"type":"office","phone":"01554600344"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:16:36', '2018-10-06 07:16:36'),
(39, 18, 5, 0, 0, 0, '"4, 5"', 4, 36, 'Jasim Uddin', '[{"type":"office","phone":"01711016542"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:17:57', '2018-10-06 07:17:57'),
(40, 18, 5, 0, 0, 0, '"4, 5"', 4, 37, 'ABM Wahidur Rahman', '[{"type":"office","phone":"01715785390"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:18:45', '2018-10-06 07:18:45'),
(41, 18, 5, 0, 0, 0, '"4, 5"', 4, 40, 'AKM Mizanur Rahman', '[{"type":"office","phone":"01716637536"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:19:41', '2018-10-06 07:19:41'),
(42, 18, 5, 0, 0, 0, '"4, 5"', 4, 23, 'Kabir Udin Shah', '[{"type":"office","phone":"01711908530"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:20:51', '2018-10-06 07:20:51'),
(43, 18, 5, 0, 0, 0, '"4, 5"', 4, 24, 'Mohd. Joynul Abedin', '[{"type":"office","phone":"01721727508"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:22:22', '2018-10-06 07:22:22'),
(44, 18, 5, 0, 0, 0, '"4, 5"', 4, 79, 'Shahidul Islam', '[{"type":"office","phone":"01786490132"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:23:18', '2018-10-06 07:23:18'),
(45, 18, 5, 0, 0, 0, '"4, 5"', 4, 85, 'Mehedi Hasan', '[{"type":"office","phone":"01736725660"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:24:32', '2018-10-06 07:24:32'),
(46, 18, 5, 0, 0, 0, '"4, 5"', 4, 63, 'Momtaj Uddin', '[{"type":"office","phone":"01727756344"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:25:14', '2018-10-06 07:25:14'),
(47, 18, 5, 0, 0, 0, '"4, 5"', 4, 59, 'Mohd. Alauddin Mia', '[{"type":"office","phone":"01716928747"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:26:12', '2018-10-06 07:26:12'),
(48, 18, 5, 0, 0, 0, '"4, 5"', 4, 51, 'Mahbuba Akter', '[{"type":"office","phone":"01554600344"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:26:39', '2018-10-06 07:26:39'),
(49, 18, 5, 0, 0, 0, '"4, 5"', 4, 52, 'Kakon Rani Bosak', '[{"type":"office","phone":"01711144626"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:27:04', '2018-10-06 07:27:04'),
(83, 18, 5, 0, 0, 0, '"4, 5"', 4, 65, 'Joynal Abedin', '[{"type":"office","phone":"01746316418"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:38:00', '2018-10-06 07:38:00'),
(84, 18, 6, 0, 0, 0, '', 4, 80, 'Faruk Ahmed', '[{"type":"office","phone":"027765124"},{"type":"mobile","phone":"01712222939"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 08:02:17'),
(85, 18, 6, 0, 0, 0, '', 4, 81, 'Ashikur Rahman', '[{"type":"mobile","phone":"01711102211"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 08:01:30'),
(117, 18, 5, 0, 0, 0, '"4, 9"', 4, 61, 'Hamidur Rahman', '[{"type":"office","phone":"01716902211"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:12:59', '2018-10-06 08:12:59'),
(82, 18, 5, 0, 0, 0, '"4, 5"', 4, 87, 'Shilpi Naag', '[{"type":"office","phone":"01716504029"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:34:51', '2018-10-06 07:34:51'),
(86, 18, 6, 0, 0, 0, '', 4, 51, 'Farzana Abedin', '[{"type":"office","phone":"027765015"},{"type":"mobile","phone":"01711102211"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 08:00:19'),
(87, 18, 6, 0, 0, 0, '', 4, 77, 'Mostafa Kamal', '[{"type":"office","phone":"027765125"},{"type":"mobile","phone":"01713373330"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:59:17'),
(88, 18, 6, 0, 0, 0, '', 4, 78, 'AKM Shamim Hasan', '[{"type":"mobile","phone":"01711102211"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:58:37'),
(89, 18, 6, 0, 0, 0, '', 4, 79, 'Habibullah Mia', '[{"type":"office","phone":"027765122"},{"type":"mobile","phone":"01712363723"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:58:06'),
(90, 18, 6, 0, 0, 0, '', 4, 52, 'Akbar Hossain Mia', '[{"type":"mobile","phone":"01732074033"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:57:28'),
(91, 18, 6, 0, 0, 0, '', 4, 53, 'Aksaur Rahman', '[{"type":"mobile","phone":"01732466049"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:57:01'),
(118, 18, 5, 0, 0, 0, '"4, 5"', 4, 91, 'Kazi Sahida Begum', '[{"type":"office","phone":"01711930758"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:17:40', '2018-10-06 08:17:40'),
(92, 18, 6, 0, 0, 0, '', 4, 54, 'Ibn Mayaz Pramanik', '[{"type":"mobile","phone":"01722109822"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:56:32'),
(93, 18, 6, 0, 0, 0, '', 4, 55, 'H M Shahin', '[{"type":"mobile","phone":"01712255819"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:55:57'),
(94, 18, 6, 0, 0, 0, '', 4, 56, 'Shahjalal', '[{"type":"office","phone":"027765066"},{"type":"mobile","phone":"01720078857"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:54:59'),
(81, 18, 5, 0, 0, 0, '"4, 5"', 4, 86, 'Bolram', '[{"type":"office","phone":"01727058979"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:32:05', '2018-10-06 07:32:05'),
(95, 18, 6, 0, 0, 0, '', 4, 57, 'Nazrul Islam Sheikh', '[{"type":"mobile","phone":"01552631176"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:54:05'),
(119, 18, 5, 0, 0, 0, '"4, 5"', 4, 81, 'Liakot Ali', '[{"type":"office","phone":"01711337660"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:18:57', '2018-10-06 08:18:57'),
(96, 18, 6, 0, 0, 0, '', 4, 58, 'Rahima Begum', '[{"type":"office","phone":"027765126"},{"type":"mobile","phone":"01718144197"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:53:26'),
(97, 18, 6, 0, 0, 0, '', 4, 59, 'Nasir Uddin', '[{"type":"mobile","phone":"0000"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:52:51'),
(98, 18, 6, 0, 0, 0, '', 4, 60, 'Taibur Rahman', '[{"type":"office","phone":"01716048269"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:51:47'),
(99, 18, 6, 0, 0, 0, '', 4, 61, 'Z M Shamsul Alam', '[{"type":"office","phone":"027765127"},{"type":"mobile","phone":"01712919504"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:51:10'),
(100, 18, 6, 0, 0, 0, '', 4, 62, 'Mohd. Ayub Mia', '[{"type":"mobile","phone":"01716209933"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:50:24'),
(101, 18, 6, 0, 0, 0, '', 4, 63, 'Momtaz Uddin', '[{"type":"mobile","phone":"01727756344"},{"type":"office","phone":"01556452594"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:49:42'),
(102, 18, 6, 0, 0, 0, '', 4, 64, 'Ohidul Islam', '[{"type":"mobile","phone":"01746151447"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:49:02'),
(120, 18, 5, 0, 0, 0, '"4, 5"', 4, 60, 'Mansur Rahman', '[{"type":"office","phone":"01730992142"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:20:05', '2018-10-06 08:20:05'),
(103, 18, 6, 0, 0, 0, '', 4, 88, 'Matiur Rahman', '[{"type":"mobile","phone":"01726532247"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:48:06'),
(104, 18, 6, 0, 0, 0, '', 4, 66, 'Fazlul Karim', '[{"type":"mobile","phone":"01711019389"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:46:37'),
(105, 18, 6, 0, 0, 0, '', 4, 67, 'Asim Kumar Das', '[{"type":"mobile","phone":"01769400025"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:46:10'),
(106, 18, 6, 0, 0, 0, '', 4, 68, 'Mominul Islam', '[{"type":"mobile","phone":"01769400410"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:45:42'),
(107, 18, 6, 0, 0, 0, '', 4, 69, 'Arab Ali Sheikh', '[{"type":"mobile","phone":"01769400411"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:44:37'),
(121, 18, 5, 0, 0, 0, '"4, 5"', 4, 39, 'ABM Zakaria', '[{"type":"office","phone":"01717046936"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:21:27', '2018-10-06 08:21:27'),
(108, 18, 6, 0, 0, 0, '', 4, 70, 'Abdul Kader Jilani', '[{"type":"mobile","phone":"01769400412"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:43:54'),
(109, 18, 6, 0, 0, 0, '', 4, 71, 'Mahbubur Rahman', '[{"type":"mobile","phone":"01769400413"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:43:21'),
(110, 18, 6, 0, 0, 0, '', 4, 72, 'Anwarul Haq', '[{"type":"office","phone":"01716932614"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:42:52'),
(111, 18, 6, 0, 0, 0, '', 4, 73, 'Saidur Rahman', '[{"type":"office","phone":"01552301000"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:41:56'),
(112, 18, 6, 0, 0, 0, '', 4, 74, 'Shamishtha Kundu', '[{"type":"mobile","phone":"01931998238"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:42:20'),
(122, 18, 5, 0, 0, 0, '"4, 5"', 4, 64, 'Hafiza Hakim Ruma', '[{"type":"office","phone":"01715169595"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:22:19', '2018-10-06 08:22:19'),
(123, 18, 5, 0, 0, 0, '"4, 5"', 4, 45, 'Hindol Bari', '[{"type":"office","phone":"01711906438"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:25:04', '2018-10-06 08:25:04'),
(114, 18, 6, 0, 0, 0, '', 4, 76, 'Taomal Das', '[{"type":"mobile","phone":"01712524220"},{"type":"mobile","phone":"01737097107"}]', '', NULL, NULL, NULL, NULL, NULL, '2018-10-06 07:40:11'),
(115, 18, 5, 0, 0, 0, '"4, 5"', 4, 53, 'Badsha Mia', '[{"type":"office","phone":"01913478419"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:38:57', '2018-10-06 07:38:57'),
(116, 18, 5, 0, 0, 0, '"4, 5"', 4, 48, 'Kamrul Islam Khan', '[{"type":"office","phone":"01781217589"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 07:39:47', '2018-10-06 07:39:47'),
(124, 18, 5, 0, 0, 0, '"4, 5"', 4, 46, 'Sabbir Hossain', '[{"type":"office","phone":"01676766368"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:26:30', '2018-10-06 08:26:30'),
(125, 18, 5, 0, 0, 0, '"4, 5"', 4, 92, 'Salah Uddin', '[{"type":"office","phone":"01743732835"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:27:57', '2018-10-06 08:27:57'),
(126, 18, 5, 0, 0, 0, '"4, 5"', 4, 58, 'Sadia Afrin', '[{"type":"office","phone":"01677057166, 01914139981"}]', '', NULL, NULL, NULL, NULL, '2018-10-06 08:29:44', '2018-10-06 08:29:44'),
(127, 18, 5, 0, 0, 0, '"4, 5"', 4, 57, 'Samsunnahar Khan', '[{"type":"office","phone":"01711241246"}]', 'Youth Development Officer', NULL, NULL, NULL, NULL, '2018-10-06 08:33:57', '2018-10-06 08:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_people_groups`
--

CREATE TABLE `bol_election_people_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `bol_election_people_groups`
--

INSERT INTO `bol_election_people_groups` (`id`, `name`, `sort_order`, `created_at`, `updated_at`, `parent_id`, `nest_left`, `nest_right`, `nest_depth`) VALUES
(4, 'Government', 0, '2018-10-04 13:41:50', '2018-10-04 13:48:40', 0, 1, 12, 0),
(5, 'Administration', 0, '2018-10-04 13:42:04', '2018-10-04 13:45:07', 4, 2, 3, 1),
(6, 'Law Enforcement', 0, '2018-10-04 13:42:11', '2018-10-04 13:48:36', 4, 4, 11, 1),
(7, 'Police', 0, '2018-10-04 13:43:49', '2018-10-04 13:45:41', 6, 5, 6, 2),
(8, 'RAB', 0, '2018-10-04 13:43:59', '2018-10-04 13:45:58', 6, 7, 8, 2),
(9, 'Ansar & VDP', 0, '2018-10-04 13:44:08', '2018-10-04 13:48:36', 6, 9, 10, 2),
(10, 'Local Government', 0, '2018-10-04 13:44:16', '2018-10-04 13:48:40', 0, 13, 14, 0),
(11, 'Political Party', 0, '2018-10-04 13:44:27', '2018-10-04 13:44:27', 0, 15, 16, 0),
(12, 'Important People', 0, '2018-10-04 13:44:35', '2018-10-04 13:44:35', 0, 17, 18, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_union`
--

CREATE TABLE `bol_election_union` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_union`
--

INSERT INTO `bol_election_union` (`id`, `name`, `district_id`, `upazila_id`, `created_at`, `updated_at`) VALUES
(6, 'Kusumhathi', 18, 5, '2018-10-03 09:56:31', '2018-10-03 12:56:17'),
(5, 'Nayabari', 18, 5, '2018-10-03 09:55:51', '2018-10-03 09:55:51'),
(7, 'Raipara', 18, 5, '2018-10-03 09:57:06', '2018-10-03 09:57:06'),
(8, 'Sutarpara', 18, 5, '2018-10-03 09:57:28', '2018-10-03 09:57:28'),
(9, 'Narisha', 18, 5, '2018-10-03 09:57:51', '2018-10-03 09:57:51'),
(10, 'Muksudpur', 18, 5, '2018-10-03 09:58:12', '2018-10-03 09:58:12'),
(11, 'Mahmudpur', 18, 5, '2018-10-03 09:58:36', '2018-10-03 09:58:36'),
(12, 'Bilaspur', 18, 5, '2018-10-03 09:59:05', '2018-10-03 12:58:15'),
(13, 'Shikaripara', 18, 6, '2018-10-03 12:46:37', '2018-10-03 12:46:37'),
(14, 'Joykrishnapur', 18, 6, '2018-10-03 12:48:01', '2018-10-03 12:48:01'),
(15, 'Baruakhali', 18, 6, '2018-10-03 12:48:37', '2018-10-03 12:48:37'),
(16, 'Nayansree', 18, 6, '2018-10-03 12:50:49', '2018-10-03 12:50:49'),
(17, 'Sholla', 18, 6, '2018-10-03 12:51:33', '2018-10-03 12:51:33'),
(18, 'Jantrail', 18, 6, '2018-10-03 12:52:00', '2018-10-03 12:52:00'),
(19, 'Bandura', 18, 6, '2018-10-03 12:52:20', '2018-10-03 12:52:20'),
(20, 'Kalakopa', 18, 6, '2018-10-03 12:52:53', '2018-10-03 12:52:53'),
(21, 'Bakshanagar', 18, 6, '2018-10-03 12:53:17', '2018-10-03 12:53:17'),
(22, 'Barrah', 18, 6, '2018-10-03 12:53:41', '2018-10-03 12:53:41'),
(23, 'Kailail', 18, 6, '2018-10-03 12:54:08', '2018-10-03 12:54:08'),
(24, 'Agla', 18, 6, '2018-10-03 12:54:33', '2018-10-03 12:54:33'),
(25, 'Galimpur', 18, 6, '2018-10-03 12:54:54', '2018-10-03 12:54:54'),
(26, 'Churain', 18, 6, '2018-10-03 12:55:20', '2018-10-03 12:55:20');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_upazila`
--

CREATE TABLE `bol_election_upazila` (
  `id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_upazila`
--

INSERT INTO `bol_election_upazila` (`id`, `district_id`, `name`, `created_at`, `updated_at`) VALUES
(6, 18, 'Nawabganj', '2018-10-03 12:45:38', '2018-10-03 12:50:58'),
(5, 18, 'Dohar', '2018-10-03 09:55:09', '2018-10-03 09:55:09');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_village`
--

CREATE TABLE `bol_election_village` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `union_id` int(11) DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_village`
--

INSERT INTO `bol_election_village` (`id`, `name`, `district_id`, `upazila_id`, `union_id`, `ward_id`, `created_at`, `updated_at`) VALUES
(1, 'Uttar Awranggabad', 18, 5, 5, 1, '2018-10-02 18:00:00', '2018-10-02 18:00:00'),
(2, 'Modda Awranggabad', 18, 5, 5, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Dakkhin Awranggabad', 18, 5, 5, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Pankundu', 18, 5, 5, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Barrah', 18, 5, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Anta', 18, 5, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Pashchim Dhuyair', 18, 5, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Modda Dhuyair', 18, 5, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Purbo Dhuyair', 18, 5, 5, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Uttar Shilakuta', 18, 5, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Dakkhin Shilakuta', 18, 5, 6, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Deovhog', 18, 5, 6, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Char Purulia', 18, 5, 6, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Sundoripara', 18, 5, 6, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Kartikpur', 18, 5, 6, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Kusumpur', 18, 5, 6, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Boro Basta', 18, 5, 6, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Boro Basta', 18, 5, 6, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Choto Basta', 18, 5, 6, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Babu Dangi', 18, 5, 6, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Char Kushai', 18, 5, 6, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Char Baita', 18, 5, 6, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Char Kushai', 18, 5, 6, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Puspo Khali', 18, 5, 6, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Mahtab Nagor', 18, 5, 6, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Arita', 18, 5, 6, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Auliabad', 18, 5, 6, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Imam Nagor', 18, 5, 6, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Ikrashi', 18, 5, 7, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Lokkhi Prashad', 18, 5, 7, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Haturpara', 18, 5, 7, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Palamgang', 18, 5, 7, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Jamalchar', 18, 5, 7, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Karimgong', 18, 5, 7, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Roghudevpur', 18, 5, 7, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'Raipara', 18, 5, 7, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Khalpara', 18, 5, 7, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Islampur Khalpara', 18, 5, 7, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Islampur', 18, 5, 7, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'Khathalighat', 18, 5, 7, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'Lottakhela', 18, 5, 7, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'Nagerkanda', 18, 5, 7, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'Char Nagerkanda', 18, 5, 7, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Katakhali', 18, 5, 8, 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'Pankara', 18, 5, 8, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'Banaghata', 18, 5, 8, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'Doharghata', 18, 5, 8, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'Dohar', 18, 5, 8, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'Modhruchar', 18, 5, 8, 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'Purbo Sutarpara', 18, 5, 8, 34, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'Kajirchar', 18, 5, 8, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'Purba Sutarpara', 18, 5, 8, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'Gazirtak', 18, 5, 8, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'Dayarkum', 18, 5, 8, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'Dayagojaria', 18, 5, 8, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'Mizan Nagar', 18, 5, 8, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'Gharmora', 18, 5, 8, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'Munsikanda', 18, 5, 8, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'Moksudpur', 18, 5, 10, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'Moksudpur', 18, 5, 10, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'Moksudpur', 18, 5, 10, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'Dhalarpar', 18, 5, 10, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'Khoria', 18, 5, 10, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'Khasertek', 18, 5, 10, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'Baniabari', 18, 5, 10, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'Dhakkin Khoria', 18, 5, 10, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'Bethuya', 18, 5, 10, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'Ulttadangi', 18, 5, 10, 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'Dhakkin Modhurkhula', 18, 5, 10, 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'Modhurkhula', 18, 5, 10, 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'Moitpara', 18, 5, 10, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'Ramyath', 18, 5, 10, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'Moura', 18, 5, 10, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'Chotorvugh', 18, 5, 10, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'Dripur', 18, 5, 10, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'Duboli', 18, 5, 10, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'Uttar Moura', 18, 5, 10, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'Char Baita', 18, 5, 11, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'Char Balishpur', 18, 5, 11, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'Char Kushair', 18, 5, 11, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'Horichondi', 18, 5, 11, 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'Hossainpur', 18, 5, 11, 50, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'Kusumhati', 18, 5, 11, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'Lotakhola', 18, 5, 11, 52, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'Moinot', 18, 5, 11, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'Narayanpur', 18, 5, 11, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'Purulia', 18, 5, 11, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'Mahmudpur', 18, 5, 11, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'Khukdebpur', 18, 5, 11, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'Srikrishnopur', 18, 5, 11, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'Bilaspur', 18, 5, 12, 55, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'Chotoram Nathpur', 18, 5, 12, 55, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'Hajarbhika', 18, 5, 12, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'Alamkhar Char', 18, 5, 12, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'Alinagar', 18, 5, 12, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'Char devnathpur', 18, 5, 12, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Majhirchar', 18, 5, 12, 57, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'Bororam Nathpur', 18, 5, 12, 57, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'Purbochar', 18, 5, 12, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'Radhanagar Uttar', 18, 5, 12, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'Radhanagar Dokkin', 18, 5, 12, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'Debinagar Uttar', 18, 5, 12, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'Kutubpur', 18, 5, 12, 61, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'Debinagar Dokkin', 18, 5, 12, 62, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'Kulchuri', 18, 5, 12, 63, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'Krishdevpur', 18, 5, 12, 63, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'Ananda Nagar', 18, 6, 13, 64, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(108, 'Monikanda', 18, 6, 13, 64, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(109, 'Mohespur', 18, 6, 13, 65, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(110, 'Bishumpur', 18, 6, 13, 65, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(111, 'Sonatola', 18, 6, 13, 65, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(112, 'Sibpur', 18, 6, 13, 66, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(113, 'Shekor Nagar', 18, 6, 13, 66, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(114, 'Notti', 18, 6, 13, 66, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(115, 'Nurpur', 18, 6, 13, 66, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(116, 'Vordakanda', 18, 6, 13, 66, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(117, 'Shikaripara', 18, 6, 13, 67, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(118, 'Komduppur', 18, 6, 13, 67, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(119, 'Loskorkanda', 18, 6, 13, 68, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(120, 'Charbaguni', 18, 6, 13, 68, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(121, 'Hagradi', 18, 6, 13, 68, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(122, 'Norshikhopur', 18, 6, 13, 68, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(123, 'Noyabondi', 18, 6, 13, 68, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(124, 'Gorippur', 18, 6, 13, 69, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(125, 'Bokter Nagar', 18, 6, 13, 69, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(126, 'Sherpur', 18, 6, 13, 70, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(127, 'Sujapur', 18, 6, 13, 70, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(128, 'Narayanpur', 18, 6, 13, 71, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(129, 'Daudpur', 18, 6, 13, 71, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(130, 'Panjipohori', 18, 6, 13, 72, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(131, 'Gongadia', 18, 6, 13, 72, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(132, 'Sonabaju', 18, 6, 14, 73, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(133, 'Sonkardia', 18, 6, 14, 74, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(134, 'Bahadur', 18, 6, 14, 74, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(135, 'Joykrishnapur', 18, 6, 14, 74, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(136, 'Shampur', 18, 6, 14, 74, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(137, 'Gojaria', 18, 6, 14, 74, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(138, 'Bilchori', 18, 6, 14, 74, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(139, 'Kollansri', 18, 6, 14, 75, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(140, 'Tilpaldia', 18, 6, 14, 75, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(141, 'Panikur', 18, 6, 14, 75, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(142, 'Kuthuri', 18, 6, 14, 76, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(143, 'Bamuyahati', 18, 6, 14, 76, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(144, 'Bramongram', 18, 6, 14, 76, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(145, 'Asoypur', 18, 6, 14, 76, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(146, 'Raipur', 18, 6, 14, 77, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(147, 'Kedarpur', 18, 6, 14, 77, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(148, 'Ghushail', 18, 6, 14, 78, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(149, 'ArGhushail', 18, 6, 14, 78, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(150, 'Khatbazar', 18, 6, 14, 78, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(151, 'Rajapur', 18, 6, 14, 79, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(152, 'Rajapur Noyadangi', 18, 6, 14, 79, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(153, 'Batui Muri', 18, 6, 14, 79, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(154, 'Ballenga', 18, 6, 14, 80, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(155, 'Dhuyari', 18, 6, 14, 80, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(156, 'Purbochok', 18, 6, 14, 81, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(157, 'Mothbari', 18, 6, 14, 81, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(158, 'Charakhali', 18, 6, 14, 81, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(159, 'Batui Juri', 18, 6, 14, 81, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(160, 'Dattarkanda', 18, 6, 15, 82, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(161, 'Baherchor', 18, 6, 15, 82, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(162, 'Bramon khali', 18, 6, 15, 82, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(163, 'Uttar Baruakhali', 18, 6, 15, 83, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(164, 'Nobogram', 18, 6, 15, 83, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(165, 'Madol', 18, 6, 15, 83, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(166, 'Dokkin Baruakhali', 18, 6, 15, 83, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(167, 'Chok Barilla', 18, 6, 15, 84, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(168, 'Drigogram', 18, 6, 15, 84, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(169, 'Joinotpur', 18, 6, 15, 84, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(170, 'Vanga Para', 18, 6, 15, 84, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(171, 'Korpara', 18, 6, 15, 84, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(172, 'Dorikandi', 18, 6, 15, 85, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(173, 'Veramuria', 18, 6, 15, 85, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(174, 'Kumarbarilla', 18, 6, 15, 85, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(175, 'Kandabarilla', 18, 6, 15, 85, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(176, 'Boro Barilla', 18, 6, 15, 86, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(177, 'Chotropur', 18, 6, 15, 86, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(178, 'Shialjan', 18, 6, 15, 86, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(179, 'Munsigong', 18, 6, 15, 87, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(180, 'Uttar Alalpur', 18, 6, 15, 87, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(181, 'Dokkin Alalpur', 18, 6, 15, 88, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(182, 'Prithinodda', 18, 6, 15, 88, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(183, 'Ramnagar', 18, 6, 15, 89, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(184, 'Ratanpur', 18, 6, 15, 89, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(185, 'Jahanabad', 18, 6, 15, 89, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(186, 'Kanchonagar', 18, 6, 15, 90, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(187, 'Boro Kounakandi', 18, 6, 15, 90, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(188, 'Choto Kounakandi', 18, 6, 15, 90, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(189, 'Shahedgong', 18, 6, 16, 91, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(190, 'Uttar Barrah', 18, 6, 16, 91, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(191, 'Ghospara', 18, 6, 16, 91, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(192, 'Vouadubi', 18, 6, 16, 92, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(193, 'Vurakhali', 18, 6, 16, 92, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(194, 'Choto Tasula', 18, 6, 16, 92, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(195, 'Bripitasula', 18, 6, 16, 92, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(196, 'Bilpolli', 18, 6, 16, 93, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(197, 'Char Tuital', 18, 6, 16, 93, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(198, 'Puran Tuital', 18, 6, 16, 93, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(199, 'Nuton Tuital', 18, 6, 16, 93, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(200, 'Bokchor', 18, 6, 16, 93, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(201, 'Chor Baguni', 18, 6, 16, 94, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(202, 'Abjalnagar', 18, 6, 16, 94, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(203, 'Chor Cholla', 18, 6, 16, 94, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(204, 'Khanepur Kandi', 18, 6, 16, 94, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(205, 'Kanda Kanepur', 18, 6, 16, 95, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(206, 'Chor Kanepur', 18, 6, 16, 95, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(207, 'Radhakantopur', 18, 6, 16, 95, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(208, 'Rahuthati', 18, 6, 16, 96, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(209, 'Nayansree', 18, 6, 16, 96, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(210, 'Sahajatpur', 18, 6, 16, 97, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(211, 'Kasinagar', 18, 6, 16, 97, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(212, 'Deutola', 18, 6, 16, 98, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(213, 'Paddikanda', 18, 6, 16, 98, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(214, 'Sapleja', 18, 6, 16, 98, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(215, 'Matbortek', 18, 6, 16, 99, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(216, 'Purbo Saitan Kati', 18, 6, 16, 99, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(217, 'Choto Golla', 18, 6, 16, 99, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(218, 'Boro Golla', 18, 6, 16, 99, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(219, 'Kumargolla', 18, 6, 16, 99, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(220, 'Krishnonagar', 18, 6, 16, 99, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(221, 'Uttar balukhondo', 18, 6, 17, 100, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(222, 'Dokkin Balukhondo', 18, 6, 17, 100, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(223, 'Mohisdia', 18, 6, 17, 100, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(224, 'Patiljap', 18, 6, 17, 101, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(225, 'Sikhora', 18, 6, 17, 101, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(226, 'Parasura', 18, 6, 17, 101, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(227, 'Abdani', 18, 6, 17, 202, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(228, 'Chokoria', 18, 6, 17, 103, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(229, 'Khotia', 18, 6, 17, 103, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(230, 'Jonjonia', 18, 6, 17, 103, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(231, 'Ajgora', 18, 6, 17, 103, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(232, 'Modmohonpur', 18, 6, 17, 103, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(233, 'Konda', 18, 6, 17, 104, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(234, 'Auna', 18, 6, 17, 104, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(235, 'Ayatkand', 18, 6, 17, 104, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(236, 'Duddhata', 18, 6, 17, 104, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(237, 'doukhondo', 18, 6, 17, 105, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(238, 'Kumosri', 18, 6, 17, 105, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(239, 'Sulanagar', 18, 6, 17, 105, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(240, 'Ayikbari', 18, 6, 17, 106, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(241, 'Sinhora', 18, 6, 17, 106, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(242, 'Nayahati', 18, 6, 17, 107, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(243, 'Chok Sinhora', 18, 6, 17, 107, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(244, 'Sholla', 18, 6, 17, 108, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(245, 'Bouali', 18, 6, 17, 108, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(246, 'Sinjur', 18, 6, 17, 108, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(247, 'Katrikpur', 18, 6, 17, 109, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(248, 'Sultanpur', 18, 6, 17, 109, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(249, 'Ulail', 18, 6, 17, 109, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(250, 'Chok Auna', 18, 6, 17, 109, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(251, 'Jantrail', 18, 6, 18, 110, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(252, 'Azizpur', 18, 6, 18, 110, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(253, 'Chondrokhula', 18, 6, 18, 111, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(254, 'Voulia', 18, 6, 18, 112, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(255, 'Jalalchor', 18, 6, 18, 113, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(256, 'Horiskul Uttar', 18, 6, 18, 114, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(257, 'Debukhali', 18, 6, 18, 115, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(258, 'Horiskul Dokkin', 18, 6, 18, 116, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(259, 'Gobindopur', 18, 6, 18, 117, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(260, 'Moymondi', 18, 6, 18, 117, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(261, 'Mohobbotpur', 18, 6, 19, 118, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(262, 'Majirkanda', 18, 6, 19, 118, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(263, 'Alladipur', 18, 6, 19, 119, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(264, 'Hazratpur', 18, 6, 19, 119, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(265, 'Sadapur', 18, 6, 19, 119, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(266, 'Imamnagar', 18, 6, 19, 119, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(267, 'Sayedpur', 18, 6, 19, 120, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(268, 'Harikanda', 18, 6, 19, 120, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(269, 'Mridharkanda', 18, 6, 19, 120, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(270, 'Khatalighata', 18, 6, 19, 120, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(271, 'Puraton Bandura', 18, 6, 19, 121, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(272, 'Hasnabad', 18, 6, 19, 122, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(273, 'Molasikanda', 18, 6, 19, 122, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(274, 'Hasnabad', 18, 6, 19, 123, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(275, 'Moulovibazar', 18, 6, 19, 123, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(276, 'Notun Bandura', 18, 6, 19, 124, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(277, 'Notun Bandura', 18, 6, 19, 125, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(278, 'Nurnagar', 18, 6, 19, 125, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(279, 'Mirerbingi', 18, 6, 19, 125, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(280, 'Nayanagar', 18, 6, 19, 126, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(281, 'Bardurari', 18, 6, 19, 126, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(282, 'Champanagar', 18, 6, 19, 126, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(283, 'Goalnagar', 18, 6, 20, 127, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(284, 'Golapur', 18, 6, 20, 127, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(285, 'Moddonagar', 18, 6, 20, 127, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(286, 'Rajarampur', 18, 6, 20, 128, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(287, 'Boronagar', 18, 6, 20, 128, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(288, 'Baghati', 18, 6, 20, 128, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(289, 'Bourahati', 18, 6, 20, 128, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(290, 'Mohajonpur', 18, 6, 20, 129, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(291, 'Vuyakhali', 18, 6, 20, 129, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(292, 'Pukurpar', 18, 6, 20, 129, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(293, 'Khondokar', 18, 6, 20, 129, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(294, 'Gupikantopur', 18, 6, 20, 129, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(295, 'Porchim Somsabad', 18, 6, 20, 130, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(296, 'Panalia', 18, 6, 20, 130, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(297, 'Purbo Samsabad', 18, 6, 20, 131, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(298, 'Nowabganj', 18, 6, 20, 131, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(299, 'Kasimpur', 18, 6, 20, 132, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(300, 'Khandohati', 18, 6, 20, 133, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(301, 'Madhubpur', 18, 6, 20, 133, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(302, 'Baghmara', 18, 6, 20, 134, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(303, 'Amirpur', 18, 6, 20, 134, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(304, 'Jalalpur', 18, 6, 20, 134, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(305, 'Surgong', 18, 6, 20, 134, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(306, 'Boikontopur', 18, 6, 20, 134, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(307, 'Rajpara', 18, 6, 20, 135, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(308, 'Pirmamudia', 18, 6, 20, 135, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(309, 'Bibirchor', 18, 6, 20, 135, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(310, 'Gazikhali', 18, 6, 21, 136, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(311, 'Patkhanda', 18, 6, 21, 137, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(312, 'Choto Rajpara', 18, 6, 21, 138, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(313, 'Choto Boxnagar', 18, 6, 21, 139, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(314, 'Chourahati', 18, 6, 21, 140, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(315, 'Sabukhati', 18, 6, 21, 141, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(316, 'Digirpar', 18, 6, 21, 142, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(317, 'Balurchor', 18, 6, 21, 143, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(318, 'Choto Zafarpur', 18, 6, 21, 144, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(319, 'Bollomchor', 18, 6, 22, 145, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(320, 'Kahur', 18, 6, 22, 146, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(321, 'Ubarchor', 18, 6, 22, 147, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(322, 'Kandamatra', 18, 6, 22, 148, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(323, 'Baghbari', 18, 6, 22, 149, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(324, 'Barrah', 18, 6, 22, 150, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(325, 'Barrah Porchimpar', 18, 6, 22, 151, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(326, 'Shovoria', 18, 6, 22, 152, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(327, 'Barrah Purbopar', 18, 6, 22, 153, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(328, 'Dokkin kailail', 18, 6, 23, 154, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(329, 'Uttar kailail', 18, 6, 23, 155, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(330, 'Telenga', 18, 6, 23, 156, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(331, 'Paragram', 18, 6, 23, 157, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(332, 'Dorikanda', 18, 6, 23, 158, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(333, 'Modhupur', 18, 6, 23, 159, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(334, 'Nayakanda', 18, 6, 23, 160, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(335, 'Malikanda', 18, 6, 23, 161, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(336, 'Vangavita', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(337, 'Raipur', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(338, 'Uttar Meleng', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(339, 'Meleng', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(340, 'Purbo Meleng', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(341, 'Porchim Meleng', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(342, 'Dignaga', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(343, 'Sonarga', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(344, 'Matabpur', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(345, 'Katakhali', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(346, 'Doulatpur', 18, 6, 23, 162, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(347, 'Agla', 18, 6, 24, 163, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(348, 'Agla', 18, 6, 24, 164, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(349, 'Agla', 18, 6, 24, 165, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(350, 'Agla', 18, 6, 24, 166, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(351, 'Agla', 18, 6, 24, 167, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(352, 'Agla', 18, 6, 24, 168, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(353, 'Agla', 18, 6, 24, 169, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(354, 'Agla', 18, 6, 24, 170, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(355, 'Agla', 18, 6, 24, 171, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(356, 'Andarkuta', 18, 6, 25, 172, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(357, 'Kuthibari', 18, 6, 25, 173, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(358, 'Nouadda', 18, 6, 25, 174, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(359, 'Miahati', 18, 6, 25, 175, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(360, 'Adosgram', 18, 6, 25, 176, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(361, 'Surgram', 18, 6, 25, 177, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(362, 'Joynagar', 18, 6, 25, 178, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(363, 'Borogram', 18, 6, 25, 179, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(364, 'Surjokhali', 18, 6, 25, 180, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(365, 'Shahbad', 18, 6, 25, 180, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(366, 'Khanhati', 18, 6, 25, 180, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(367, 'Nagar', 18, 6, 25, 180, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(368, 'Ramnathpur', 18, 6, 25, 180, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(369, 'Chanhati', 18, 6, 25, 180, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(370, 'Sonahajra', 18, 6, 25, 180, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(371, 'Sonkarkhali', 18, 6, 25, 180, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(372, 'Paiksha', 18, 6, 25, 180, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(373, 'Gobindopur', 18, 6, 26, 181, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(374, 'Churahati', 18, 6, 26, 181, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(375, 'Muslemhati', 18, 6, 26, 182, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(376, 'Sonatola', 18, 6, 26, 183, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(377, 'Paiksha', 18, 6, 26, 184, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(378, 'Songarkhali', 18, 6, 26, 185, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(379, 'Sona Hajra', 18, 6, 26, 186, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(380, 'Chanhati', 18, 6, 26, 187, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(381, 'Gurgapur', 18, 6, 26, 188, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(382, 'Purchim Churain', 18, 6, 26, 188, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(383, 'Purbo Churain', 18, 6, 26, 188, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(384, 'Kamarkanda', 18, 6, 26, 188, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(385, 'Porchim Munsigong', 18, 6, 26, 188, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(386, 'Purbo Munsigong', 18, 6, 26, 188, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(387, 'Modonkhali', 18, 6, 26, 188, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(388, 'Morispotti', 18, 6, 26, 188, '2018-10-05 18:00:00', '2018-10-05 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_ward`
--

CREATE TABLE `bol_election_ward` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `union_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bol_election_ward`
--

INSERT INTO `bol_election_ward` (`id`, `name`, `district_id`, `upazila_id`, `union_id`, `created_at`, `updated_at`) VALUES
(1, 'Ward 1', 18, 5, 5, '2018-10-02 18:00:00', '2018-10-02 18:00:00'),
(2, 'Ward 2', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Ward 3', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Ward 4', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Ward 5', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Ward 6', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Ward 7', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Ward 8', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Ward 9', 18, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Ward 1', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Ward 2', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Ward 3', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Ward 4', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Ward 5', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Ward 6', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Ward 7', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Ward 8', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Ward 9', 18, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Ward 1', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Ward 2', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Ward 3', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Ward 4', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Ward 5', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Ward 6', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Ward 7', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Ward 8', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Ward 9', 18, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Ward 1', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Ward 2', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Ward 3', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Ward 4', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Ward 5', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Ward 6', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Ward 7', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Ward 8', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'Ward 9', 18, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Ward 1', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Ward 2', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Ward 3', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'Ward 4', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'Ward 5', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'Ward 6', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'Ward 7', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Ward 8', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'Ward 9', 18, 5, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'Ward 1', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'Ward 2', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'Ward 3', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'Ward 4', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'Ward 5', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'Ward 6', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'Ward 7', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'Ward 8', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'Ward 9', 18, 5, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'Ward 1', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'Ward 2', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'Ward 3', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'Ward 4', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'Ward 5', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'Ward 6', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'Ward 7', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'Ward 8', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'Ward 9', 18, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'Ward 1', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(65, 'Ward 2', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(66, 'Ward 3', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(67, 'Ward 4', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(68, 'Ward 5', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(69, 'Ward 6', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(70, 'Ward 7', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(71, 'Ward 8', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(72, 'Ward 9', 18, 6, 13, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(73, 'Ward 1', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(74, 'Ward 2', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(75, 'Ward 3', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(76, 'Ward 4', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(77, 'Ward 5', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(78, 'Ward 6', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(79, 'Ward 7', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(80, 'Ward 8', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(81, 'Ward 9', 18, 6, 14, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(82, 'Ward 1', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(83, 'Ward 2', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(84, 'Ward 3', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(85, 'Ward 4', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(86, 'Ward 5', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(87, 'Ward 6', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(88, 'Ward 7', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(89, 'Ward 8', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(90, 'Ward 9', 18, 6, 15, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(91, 'Ward 1', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(92, 'Ward 2', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(93, 'Ward 3', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(94, 'Ward 4', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(95, 'Ward 5', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(96, 'Ward 6', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(97, 'Ward 7', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(98, 'Ward 8', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(99, 'Ward 9', 18, 6, 16, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(100, 'Ward 1', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(101, 'Ward 2', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(102, 'Ward 3', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(103, 'Ward 4', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(104, 'Ward 5', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(105, 'Ward 6', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(106, 'Ward 7', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(107, 'Ward 8', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(108, 'Ward 9', 18, 6, 17, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(109, 'Ward 1', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(110, 'Ward 2', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(111, 'Ward 3', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(112, 'Ward 4', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(113, 'Ward 5', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(114, 'Ward 6', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(115, 'Ward 7', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(116, 'Ward 8', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(117, 'Ward 9', 18, 6, 18, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(118, 'Ward 1', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(119, 'Ward 2', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(120, 'Ward 3', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(121, 'Ward 4', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(122, 'Ward 5', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(123, 'Ward 6', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(124, 'Ward 7', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(125, 'Ward 8', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(126, 'Ward 9', 18, 6, 19, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(127, 'Ward 1', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(128, 'Ward 2', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(129, 'Ward 3', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(130, 'Ward 4', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(131, 'Ward 5', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(132, 'Ward 6', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(133, 'Ward 7', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(134, 'Ward 8', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(135, 'Ward 9', 18, 6, 20, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(136, 'Ward 1', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(137, 'Ward 2', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(138, 'Ward 3', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(139, 'Ward 4', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(140, 'Ward 5', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(141, 'Ward 6', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(142, 'Ward 7', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(143, 'Ward 8', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(144, 'Ward 9', 18, 6, 21, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(145, 'Ward 1', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(146, 'Ward 2', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(147, 'Ward 3', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(148, 'Ward 4', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(149, 'Ward 5', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(150, 'Ward 6', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(151, 'Ward 7', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(152, 'Ward 8', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(153, 'Ward 9', 18, 6, 22, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(154, 'Ward 1', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(155, 'Ward 2', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(156, 'Ward 3', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(157, 'Ward 4', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(158, 'Ward 5', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(159, 'Ward 6', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(160, 'Ward 7', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(161, 'Ward 8', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(162, 'Ward 9', 18, 6, 23, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(163, 'Ward 1', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(164, 'Ward 2', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(165, 'Ward 3', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(166, 'Ward 4', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(167, 'Ward 5', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(168, 'Ward 6', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(169, 'Ward 7', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(170, 'Ward 8', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(171, 'Ward 9', 18, 6, 24, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(172, 'Ward 1', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(173, 'Ward 2', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(174, 'Ward 3', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(175, 'Ward 4', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(176, 'Ward 5', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(177, 'Ward 6', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(178, 'Ward 7', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(179, 'Ward 8', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(180, 'Ward 9', 18, 6, 25, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(181, 'Ward 1', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(182, 'Ward 2', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(183, 'Ward 3', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(184, 'Ward 4', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(185, 'Ward 5', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(186, 'Ward 6', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(187, 'Ward 7', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(188, 'Ward 8', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00'),
(189, 'Ward 9', 18, 6, 26, '2018-10-05 18:00:00', '2018-10-05 18:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bol_election_agent`
--
ALTER TABLE `bol_election_agent`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_designations`
--
ALTER TABLE `bol_election_designations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_districts`
--
ALTER TABLE `bol_election_districts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_election_area`
--
ALTER TABLE `bol_election_election_area`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_election_center`
--
ALTER TABLE `bol_election_election_center`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_organizations`
--
ALTER TABLE `bol_election_organizations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_people`
--
ALTER TABLE `bol_election_people`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_people_groups`
--
ALTER TABLE `bol_election_people_groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_union`
--
ALTER TABLE `bol_election_union`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_upazila`
--
ALTER TABLE `bol_election_upazila`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_village`
--
ALTER TABLE `bol_election_village`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `bol_election_ward`
--
ALTER TABLE `bol_election_ward`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bol_election_agent`
--
ALTER TABLE `bol_election_agent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bol_election_designations`
--
ALTER TABLE `bol_election_designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `bol_election_districts`
--
ALTER TABLE `bol_election_districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `bol_election_election_area`
--
ALTER TABLE `bol_election_election_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bol_election_election_center`
--
ALTER TABLE `bol_election_election_center`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;
--
-- AUTO_INCREMENT for table `bol_election_organizations`
--
ALTER TABLE `bol_election_organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bol_election_people`
--
ALTER TABLE `bol_election_people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `bol_election_people_groups`
--
ALTER TABLE `bol_election_people_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `bol_election_union`
--
ALTER TABLE `bol_election_union`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `bol_election_upazila`
--
ALTER TABLE `bol_election_upazila`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bol_election_village`
--
ALTER TABLE `bol_election_village`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=389;
--
-- AUTO_INCREMENT for table `bol_election_ward`
--
ALTER TABLE `bol_election_ward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
