-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 06, 2018 at 02:12 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `electionportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `bol_election_people_groups`
--

CREATE TABLE `bol_election_people_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `bol_election_people_groups`
--

INSERT INTO `bol_election_people_groups` (`id`, `name`, `sort_order`, `created_at`, `updated_at`, `parent_id`, `nest_left`, `nest_right`, `nest_depth`) VALUES
(4, 'Government', 0, '2018-10-04 13:41:50', '2018-10-04 13:48:40', 0, 1, 12, 0),
(5, 'Administration', 0, '2018-10-04 13:42:04', '2018-10-04 13:45:07', 4, 2, 3, 1),
(6, 'Law Enforcement', 0, '2018-10-04 13:42:11', '2018-10-04 13:48:36', 4, 4, 11, 1),
(7, 'Police', 0, '2018-10-04 13:43:49', '2018-10-04 13:45:41', 6, 5, 6, 2),
(8, 'RAB', 0, '2018-10-04 13:43:59', '2018-10-04 13:45:58', 6, 7, 8, 2),
(9, 'Ansar & VDP', 0, '2018-10-04 13:44:08', '2018-10-04 13:48:36', 6, 9, 10, 2),
(10, 'Local Government', 0, '2018-10-04 13:44:16', '2018-10-04 13:48:40', 0, 13, 14, 0),
(11, 'Political Party', 0, '2018-10-04 13:44:27', '2018-10-04 13:44:27', 0, 15, 16, 0),
(12, 'Important People', 0, '2018-10-04 13:44:35', '2018-10-04 13:44:35', 0, 17, 18, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bol_election_people_groups`
--
ALTER TABLE `bol_election_people_groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bol_election_people_groups`
--
ALTER TABLE `bol_election_people_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
