$(document).ready(function() {
    var html = "";
    var count = 0;
    setInterval(function(){ 
        $.request('onDisplayTimer', {
            success:function(data){
                html = "";
                count++;
                console.log(count);
                if(count == 100){
                    location.reload();
                }
                if(data.display == 1){
                    html += "<tr><td>Name</td><td>"+data.name+"</td></tr>";    
                    html += "<tr><td>phone</td><td>"+data.phone+"</td></tr>"; 
                    if(data.email != ""){
                        html += "<tr><td>Email</td><td>"+data.email+"</td></tr>"; 
                    }

                    if(data.organization != ""){
                        html += "<tr><td>Organization</td><td>"+data.organization+"</td></tr>"; 
                    }

                    if(data.department != ""){
                        html += "<tr><td>Department</td><td>"+data.department+"</td></tr>"; 
                    }

                    if(data.contact_person != ""){
                        html += "<tr><td>Contact</td><td>"+data.contact_person+"</td></tr>"; 
                    }
                    html += "<tr><td>Purpose</td><td>"+data.purpose+"</td></tr>"; 
                    html += "<tr><td>Visitor address</td><td>"+data.visitor_address+"</td></tr>"; 
                    html += "<tr><td>Visitor Card Number</td><td>"+data.visitor_card_number+"</td></tr>";
                
                    $('.tbody-class').html(html);
                    $('.details').show();
                }else{
                    $('.tbody-class').html("");
                    $('.details').hide();
                }
            }
        })
    }, 1000);
});