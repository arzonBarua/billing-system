$(document).on('click','.submit',function(e){
    e.preventDefault();
    $('.loading-show').show();
    var value       = $('.date_range').val();
    var project_id  = $('.custom-select').val();
    console.log(project_id);
    $.request('onSubmit', {
        data: {value: value, project_id: project_id},
        success: function(data) {
          $('#myId').html(data.results);
          $('.loading-show').hide();
        }
        // error:function() {
        //   $('.loading-show').hide();
        // }
    })
});

$(function() {
    $('input[name="datefilter"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });
  
    $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });
  
    $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('.swtich').on('click',function(){
        var value = $(this).val();
        // swal("Hello world!");
        // if(confirm("Are you sure you want to display this?")){
        //     $.request('onDisplay', {data: {value: value}})
        // }else{
        //     return false;
        // }
        swal({
            title: "Are you sure what to display visitor information?",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
        },
            function(isConfirm) {
                if (isConfirm) {
                    $.request('onDisplay', {data: {value: value}})
                } else {
                    location.reload();
                }
            }
        );
    });
    
    $('.swtich-nothing').on('click',function(){
        return false;
    })
});


