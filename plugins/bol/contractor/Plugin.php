<?php namespace BOL\Contractor;

use Event;
use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public $require = [];

    public function registerComponents()
    {
        return [
            'BOL\Contractor\Components\BolContractorRegistrationForm'  => 'ContractorRegistration',
            'BOL\Contractor\Components\BolContractorWelcome' => 'BolContractorWelcome'
        ];
    }

    public function registerSettings()
    {
        return [];
    }

    public function registerFormWidgets()
    {
        return [
           // 'BOL\Contractor\FormWidgets\VariantSelector' => 'variantselector',
        ];
    }

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {
            //(new DiscountApi())->updateDiscountUsages();
        })->hourly();
    }

    public function boot()
    {
        $this->registerWebhookEvents();
        $this->registerStaticPagesEvents();
    }

    protected function registerWebhookEvents()
    {

    }

    protected function registerStaticPagesEvents()
    {
        
    }
}