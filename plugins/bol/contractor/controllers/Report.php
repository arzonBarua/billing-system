<?php namespace BOL\Contractor\Controllers;

use Backend;
use BackendMenu;
use BackendAuth;
use Request;
use Validator;
use ValidationException;
use Mail;
use Db;
use Flash;
use Backend\Classes\Controller;
use BOL\Contractor\Models\ContractorList;

class Report extends Controller{
    public $requiredPermissions = ['bol.contractor.manage_contractor_report'];

    public function __construct(){
        parent::__construct();
        BackendMenu::setContext('BOL.Contractor', 'bol-contractor', 'contractor-report');
    }

    public function index(){
        $this->pageTitle = 'Report';
        $this->addCss('/plugins/bol/contractor/assets/css/daterangepicker.css?time='.time());
        $this->addJs('/plugins/bol/contractor/assets/js/daterangepicker.min.js?time='.time());
        $this->addJs('/plugins/bol/contractor/assets/js/custom.js?time='.time());
        $this->addJs('/plugins/bol/contractor/assets/js/moment.min.js?time='.time());

        $contractorList = new ContractorList();

        $options = [];
        $projectNames = $contractorList->orderBy('project_number','ASC')->get();
        $projectNames->each(function ($projectName) use (&$options) {
            $options[$projectName->id] = $projectName->project_name;
        });

        $this->vars['options'] = $options;
       
        return $this->makePartial('report');
    }

    function onSubmit(){

        $rules = ['value'=>'required'];
        $validation = Validator::make(post(), $rules);
        if ($validation->fails()) {
            $validation = ['#layout-flash-messages' => 'Plase fill the date range field'];   
            throw new ValidationException($validation);
        }

        list($firstDate1, $lastDate2) = explode("-",post('value'));
        $firstDate                  = date("Y-m-d",strtotime($firstDate1));
        $lastDate                   = date("Y-m-d",strtotime($lastDate2));

        $project_id                 = post('project_id');

        $payment = new Payment();
        $paymentList = [];
        $billingList = [];
        
        $totalPayment = 0;
        $totalBilling = 0;

        if(empty($project_id)){
            $resultPayments = Db::table('bol_contractor_payments')
                            ->join('bol_contractor_lists', 'bol_contractor_payments.contractorlist_id', '=', 'bol_contractor_lists.id')
                            ->whereBetween('payment_date', [$firstDate, $lastDate])
                            ->select('bol_contractor_payments.*', 'bol_contractor_lists.project_name')
                         ->get();
        }else{
            $resultPayments = Db::table('bol_contractor_payments')
                            ->join('bol_contractor_lists', 'bol_contractor_payments.contractorlist_id', '=', 'bol_contractor_lists.id')
                            ->where('contractorlist_id',$project_id)
                            ->whereBetween('payment_date', [$firstDate, $lastDate])
                            ->select('bol_contractor_payments.*', 'bol_contractor_lists.project_name')
                         ->get();
        }

        foreach($resultPayments as $value){
            $paymentList[] = [
                'project_name'      =>$value->project_name,
                'payment_type'      =>$value->payment_type,
                'paid_amount'       =>$value->paid_amount,
                'payment_purpose'   =>$value->payment_purpose,
                'payment_date'      =>date('m/d/Y',strtotime($value->payment_date)) 
            ];
            $totalPayment += $value->paid_amount;
        }



        if(empty($project_id)){
            $resultBilling = Db::table('bol_contractor_bills')
                            ->join('bol_contractor_lists', 'bol_contractor_bills.contractorlist_id', '=', 'bol_contractor_lists.id')
                            ->whereBetween('bill_date', [$firstDate, $lastDate])
                            ->select('bol_contractor_bills.*', 'bol_contractor_lists.project_name')
                         ->get();
        }else{
            $resultBilling = Db::table('bol_contractor_bills')
                            ->join('bol_contractor_lists', 'bol_contractor_bills.contractorlist_id', '=', 'bol_contractor_lists.id')
                            ->where('contractorlist_id',$project_id)
                            ->whereBetween('bill_date', [$firstDate, $lastDate])
                            ->select('bol_contractor_bills.*', 'bol_contractor_lists.project_name')
                         ->get();
        }

        foreach($resultBilling as $value){
            $billingList[] = [
                'project_name'      =>$value->project_name,
                'bill_amount'       =>$value->bill_amount,
                'bill_date'         =>date('m/d/Y',strtotime($value->bill_date)) 
            ];
            $totalBilling += $value->bill_amount;
        }

        $this->vars['paymentList']      = $paymentList;
        $this->vars['billingList']      = $billingList;
        $this->vars['totalDifference']  = $totalBilling - $totalPayment;

        $this->vars['totalBilling']     = $totalBilling;
        $this->vars['totalPayment']     = $totalPayment;

        $this->vars['from_date']        = $firstDate1;
        $this->vars['end_date']         = $lastDate2;
    
        return [
            'results' => $this->makePartial('ajaxtable')
        ];
    }
}