<?php namespace BOL\Contractor\Controllers;

use Backend;
use BackendMenu;
use BackendAuth;
use Request;
use Mail;
use Db;
use Flash;
use Backend\Classes\Controller;


class Payment extends Controller{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['bol.contractor.manage_contractor_payment'];

    protected $user;

    public function __construct(){
        parent::__construct();
        BackendMenu::setContext('BOL.Contractor', 'bol-contractor', 'contractor-payment');
    }

    public function index(){
        parent::index();
    }

   

}