<?php namespace BOL\Contractor\Controllers;

use Backend;
use BackendMenu;
use BackendAuth;
use Request;
use Validator;
use ValidationException;
use Mail;
use Db;
use Flash;
use Backend\Classes\Controller;

class MisellaneousReport extends Controller{
    public $requiredPermissions = ['bol.contractor.manage_misellaneous_report'];

    public function __construct(){
        parent::__construct();
        BackendMenu::setContext('BOL.Contractor', 'bol-contractor', 'contractor-misellaneous-report');
    }

    public function index(){
        $this->pageTitle = 'Misellaneous Report';
        $this->addCss('/plugins/bol/contractor/assets/css/daterangepicker.css?time='.time());

        $this->addJs('/plugins/bol/contractor/assets/js/custom.js?time='.time());
        $this->addJs('/plugins/bol/contractor/assets/js/moment.min.js?time='.time());
        $this->addJs('/plugins/bol/contractor/assets/js/daterangepicker.min.js?time='.time());
       
        return $this->makePartial('report');
    }

    function onSubmit(){
        $rules = ['value'=>'required'];
        $validation = Validator::make(post(), $rules);
        if ($validation->fails()) {
            $validation = ['#layout-flash-messages' => 'Plase fill the date range field'];   
            throw new ValidationException($validation);
        }

        list($firstDate1, $lastDate2) =  explode("-",post('value'));
        $firstDate                  = date("Y-m-d",strtotime($firstDate1));
        $lastDate                   = date("Y-m-d",strtotime($lastDate2));

        $payment = new Payment();
        $totalList = [];       
        $total = 0;
      
        $resultPayments = Db::table('bol_contractor_payments')
                            ->join('bol_contractor_lists', 'bol_contractor_payments.contractorlist_id', '=', 'bol_contractor_lists.id')
                            ->whereBetween('payment_date', [$firstDate, $lastDate])
                            ->select('bol_contractor_payments.*', 'bol_contractor_lists.project_name')
                         ->get();

        foreach($resultPayments as $value){
            $totalList[] = [
                'project_name'      =>$value->project_name,
                'amount'            =>$value->paid_amount,
                'date'      =>date('m/d/Y',strtotime($value->payment_date))
            ];
            $total += $value->paid_amount;
        }

        $resultBilling = Db::table('bol_contractor_miscellaneous')
                            ->join('bol_contractor_miscellaneous_type', 'bol_contractor_miscellaneous.miscellaneoustype_id', '=', 'bol_contractor_miscellaneous_type.id')
                            ->whereBetween('expenses_date', [$firstDate, $lastDate])
                            ->select('bol_contractor_miscellaneous.*', 'bol_contractor_miscellaneous_type.name')
                         ->get();

        foreach($resultBilling as $value){
            $totalList[] = [
                'project_name'      =>$value->name,
                'amount'            =>$value->amount,
                'date'              =>date('m/d/Y',strtotime($value->expenses_date)) 
            ];
            $total += $value->amount;
        }

        $this->vars['total']            = $total;
        $this->vars['totalList']        = $totalList;
        $this->vars['from_date']        = $firstDate1;
        $this->vars['end_date']         = $lastDate2;
       
        return [
            'results' => $this->makePartial('ajaxtable')
        ];
    }
}