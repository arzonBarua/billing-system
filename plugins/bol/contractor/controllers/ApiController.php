<?php namespace BOL\Contractor\Controllers;

use Mail;
use Response;
use Validator;
use ValidationException;

use Backend\Classes\Controller;

use BOL\Contractor\Models\Employee;
use BOL\Contractor\Models\Department;
use BOL\Contractor\Models\Designation;
use BOL\Contractor\Models\NotificationReceiver;


/**
 * 200: OK. The standard success code and default option.
 * 201: Object created. Useful for the store actions.
 * 204: No content. When an action was executed successfully, but there is no content to return.
 * 206: Partial content. Useful when you have to return a paginated list of resources.
 * 400: Bad request. The standard option for requests that fail to pass validation.
 * 401: Unauthorized. The user needs to be authenticated.
 * 403: Forbidden. The user is authenticated, but does not have the permissions to perform an action.
 * 404: Not found. This will be returned automatically by Laravel when the resource is not found.
 * 500: Internal server error. Ideally you're not going to be explicitly returning this, but if something unexpected breaks, this is what your user is going to receive.
 * 503: Service unavailable. Pretty self explanatory, but also another code that is not going to be returned explicitly by the application.
 */

class ApiController extends Controller
{
    public $employee;
    public $department;
    public $designation;
    public $receiver;

    public function __construct()
    {
        parent::__construct();
        // echo 'ok';
        // die;

        $this->employee = new Employee();
        $this->department = new Department();
        $this->designation = new Designation();
        $this->receiver = new NotificationReceiver();
    }

    public function apiCheckEmail()
    {
        $post = post();
        $rules = [
            'email' => 'required|email|between:6,255',
        ];
        
        $validation = Validator::make($post, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }

        $data = $this->employee->where('email', $post['email'])->first();

        // var_dump($post['email']);die;
        $email = $post['email'];
        if(!$data)
        {
            Mail::send('backend::mail.pricing', [], function ($message) use ($email) {
                $message->to($email, 'GUEST');
            });
    
            return Response::json(["status" => "ok", "data" => $data, "msg" => "You are not registered user of this system. Check your email for details."]);
        }

        $otp = rand(111111,999999);
        $this->employee->where('email', $post['email'])->update(['otp' => $otp]);
        $mail_data = ['otp' => $otp, 'employee_name' => $data->employee_name];
        //send email details
        Mail::send('backend::mail.otp', $mail_data, function ($message) use ($data) {
            $message->to($data->email, $data->employee_name);
        });
        return Response::json(["status" => "ok", "data" => $data, "msg" => "An OTP has been sent to your email ".$post['email']]);

        
    }

    public function apiUserLogin()
    {
        $post = post();
        $rules = [
            'email' => 'required|email|between:6,255',
            'otp' => 'required|max:6',
        ];
        
        $validation = Validator::make($post, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }

        $data = $this->employee->where(['email' => $post['email'], 'otp' => $post['otp']])->first();

        $data = [
            "id" => $data->id,
            "employee_name" => $data->employee_name,
            "employee_image" => ($data->employee_image) ? $data->employee_image->getThumb(350, 350, ['mode' => 'crop']) : false,
            "company_id" => $data->company_id,
            "company_name" =>  $data->company->company_name,
            "company_image" => ($data->company->company_logo) ? $data->company->company_logo->getThumb(350, 350, ['mode' => 'crop']) : false,
            "department_id" => $data->department_id,
            "otp" => $data->otp,
            "department_name" =>  $data->department->department_name,
            "designation_id" => $data->designation_id,
            "designation_name" =>  $data->designation->designation_name,
            "date_of_birth" => $data->date_of_birth,
            "blood_group" => $data->blood_group,
            "address" => $data->address,
            "email" => $data->email,
            "phones" => $data->phones,
            "extension" => $data->extension,
            "created_at" => $data->created_at,
            "updated_at" => $data->updated_at,
        ];

        if($data)
        {
            //send email details
            return Response::json(["status" => "ok", "data" => $data, "msg" => "You are successfully logged in."]);
        }

        return Response::json(["status" => "ok", "data" => $data, "msg" => "Wrong Email address or OTP."]);
    }

    public function apiLoadContacts()
    {
        $get = get();
        $rules = [
            'id' => 'required',
            'company_id' => 'required',
            'otp' => 'required',
        ];
        
        $validation = Validator::make($get, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }
        
        $data = $this->employee->where( $get )->first();
        
        if (!$data)
        { 
            return Response::json(["status" => "error", "msg" => "Invalid data request."]);
        }
        
        $data = $this->employee->where( 'company_id', $data->company_id )->get();

        $contacts = [];
        foreach($data as $row){
            $contacts[] = [
                "id" => $row->id,
                "employee_name" => $row->employee_name,
                "employee_image" => ($row->employee_image) ? $row->employee_image->getThumb(350, 350, ['mode' => 'crop']) : false,
                "company_id" => $row->company_id,
                "company_name" =>  $row->company->company_name,
                "department_id" => $row->department_id,
                "department_name" =>  $row->department->department_name,
                "designation_id" => $row->designation_id,
                "designation_name" =>  $row->designation->designation_name,
                "date_of_birth" => $row->date_of_birth,
                "blood_group" => $row->blood_group,
                "address" => $row->address,
                "email" => $row->email,
                "phones" => $row->phones,
                "extension" => $row->extension,
                "created_at" => $row->created_at,
                "updated_at" => $row->updated_at,
            ];
        }

        return Response::json(["status" => "ok", "data" => $contacts, "msg" => count($contacts)." Data Found."]);
    }

    public function apiDepartmentContacts()
    {
        $get = get();
        $rules = [
            'id' => 'required',
            'company_id' => 'required',
            'otp' => 'required',
        ];
        
        $validation = Validator::make($get, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }
        
        $data = $this->employee->where( $get )->first();
        
        if (!$data)
        { 
            return Response::json(["status" => "error", "msg" => "Invalid data request."]);
        }
        
        $data = $this->department->where( 'company_id', $data->company_id )->get();

        // var_dump($data); die;

        $contacts = [];
        foreach($data as $row1)
        {
            $employees = [];
            foreach($row1->employee()->get() as $row)
            {
                $employees[] = [
                                "id" => $row->id,
                                "employee_name" => $row->employee_name,
                                "employee_image" => ($row->employee_image) ? $row->employee_image->getThumb(350, 350, ['mode' => 'crop']) : false,
                                "company_id" => $row->company_id,
                                "company_name" =>  $row->company->company_name,
                                "department_id" => $row->department_id,
                                "department_name" =>  $row->department->department_name,
                                "designation_id" => $row->designation_id,
                                "designation_name" =>  $row->designation->designation_name,
                                "date_of_birth" => $row->date_of_birth,
                                "blood_group" => $row->blood_group,
                                "address" => $row->address,
                                "email" => $row->email,
                                "phones" => $row->phones,
                                "extension" => $row->extension,
                                "created_at" => $row->created_at,
                                "updated_at" => $row->updated_at,
                            ];
            }

            $contacts[] = [
                'department_name' => $row1->department_name,
                'employees' => $employees,
                'count' => count($employees)
            ];
        }

        return Response::json(["status" => "ok", "data" => $contacts, "msg" => count($contacts)." Data Found."]);
    }


    public function apiLoadDepartments()
    {
        $get = get();
        $rules = [
            'id' => 'required',
            'company_id' => 'required',
            'otp' => 'required',
        ];
        
        $validation = Validator::make($get, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }
        
        $data = $this->employee->where( $get )->first();
        
        if (!$data)
        { 
            return Response::json(["status" => "error", "msg" => "Invalid data request."]);
        }
        
        $data = $this->department->select('department_name')
                                    ->where( 'company_id', $data->company_id )
                                        ->get()->toArray();
        
        $data = array_column($data, 'department_name');

        return Response::json(["status" => "ok", "data" => $data, "msg" => count($data)." Data Found."]);
    }

    public function apiLoadDesignations()
    {
        $get = get();
        $rules = [
            'id' => 'required',
            'company_id' => 'required',
            'otp' => 'required',
        ];
        
        $validation = Validator::make($get, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }
        
        $data = $this->employee->where( $get )->first();
        
        if (!$data)
        { 
            return Response::json(["status" => "error", "msg" => "Invalid data request."]);
        }
        
        $data = $this->designation->select('designation_name')->where( 'company_id', $data->company_id )->orderBy('sort_order', 'asc')->get()->toArray();
        
        $data = array_column($data, 'designation_name');

        return Response::json(["status" => "ok", "data" => $data, "msg" => count($data)." Data Found."]);
    }

    public function apiLoadBloodGroups()
    {
        $get = get();
        $rules = [
            'id' => 'required',
            'company_id' => 'required',
            'otp' => 'required',
        ];
        
        $validation = Validator::make($get, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }
        
        $data = $this->employee->where( $get )->first();
        
        if (!$data)
        { 
            return Response::json(["status" => "error", "msg" => "Invalid data request."]);
        }
        
        $data = $this->employee->select('blood_group')->distinct()->where( 'company_id', $data->company_id )->orderBy('blood_group', 'asc')->get()->toArray();
        
        $data = array_column($data, 'blood_group');

        return Response::json(["status" => "ok", "data" => $data, "msg" => count($data)." Data Found."]);
    }

    public function apiLoadFilters()
    {
        $get = get();
        $rules = [
            'id' => 'required',
            'company_id' => 'required',
            'otp' => 'required',
        ];
        
        $validation = Validator::make($get, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }
        
        $employee = $this->employee->where( $get )->first();
        
        if (!$employee)
        { 
            return Response::json(["status" => "error", "msg" => "Invalid data request."]);
        }

        $data = $this->department->select('department_name')->where( 'company_id', $employee->company_id )->get()->toArray();
        $departments = array_column($data, 'department_name');
        
        $data = $this->designation->select('designation_name')->where( 'company_id', $employee->company_id )->orderBy('sort_order', 'asc')->get()->toArray();
        $designations = array_column($data, 'designation_name');

        $data = $this->employee->select('blood_group')->distinct()->where( 'company_id', $employee->company_id )->orderBy('blood_group', 'asc')->get()->toArray();
        $blood_groups = array_column($data, 'blood_group');


        $data = [
            'department' => $departments,
            'designation' => $designations,
            'blood_group' => $blood_groups
        ];

        return Response::json(["status" => "ok", "data" => $data, "msg" => count($data)." Data Found."]);
    }


    public function apiLoadNotifications()
    {
        $get = get();
        $rules = [
            'user_id' => 'required',
        ];
        
        $validation = Validator::make($get, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }
        
        $receiver = $this->receiver->where( $get );
        
        
        if (!$receiver->count())
        { 
            return Response::json(["status" => "error", "msg" => "Invalid data request."]);
        }

        $notifications = [];
        
        foreach($receiver->orderBy('created_at', 'desc')->get() as $data)
        {
            $notifications[] = [
                "id" => $data->id,
                "title" => $data->notification->title,
                "message" => $data->notification->message,
                "seen" => $data->seen,
                "created_at" => $data->created_at,
                "updated_at" => $data->updated_at,
            ];
        }

        return Response::json(["status" => "ok", "data" => $notifications, "msg" => count($notifications)." Data Found."]);
    }



    public function apiNotificationUnreadCount()
    {
        $get = get();
        $rules = [
            'user_id' => 'required',
        ];
        
        $validation = Validator::make($get, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }
        
        $count = $this->receiver->where( $get )
                                ->where('seen', 0)
                                ->count();
        

        return Response::json(["status" => "ok", "data" => $count, "msg" => $count." Unread Message."]);
    }

    public function apiMakeNotificationRead()
    {
        $get = get();
        $rules = [
            'user_id' => 'required',
            'id' => 'required',
        ];
        
        $validation = Validator::make($get, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }

        $data = $this->receiver->where($get)->update(['seen' => 1]);

        return Response::json(["status" => "ok", "data" => $data, "msg" => "Message Seen Successfully."]);
    }   

    public function apiDeleteNotification()
    {
        $get = get();
        $rules = [
            'user_id' => 'required',
            'id' => 'required',
        ];
        
        $validation = Validator::make($get, $rules);
        
        if ($validation->fails())
        { 
            return Response::json(["status" => "error", "msg" => $validation->messages()->all()]);
        }

        $data = $this->receiver->where($get)->delete();

        return Response::json(["status" => "ok", "data" => $data, "msg" => "Message Delete Successfully."]);
    }

    public function marge_errors($array) { 
        if (!is_array($array)) { 
          return FALSE; 
        } 
        $result = array(); 
        foreach ($array as $key => $value) { 
          if (is_array($value)) { 
            $result = array_merge($result, $this->marge_errors($value)); 
          } 
          else { 
            $result[$key] = $value; 
          } 
        } 
        return $result; 
    }
}