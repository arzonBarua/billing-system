<?php namespace BOL\Contractor\Controllers;

use Backend;
use BackendMenu;
use BackendAuth;
use Request;
use Mail;
use Db;
use Flash;
use Backend\Classes\Controller;

class Misellaneous extends Controller{

    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['bol.contractor.manage_contractor_misellaneous'];

    protected $user;

    public function __construct(){
        parent::__construct();
        BackendMenu::setContext('BOL.Contractor', 'bol-contractor', 'contractor-payment-misellaneous');
    }

    public function index(){
        parent::index();
    }

}