<?php namespace BOL\Contractor\Controllers;

use Backend;
use BackendMenu;
use BackendAuth;
use Request;
use Mail;
use Db;
use Flash;
use Backend\Classes\Controller;

class ContractorList extends Controller{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['bol.contractor.manage_contractor_list'];

    protected $user;

    public function __construct(){
        parent::__construct();
        BackendMenu::setContext('BOL.Contractor', 'bol-contractor', 'contractor-list');
    }

    public function index(){
        parent::index();
        $this->addCss('/plugins/bol/contractor/assets/css/daterangepicker.css?time='.time());
        $this->addJs('/plugins/bol/contractor/assets/js/daterangepicker.min.js?time='.time());
        $this->addJs('/plugins/bol/contractor/assets/js/custom.js?time='.time());
    }
    
    public function onDisplay(){
        $toggle = 1;
        $value = input('value');
        $request = Db::table('bol_contractor_lists')->where('id',$value)->first();

        if($request->status == 1){
            $toggle = 0;
        }

        
        
        Db::table('bol_contractor_lists')->where('id',$value)->update([
            'status' => $toggle,
        ]);
        //return $this->listRefresh();

        
        return Backend::redirect('bol/contractor/contractorlist/');
    }
}