<?php namespace BOL\Contractor\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Db;
use Flash;
use Backend;
use Redirect;
use Validator;
use BackendAuth;
use ValidationException;
use Input;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use BOL\Contractor\Models\Guest;

class BolContractorRegistrationForm extends ComponentBase
{
    
    public $employees;

    public function componentDetails()
    {
        return [
            'name'        => 'Contractor Registration',
            'description' => 'Contractor admin panel registration.',
        ];
    }

    public function defineProperties(){
        return [];
    }

    public function onRun(){
       
    }

    public function onDisplayTimer(){
        $result = Db::table('bol_reception_guests')->where('display',1)->first();
        $data['display']                = !empty($result->name) ? 1 : 0;
        
        if($data['display']){ 
            $data['name']                   = $result->name;
            $data['phone']                  = $result->phone;
            $data['email']                  = $result->email;
            $data['organization']           = $result->organization;
            $data['visitor_address']        = $result->visitor_address;
            $data['visitor_card_number']    = $result->visitor_card_number;
            $data['contact_person']         = !empty($result->contact_person) ? $result->contact_person : "";
            $data['department']             = !empty($result->department) ? $result->department : "";
            $data['purpose']                = ($result->purpose == "others") ? $result->others : $result->purpose;
        }

        return $data;
    }
}