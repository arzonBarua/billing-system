<?php namespace BOL\Contractor\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Db;
use Flash;
use Backend;
use Redirect;
use Validator;
use BackendAuth;
use ValidationException;
use Input;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use BOL\Contractor\Models\Guest;

class _BolContractorRegistrationForm extends ComponentBase
{
    
    public $employees;

    public function componentDetails()
    {
        return [
            'name'        => 'Contractor Registration',
            'description' => 'Contractor admin panel registration.',
        ];
    }

    public function defineProperties()
    {

        return [];
    }

    public function onRun(){
        $obj = new Guest;
        $this->page['employees']   = $obj->getContactPersonOptions();
        $this->page['departments'] = $obj->getDepartmentId();

    }

    public function onAdminRegistration(){
        /*
            * Validate input
            */
            
        $data = post();
        $customMessage = 0;

       
        if (!array_key_exists('password_confirmation', $data)) {
            $data['password_confirmation'] = post('password');
        }

        $rules = [
            'full_name'      => 'required',
            'mobile'         => 'required|regex:/(01)[0-9]{9}/',
            'organization'   => 'required',
            'visitor_number' => 'required',
            'purpose'        => 'required',
            'company'        => 'required'
        ];

        // if($data['purpose'] == "Meeting" || $data['purpose'] == "Personal"){
        //     $rules['select_employee'] = 'required';
        // }

        if($data['purpose'] == "Others"){
            $rules['other_purpose'] = 'required';
        }

        if(isset($data['has_companion'])){
            $rules['visitor_count'] = 'required';
        }

        if(empty($data['department']) && empty($data['select_employee'])){
            $rules['select_employee'] = 'required';
            $rules['department'] = 'required';
            $customMessage = 1;
        }

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            if($customMessage){
                $validation = ['#layout-flash-messages' => 'Please either employee or department'];
            }
            throw new ValidationException($validation);
        }

        $registration = [
            'name'              => $data['full_name'],
            'phone'             => $data['mobile'],
            'organization'      => $data['organization'],
            'purpose'           => $data['purpose'],
            'contact_person'    => $data['select_employee'],
            'department'        => $data['department'],
            'visitor_number'    => $data['visitor_number'],
            'others'            => $data['other_purpose'],
            'company'           => $data['company'],
            'visitor_count'     => isset($data['visitor_count']) ? $data['visitor_count'] : "",
            'in_time'           => date('Y-m-d H:i:s'),
            'created_at'        => date('Y-m-d H:i:s'),
            'status'            => 0
        ];

        Db::table('bol_reception_guests')->insert($registration);

        Flash::success('Your are successfully Registared. Please login with ');
        
        return Backend::redirect('../welcome');
    }

}