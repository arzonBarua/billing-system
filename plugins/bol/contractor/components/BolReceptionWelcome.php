<?php namespace BOL\Contractor\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Db;
use Flash;
use Backend;
use Redirect;
use Validator;
use BackendAuth;
use ValidationException;
use Input;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use BOL\Contractor\Models\Guest;

class BolContractorWelcome extends ComponentBase{
    
    public function componentDetails(){
        return [
            'name'        => 'Welcome',
            'description' => 'Welcome Page',
        ];
    }

    public function defineProperties(){
        return [];
    }

    public function onRun(){
        $res = Db::table('bol_reception_guests')->orderBy('id','desc')->first();

        $this->page['name']             = $res->name;
        $this->page['phone']            = $res->phone;
        $this->page['visitor_number']   = $res->visitor_number;

        if(!empty($res->contact_person)){
            $this->page['contact_person']  = $res->contact_person;
        }

        if(!empty($res->department)){
            $this->page['department']  = $res->department;
        }

        if(!empty($res->visitor_count)){
            $this->page['visitor_count']  = $res->visitor_count;
        }

        if($res->purpose){
            $this->page['contact_person']   = $res->contact_person;
        }
        
        $this->page['organization']     = $res->organization;
       
        if($res->purpose == "Others"){
            $res->purpose = $res->others;
        }

        $this->page['purpose']          = $res->purpose;
        $this->page['in_time']          = date('m/d/Y h:i a',strtotime($res->in_time));
    }

}