<?php namespace BOL\Contractor\FormWidgets;
use Input;
use Db;
use Backend\Classes\FormWidgetBase;
/**
 * VariantSelector Form Widget
 */
class SalutationList extends FormWidgetBase{
	
	private $htmlString;
	private $loadValue;

	public function render(){
		$this->loadValue = $this->getLoadValue();
		$this->getSalutationOptions();
		$this->vars['value']  = $this->htmlString;
		return $this->makePartial('salutation');
	}

	public function getSalutationOptions(){
		$array = ['Mr.','Ms.','Mrs.'];

		$this->htmlString  = "<select class='form-control' name='Guest[salutation]' id='Form-field-Guest-salutation'>";
		$this->htmlString .= "<option value=''>Select Title</option>";
		foreach($array as $values){
			$check = ($this->loadValue == $values) ? "selected" : "";
			$this->htmlString .= "<option value='".$values."' $check >".$values."</option>";
		}
		$this->htmlString .= "</select>";
	}
}


