<?php namespace BOL\Contractor\Models;

use InvalidArgumentException;
use Validator;
use ValidationException;
use Model;
use BackendAuth;
use Db;
use Mail;
use BOL\Contractor\Models\ContractorList;
/**
 * Model
 */
class Payment extends Model{

    use \October\Rain\Database\Traits\Validation;

    public $timestamps = true;
    public $table = 'bol_contractor_payments';
    public $translatable = [];
    public $implement = [];

    public $rules = [
        'contractorlist_id'      => 'required',
        'payment_type'           => 'required',
        'paid_amount'            => 'required',
        'payment_purpose'        => 'required',
        'payment_date'           => 'required'   
    ];

    public $belongsTo = ['contractorlist' => 'BOL\Contractor\Models\ContractorList',];

    public $belongsToMany = [];

    public $attachOne = [];

    public function getUser(){
        $user = BackendAuth::getUser();
        return $user->id;
    }

    public function getContractorListIdOptions(){
		$options = [
			null => 'Select Project Name',
		];

		$items = new ContractorList();

		$items->each(function ($item) use (&$options) {
			return $options[$item->id] = $item->project_name;
		});

		return $options;
	}

    public function beforeCreate(){
        $this->created_by = $this->getUser();
    }

    public function beforeUpdate(){
        $this->updated_by = $this->getUser();
    }

}