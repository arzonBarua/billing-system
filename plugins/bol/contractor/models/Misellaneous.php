<?php namespace BOL\Contractor\Models;

use InvalidArgumentException;
use Validator;
use ValidationException;
use Model;
use BackendAuth;
use Db;
use Mail;
use BOL\Contractor\Models\MisellaneousType;
/**
 * Model
 */
class Misellaneous extends Model{

    use \October\Rain\Database\Traits\Validation;

    public $timestamps = true;
    public $table = 'bol_contractor_miscellaneous';
    public $translatable = [];
    public $implement = [];

    public $rules = [
        'miscellaneoustype_id'      => 'required',
        'remarks'                   => 'required',
        'expenses_date'             => 'required'
    ];

    public $belongsTo = ['miscellaneoustype' => 'BOL\Contractor\Models\MisellaneousType'];

    public $belongsToMany = [];

    public $attachOne = [];

    public function getUser(){
        $user = BackendAuth::getUser();
        return $user->id;
    }

    public function getMiscellaneoustypeIdOptions(){
		$options = [
			null => 'Select Misellaneous Type',
		];

		$items = new MisellaneousType();

		$items->each(function ($item) use (&$options) {
			return $options[$item->id] = $item->name;
		});

		return $options;
	}

    public function beforeCreate(){
        $this->created_by = $this->getUser();
    }

    public function beforeUpdate(){
        $this->updated_by = $this->getUser();
    }

}