<?php namespace BOL\Contractor\Models;

use InvalidArgumentException;
use Validator;
use ValidationException;
use Model;
use BackendAuth;
use Db;
use Mail;
use BOL\Contractor\Models\SupplierType;
/**
 * Model
 */
class SupplierList extends Model{

    use \October\Rain\Database\Traits\Validation;

    public $timestamps = true;
    public $table = 'bol_supplier_list';
    public $translatable = [];
    public $implement = [];

    public $rules = [
        'supplier_name'              => 'required',
        'project_name'               => 'required',
        'project_number'             => 'required',
        'phone'                      => 'required|regex:/[0-9]{13}/|max:13',
        'project_status_date'        => 'required',
        'address'                    => 'required'
    ];

    public $belongsTo = ['suppliertype' => 'BOL\Contractor\Models\SupplierType'];

    public $belongsToMany = [];

    public $attachOne = [];

    public function getSupplierTypeIdOptions(){
        $options = [
            null => 'Select Supplier Type',
        ];

        $supplierType = new SupplierType();
        $items = $supplierType->orderBy('name','ASC')->get();
    
        $items->each(function ($item) use (&$options) {
            return $options[$item->id] = $item->name;
        });
        return $options;
    }

    public function getUser(){
        $user = BackendAuth::getUser();
        return $user->id;
    }

    public function beforeCreate(){
        $this->created_by = $this->getUser();
    }

    public function beforeUpdate(){
        $this->updated_by = $this->getUser();
    }

    public function beforeSave(){
        $this->status = 1;
    }
}