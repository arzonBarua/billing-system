<?php namespace BOL\Contractor\Models;

use InvalidArgumentException;
use Validator;
use ValidationException;
use Model;
use BackendAuth;
use Db;
use Mail;
use BOL\Contractor\Models\ContractorType;
/**
 * Model
 */
class ContractorList extends Model{

    use \October\Rain\Database\Traits\Validation;

    public $timestamps = true;
    public $table = 'bol_contractor_lists';
    public $translatable = [];
    public $implement = [];

    public $rules = [
        'name'                       => 'required',
        'project_name'               => 'required',
        'project_number'             => 'required',
        'phone'                      => 'required|regex:/[0-9]{13}/|max:13',
        'project_status_date'        => 'required',
        'address'                    => 'required'
    ];

    public $belongsTo = ['contractortype' => 'BOL\Contractor\Models\ContractorType'];

    public $belongsToMany = [];

    public $attachOne = [];

    public function getContractorTypeIdOptions(){
        $options = [
            null => 'Select Contractor Type',
        ];

        $contractortype = new ContractorType();
        $items = $contractortype->orderBy('name','ASC')->get();
    
        $items->each(function ($item) use (&$options) {
            return $options[$item->id] = $item->name;
        });
        return $options;
    }

    public function getUser(){
        $user = BackendAuth::getUser();
        return $user->id;
    }

    public function beforeCreate(){
        $this->created_by = $this->getUser();
    }

    public function beforeUpdate(){
        $this->updated_by = $this->getUser();
    }

    public function beforeSave(){
        $this->status = 1;
    }
}