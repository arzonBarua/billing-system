<?php namespace BOL\Contractor\Models;

use InvalidArgumentException;
use Validator;
use ValidationException;
use Model;
use BackendAuth;
use Db;
use Mail;
/**
 * Model
 */
class SupplierType extends Model{

    use \October\Rain\Database\Traits\Validation;

    public $timestamps = true;
    public $table = 'bol_supplier_type';
    public $translatable = [];
    public $implement = [];

    public $rules = [
        'name'      => 'required',
    ];

    public $belongsTo = [];

    public $belongsToMany = [];

    public $attachOne = [];

    public function getUser(){
        $user = BackendAuth::getUser();
        return $user->id;
    }


    public function beforeCreate(){
        $this->created_by = $this->getUser();
    }

    public function beforeUpdate(){
        $this->updated_by = $this->getUser();
    }

}