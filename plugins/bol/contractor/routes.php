<?php


Route::get('/', function(){
    return Backend::redirect('/');
});

Route::group([
    //'middleware' => ['web'],
    'prefix' => 'api'
], function () {
    Route::post('check-email', 'BOL\Contractor\Controllers\ApiController@apiCheckEmail');
    Route::post('user-login', 'BOL\Contractor\Controllers\ApiController@apiUserLogin');
    Route::get('change-desk-options', 'BOL\Contractor\Controllers\guest@changeDeskOptions');
});