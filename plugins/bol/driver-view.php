<?php ob_start();
//Set New value for the page options and if not need to set anything then just left the option blank or as it is
$pg_opt = array(
    "site_title" => "Reservation System",
    "page_title" => "Vehicle Information",
    "page_sub_title" => "",
    "page_parent" => "Vehicle Reservation",
    "page_parent_link" => "car-add.php",
    "page" => array("vehicle", "car-info", "car-view", "")
);

include_once "inc/head.php";
include_once "inc/topbar.php";
include_once "inc/menu.php";
include_once "inc/right-sidebar.php";
include_once "inc/db_con.php";
include_once "inc/functions.php";

// Delete And Status Change Operation
//if(empty($_SESSION['user_login']) || $_SESSION['user_login'] != "superAdmin"){
//    header("Location: login.php");
//}

$table_name = 'vehicle_info';

$current_page = end(explode("/", $_SERVER['PHP_SELF'])); // Get the current page
if (isset($_GET['id']) && $_GET['id'] != "") {
    $id = $_GET['id'];
    $action = $_GET['action'];

    // Delete
    if ($action == 'delete') {
        $_data = mysqli_fetch_object(mysqli_query($con, "SELECT * FROM $table_name WHERE `id` = '" . $id . "'"));
        mysqli_query($con, "DELETE FROM $table_name WHERE `id` = '" . $id . "'");
    } 
}
?>
    <div id="content" class="content">
<?php include_once "inc/breadcumb.php"; ?>
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                            class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                            class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                            class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">
                <?php echo $pg_opt['page_title']; ?>&nbsp;&nbsp;&nbsp;
                <a href="<?php echo $pg_opt['page_parent_link']; ?>" title="Add Publication"
                   class="btn btn-xs btn-icon btn-circle btn-info"><i class="fa fa-plus"></i></a>
            </h4>
        </div>
        <div class="panel-body">
            <table class="table table-bordered" id="data-table">
                <thead>
                <tr>
                    <th width="3%">Sl</th>
                    <th width="15%">Model Name</th>
                    <th width="15%">Registration Number</th>
                    <th width="10%">Vehicle Type</th>
                    <th width="10%">Enginee Number</th>
                    <th width="12%">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sl = 1;

                $sql = "SELECT vehicle_type.type_name, vehicle_info.* FROM vehicle_info LEFT JOIN vehicle_type on vehicle_info.vehicle_type_id = vehicle_type.id WHERE `location` = '$_SESSION[location]' ORDER BY vehicle_info.id ASC";
                $qry = mysqli_query($con, $sql);
                while ($data = mysqli_fetch_object($qry)) {
                    ?>
                    <tr>
                        <td><?php echo $sl++; ?></td>
                        <td><?php echo $data->model_name; ?></td>    
                        <td><?php echo $data->registration_number; ?></td>
                        <td><?php echo $data->type_name; ?></td>
                        <td><?php echo $data->eng_number; ?></td>
                        <td>
                            <a href="<?php echo $pg_opt['page_parent_link']; ?>?id=<?php echo $data->id; ?>"
                               class="btn btn-xs btn-icon btn-success" title="Edit"><i class="fa fa-pencil"></i></a> |
                            <a href="#view_<?php echo $data->id; ?>" data-toggle='modal'
                               class="btn btn-xs btn-icon btn-default" title="View"><i class="fa fa-eye"></i></a> |
                            <a href="?id=<?php echo $data->id; ?>&action=delete" class="btn btn-xs btn-icon btn-danger"
                               title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                            <div class="modal fade" id="<?php echo 'view_' . $data->id; ?>" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                ×
                                            </button>
                                            <h4 class="modal-title">Details of
                                                <b><?php echo $pg_opt['page_title'] ?></b></h4>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-bordered">
                                                 <tr>
                                                    <th width="20%">Model</th>
                                                    <td><?php echo $data->model_name; ?></td>
                                                </tr>    
                                                <tr>
                                                    <th width="20%">Registration Number</th>
                                                    <td><?php echo $data->registration_number; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Vehicle Type</th>
                                                    <td><?php echo $data->type_name; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Enginne  Number</th>
                                                    <td><?php echo $data->eng_number; ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    </div>
<?php
include_once "inc/foot.php";
?>