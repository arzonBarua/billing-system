<?php ob_start();
//Set New value for the page options and if not need to set anything then just left the option blank or as it is
$pg_opt = array(
    "site_title" => "Reservation System",
    "page_title" => "Add Vehicle Information",
    "page_sub_title" => "",
    "page_parent" => "Vehicle Reservation",
    "page_parent_link" => "car-view.php",
    "page" => array("vehicle", "car-info", "car-add", "")
);


include_once "inc/db_con.php";
include_once "inc/functions.php";

$table_name = 'vehicle_info';

// Fetch data from database
if (isset($_GET['id']) && $_GET['id'] != "") {
    $id = intval($_GET['id']);
    $_data = mysqli_fetch_object(mysqli_query($con, "SELECT * FROM $table_name WHERE `id` = '" . $id . "'"));
    // Set new value for the page options
    $pg_opt['page_title'] = 'Update information';
}

include_once "inc/head.php";
include_once "inc/topbar.php";
include_once "inc/menu.php";
include_once "inc/right-sidebar.php";


// Insertion or Updatation
$error = '';
if (isset($_POST['submit']) && $_POST['submit'] != "") {
    $values['location'] = $_SESSION['location'];
    $values['model_name'] = $_POST['model_name'];
    $values['registration_number'] = trim($_POST['registration_number']);
    $values['vehicle_type_id'] = trim($_POST['vehicle_type_id']);
    $values['eng_number'] = trim($_POST['eng_number']);

    /* Get the data if error held */
    $_data = (object)$values;
    $validate_result = validate($values, array("registration_number", "vehicle_type_id", "eng_number"));

    if ($validate_result['status'] == "success" && !$error) {
        $id = intval($_POST['id']);
        if (isset($id) && $id != "") {
            $values['modi_date'] = date("Y-m-d");
            $values['modi_by'] = $_SESSION['full_name'];
            $sql = updateQryStr($values, $table_name, 'id', $id, $con);
        } else {
            $values['entry_date'] = date("Y-m-d");
            $values['entry_by'] = $_SESSION['full_name'];
            $sql = insertQryStr($values, $table_name, $con);
        }
        $qry = mysqli_query($con, $sql) or die(mysqli_error($con));
        if ($qry) {
            echo "<META http-equiv='refresh' content='0;URL=" . $pg_opt['page_parent_link'] . "'>";
            exit;
        } else {
            $err = "Something missing..";
        }
    } else {
        $err = ($error != "") ? $error : $validate_result['error'];
    }

}
?>
    <div id="content" class="content">
        <?php include_once "inc/breadcumb.php"; ?>
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                                class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                                class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                       data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title"><?php echo $pg_opt['page_title']; ?></h4>
            </div>
            <div class="panel-body">

                <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Model Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="model_name" value="<?php echo $_data->model_name; ?>"
                                   class="form-control" placeholder="Enter Model Name" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Registration Number<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="registration_number" value="<?php echo $_data->registration_number; ?>"
                                   class="form-control" placeholder="Enter Registration Number" required/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">Vehicle Type<span class="required">*</span></label>
                        <div class="col-md-9">
                            <select class="form-control" name="vehicle_type_id">
                                <option value=""> - Select Type -</option>
                                <?php
                                $sql_fetch = "SELECT * FROM `vehicle_type` WHERE `status` = '1'";
                                $qry_fetch = mysqli_query($con, $sql_fetch);
                                while ($data_fetch = mysqli_fetch_object($qry_fetch)) {
                                    $sel = ($_data->vehicle_type_id == $data_fetch->id) ? 'selected="selected"' : '';
                                    echo '<option ' . $sel . ' value="' . $data_fetch->id . '">' . $data_fetch->type_name . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label class="col-md-3 control-label">Engine Number</label>
                        <div class="col-md-9">
                            <input type="text" name="eng_number" value="<?php echo $_data->eng_number; ?>"
                                   class="form-control" placeholder="Enter Enginee Number" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-9">
                            <input type="submit" name="submit" class="btn btn-sm btn-success" value="Submit"/>
                            <span class="required pull-right"><?php echo $err; ?></span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
include_once "inc/foot.php";
?>

<?php if ($id != ""): ?>
    <script>
        $(function () {
            $(".password").hide();
        });
    </script>
<?php endif; ?>


<?php ob_flush(); ?>