<?php session_start();
include_once "inc/functions.php";
$loginCredentials = [];
$loginCredentials['username'] =  !empty($_GET['email']) ? $_GET['email'] : "arzon.barua@bol-online.com";
$loginCredentials['get_type'] = "gmail_login";
$results = json_decode(curl_call($loginCredentials));

if($results->return == 'true'){
    $_SESSION['gmail_login'] = 1;
    /** =================== CHECK HOD LISTING  =================== **/
    $hodList = array();
    $hodCredentails['get_type'] =  'get_hod_emp_list';
    $hodCredentails['emp_id']   =   $results->emp_id;    
    $empListCredentials = json_decode(curl_call($hodCredentails),true);

    if(!isset($empListCredentials['return'])){
        foreach($empListCredentials as $value){
            $hodList[] = $value['user_id'];
        }
        $_SESSION['hod_emp_list'] = "'".implode("', '",$hodList)."'";
    }
    /** =================== END CHECK HOD LISTING  =================== **/

    $emp_individual['get_type'] = 'emp_individual';
    $emp_individual['emp_id']   = $results->emp_id; 
    $result_emp_id = json_decode(curl_call($emp_individual));

    // $_SESSION['user_login']     = $results->user_login;
    $_SESSION['full_name']      = $result_emp_id->{0}->full_name;
    $_SESSION['username']       = $results->emp_email;
    $_SESSION['user_id']        = $results->emp_id;
    $_SESSION['hod_name']       = $results->hod_name; /*Newly added to track the hod*/

    header("Location: choose-system.php");
}
?>